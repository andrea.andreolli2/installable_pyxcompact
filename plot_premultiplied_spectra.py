from pyxcompact.simulation import Simulation, MultiSimulation
from pyxcompact.filechecker import Stats2DManager, MonitorManager, CheckpointManager, PlanesManager
import matplotlib.pyplot as plt
import numpy as np
from numpy.fft import fftn
from pathlib import Path
from numba import jit
import os

sim  = Simulation("/media/data/ISTM/re150/re150/input.i3d")
sim.compute_coordinates()





lx = sim.getInputParam("BasicParam/xlx")
ly = sim.getInputParam("BasicParam/yly")
lz = sim.getInputParam("BasicParam/zlz")


g = Stats2DManager(sim, "x")
averaged = g.TimeAverageInterval(95000, 100000).flatten("z").computeReynoldsStresses()

usePlanes = True
yplus = [15, 100]
axes = sim.getDimensionlessAxes(True)
indices = [(np.abs(axes[1] - i)).argmin() for i in yplus]
if (usePlanes):
    p = PlanesManager(sim)
    result = p.GetTimeAveragedPhiTensor(20000, 100000, "y")
    indices = result[0] 
    yplus = [axes[1][i] for i in indices]
    Rhat_ij = result[1]
else:
    c = CheckpointManager(sim)
    Rhat_ij = c.extractVirtualPlanes("y", indices).GetTimeAveragedPhiTensor(20000, 100000, "y")[1]
yplus = [axes[1][i] for i in indices]
plt.figure(figsize=(len(indices) * 6, 4))
for i in range(len(indices)):
    plt.subplot(1, len(indices), i+1)
    avg = np.average(Rhat_ij[:, :, :, :, i], 1)
    lmbda_1 = np.zeros((len(sim.x)))
    for x in range(len(sim.x)):
        kx = x * 2 * np.pi / lx
        avg[x, :, :] *= kx / averaged.getDimensionFactor("<u>")**2
        if kx > 0:
            lmbda_1[x] = 2 * np.pi / kx * sim.getInputParam(f"BasicParam/re")
        else:
            lmbda_1[x] = np.NaN
    avg[0:0, :, :] = np.NaN
    plt.plot(lmbda_1, avg[:, 0, 0], label = r"$\Phi_{uu}$")
    plt.plot(lmbda_1, avg[:, 1, 1], label = r"$\Phi_{vv}$")
    plt.plot(lmbda_1, avg[:, 2, 2], label = r"$\Phi_{ww}$")
    plt.xlim(left=10)
    plt.ylim(0, 1)
    plt.legend()
    plt.xlabel(r"$\lambda_x^+$")
    plt.ylabel(r"$k_x\cdot\Phi/u_\tau²$")
    plt.title(f"Premultiplied Velocity spectrum tensor for y+ = {yplus[i]:.2f}")
    plt.xscale("log")

pl_str = "_planes" if usePlanes else ""
plt.savefig(f"output/premultiplied_spectra{pl_str}.png")
plt.show()
