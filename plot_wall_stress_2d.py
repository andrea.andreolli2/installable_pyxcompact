from pyxcompact.simulation import Simulation, MultiSimulation
from pyxcompact.filechecker import Stats2DManager, MonitorManager, CheckpointManager, ypManager, BCManager
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path

sim = Simulation("/media/data/ISTM/wall_friction/variable/laminar_example_cluster/input.i3d")
sim.compute_coordinates()
a = Stats2DManager(sim, "x")
b = CheckpointManager(sim)
#c = ypManager(sim)

start = 500
res = a.TimeAverageInterval(start, start).computeReynoldsStresses().computeVelocityDeficit()

print(f"Total wall stress: {res.getWallStress()}")
wall_stress = res.getWallStress(flatten=False, degree=sim.getInputParam('BasicParam/ibc_shear_stress'))
print(f"Row-wise wall stress: {wall_stress}")
#print(res.getWallStress(flatten=False))
bc = BCManager(sim)
bc.files[0].loadFile()
re_cent = 1 / res.get_nu()
re = sim.getInputParam('BasicParam/re')
bdudy = re**2 / re_cent * res.get_nu()

expected = np.average(bc.files[0].contents["ubc"], 0) * bdudy
print(f"Expected: {expected}")

print(f"Allclose? {np.allclose(expected, wall_stress)}")

plots = ["uh-u", "<v>", "<w>"]
titles = ["Velocity deficit (wall units)", "<v>+", "<w>+"]

plt.figure(figsize=(14, 8))
counter = 1
for p, t in zip(plots, titles):
    plt.subplot(2, 3, counter)
    a.files[0].loadFile()
    plt.pcolormesh(sim.z, sim.y, res.getDimensionless(True)[p])
    plt.xlabel("z/h")
    plt.ylabel("y/h")
    plt.title(t)
    plt.colorbar()
    plt.ylabel("")
    counter+=1
plt.subplot(2, 3, 4)
plt.plot(sim.z, wall_stress / bdudy)
plt.title("Wall stress du/dy")
plt.ylabel("du/dy / tau_w / nu")
plt.subplot(2, 3, 5)
plt.plot(sim.z, res.getWallStress(flatten=False, degree=sim.getInputParam('BasicParam/ibc_shear_stress'), direction="w"))
plt.title("Wall stress dw/dy")
plt.ylabel("dw/dy")
#plt.show()
plt.savefig(f"output/variable_t={start}")
plt.figure()
x, y = np.meshgrid(sim.z, sim.y)
plt.quiver(sim.z, sim.y, res.getDimensionless(True)["<w>"], res.getDimensionless(True)["<v>"], width=0.001)
plt.savefig(f"output/variable_quiver_t={start}")
plt.show()

