from pyxcompact.simulation import Simulation, MultiSimulation
from pyxcompact.simulationfile import WallStressBCFile
import matplotlib.pyplot as plt
from argparse import ArgumentParser

# parse args
parser = ArgumentParser(description='Plot the slip length boundary condtion from the input file.')
parser.add_argument('sim_dir', metavar=('sim_dir'), type=str, nargs=1, help='Input.i3d for target simulation.')
settings = parser.parse_args()
infile = settings.sim_dir[0]

# read file
sim  = Simulation(infile)
n = {coord : sim.getInputParam(f"BasicParam/n{coord}") for coord in ["x", "y", "z"]}
w = WallStressBCFile(None, sim, False, "w")
w.loadFile()
print(w.contents)

plt.imshow(w.contents['wbc'])
plt.show()
plt.scatter(sim.z, w.contents['wbc'][0,:], marker="x")
plt.show()