from pyxcompact.simulation import Simulation, MultiSimulation
from pyxcompact.filechecker import Stats2DManager, MonitorManager
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path

from matplotlib.animation import FuncAnimation

def read_headers(root, headers):
    result = {}
    for file, skip, header in headers:
        tx = np.loadtxt(root / file, skiprows=skip)
        for i in range(len(header)):
            result[header[i]] = [tx[:, 1], tx[:, i]]
    return result

def read_ml(re : int):
    root = Path("/media/data/ISTM/re150/Texas/")
    headers = [[f"LM_Channel_0{re}_mean_prof.dat", 72, ["y/delta", "y+", "<u>", "d<u>/dy", "<w>", "P"]],
               [f"LM_Channel_0{re}_vel_fluc_prof.dat", 75, ["y/delta", "y+", "<u'*u'>", "<v'*v'>", "<w'*w'>", "<u'*v'>", "<u'*w'>", "<v'*w'>"]],
               [f"LM_Channel_0{re}_vp_prof.dat", 68, ["y/delta", "y+", "<u'*p'>", "<v'*p'>", "<w'*p'>"]],
               [f"LM_Channel_0{re}_vor_pres_fluc_prof.dat", 75, ["y/delta", "y+", "<omega_x'*omega_x'>", "<omega_y'*omega_y'>", "<omega_z'*omega_z'>", 
                                                      "<omega_x'*omega_y'>", "<omega_x'*omega_z'>", "<omega_y'*omega_z'>", "<p'*p'>"]]]
    return (f"Lee 2020, Re={re}", read_headers(root, headers))

def read_mkm(re : int):
    root = Path(f"/media/data/ISTM/re150/Texas2/chan{re}/profiles/")
    headers = [[f"chan{re}.means", 25, ["y/delta", "y+", "<u>", "d<u>/dy", "<w>", "d<w>/dy" "P"]],
               [f"chan{re}.reystress", 25, ["y/delta", "y+", "<u'*u'>", "<v'*v'>", "<w'*w'>", "<u'*v'>", "<u'*w'>", "<v'*w'>"]],
               [f"chan{re}.velp", 25, ["y/delta", "y+", "<u'*p'>", "<v'*p'>", "<w'*p'>", "<p'*p'>"]],
               [f"chan{re}.vortvar", 25, ["y/delta", "y+", "<omega_x'*omega_x'>", "<omega_y'*omega_y'>", "<omega_z'*omega_z'>", 
                                                      ]]]
    return (f"MKM 1999, Re={re}", read_headers(root, headers))

to_plot = [[(Simulation("/media/data/ISTM/re150/re150/input.i3d"), [50000, 100000], "NSBC")], 
           [(MultiSimulation(["/media/data/ISTM/wall_friction/re180/first_order/input.i3d"]), [50000, 200000], "SSBC First Order")],
           [(MultiSimulation(["/media/data/ISTM/wall_friction/re180/second_order/input.i3d"]), [50000, 200000], "SSBC Second Order")],
           [(MultiSimulation(["/media/data/ISTM/wall_friction/re180/third_order/input.i3d"]), [50000, 200000], "SSBC Third Order")]
           ]
           #[(MultiSimulation(["/media/data/ISTM/re150/re540/run01/input.i3d"]), [50000, 75000]), read_ml(550), read_mkm(590)]]

plots = ["uh-u", "<u'*u'>", "<v'*v'>", "<w'*w'>", "<u'*v'>", "<u'*w'>", "<v'*w'>", "<u'*p'>", "<v'*p'>", "<w'*p'>", "<p'*p'>"]
grid = [3, 4]
plt.figure(figsize=(20, 12))

counter = 0
for l in to_plot:
    for first, second, third in l:
        plt.subplot(*grid, grid[0]*grid[1])
        if isinstance(first, Simulation):
            g = Stats2DManager(first, "x")
            averaged = g.TimeAverageInterval(*second).flatten("z").computeReynoldsStresses().computeVelocityDeficit()
            print(averaged.getWallStress())
            axes = g.getDimensionlessAxes(True)
            re = first.getInputParam("BasicParam/re")
            plt.plot([0], [0], label=f"{third} {second} Re={re}")
        else:
            plt.plot([0], [0], label=first)
        plt.legend()
        
        for plot, i in zip(plots, range(len(plots))):
            plt.subplot(*grid, i+1)
            if isinstance(first, Simulation):
                dimensionless_data = averaged.getDimensionless(True)[plot]
                plt.semilogx(axes[0][0:int(len(axes[0]) / 2)], dimensionless_data[0:int(len(axes[0]) / 2)], label=f"{plot} {second} Re={re}")
                plt.gca().set_xlim(left=1, right=axes[0][int(len(axes[0]) / 2)] * 1.2)
            elif isinstance(second, dict):
                plt.semilogx(second[plot][0], second[plot][1], label=first)
            plt.xlabel("y+")
            plt.ylabel(f"{plot}+")
            counter += 1
plt.savefig("output/statistics_ssbc.png", dpi=200)
plt.show()
