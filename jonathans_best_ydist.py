from pyxcompact.simulation import ystretching
import matplotlib.pyplot as plt
import numpy as np

#Re = 360
#centerline_meshsize = 8 / 512
Re = 540
centerline_meshsize = 8 / 1024

eps = 10**(-6)
bestn = 0
oldbest = 100
bestbeta = 0
for n in range(301, 610, 2):
    def fct(x):
        return ystretching(2, n, x, 2)[1] - 1/Re
    # find beta such that y[1]+ = 1 using Newton's method
    x0 = 0.5
    res = 1
    while (abs(res) > 10**(-15)):
        res = fct(x0)
        numeric_derivative = (fct(x0+eps) - fct(x0-eps)) / eps / 2
        x0 = x0 - res / numeric_derivative
    center_index = int((n-1)/2)
    ystret = ystretching(2, n, x0, 2)
    center_size = ystret[center_index +1] - ystret[center_index]
    diff = abs(center_size - centerline_meshsize)
    print(f"For n={n}: beta={x0}, centerline_meshsize={center_size}, diff={diff}")
    if (diff < oldbest):
        oldbest = diff
        bestn = n
        bestbeta = x0
ystret = ystretching(2, bestn, bestbeta, 2)
center_index = int((bestn-1)/2)
print(f"Best result: n={bestn}: beta={bestbeta} with y+[firstnode]={ystret[1]*Re}, centerline_meshsize={ystret[center_index+1] - ystret[center_index]}, should be {centerline_meshsize}")

plt.plot(ystret, ".")

#plt.plot(np.gradient(ystret))
print(ystret[1]*Re)
plt.show()