from pyxcompact.simulation import ystretching
import matplotlib.pyplot as plt
import numpy as np
import numpy as np
from scipy.optimize import fsolve

retau = 500
initial_ny = 90 # ny
maximum_ny = 1300 # ny
initial_guess_beta = [0.4]
target_dycp = 10 # desired dy at the centerline
target_dywp = 1 # desired dy at the wall

# do not expect parameters below to change
ly = 2 # size of domain in wall-normal direction
istret = 2 # stretching on both sides

possible_values_ny = np.array([3,5,7,9,11,13,17,19,21,25,31,33,37,41,49,51,55,61,65,73,81,91,97,101,109,121,129,145,151,161,163,181,193,201,217,241,251,257,271,289,301,321,325,361,385,401,433,451,481,487,501,513,541,577,601,641,649,721,751,769,801,811,865,901,961,973,1001,1025,1081,1153,1201,1251,1281,1297,1351,1441,1459,1501,1537,1601,1621,1729,1801,1921,1945,2001,2049,2161,2251,2305,2401,2431,2501,2561,2593,2701,2881,2917,3001,3073,3201,3241,3457,3601,3751,3841,3889,4001,4051,4097,4321,4375,4501,4609,4801,4861,5001,5121,5185,5401,5761,5833,6001,6145,6251,6401,6481,6751,6913,7201,7291,7501,7681,7777,8001,8101,8193,8641,8749,9001,9217,9601,9721])

def get_dy_wall(ny,beta):
    y = ystretching(ly, ny, beta, istret)
    dy = y[1] - y[0]
    return dy*retau

def distance_desired_dy_wall(p,ny):
    return get_dy_wall(ny,p[0]) - target_dywp

def get_dy_center(ny, beta):
    y = ystretching(ly, ny, beta, istret)
    ny = len(y)
    if not (ny%2)==0:
        ny = ny-1
    ih = int(ny/2); il = ih - 1
    dy = y[ih] - y[il]
    return dy*retau

possible_values_ny = possible_values_ny[possible_values_ny>initial_ny]
possible_values_ny = possible_values_ny[possible_values_ny<maximum_ny]

for yy in possible_values_ny:
    x = fsolve(distance_desired_dy_wall, initial_guess_beta, args=(yy))
    print(yy, x[0], get_dy_wall(yy, x[0]), get_dy_center(yy, x[0]))
    dycp = get_dy_center(yy,x[0])
    if dycp <= target_dycp:
        ny = yy
        break
