# Python tools for incompact3d
A python package with tools to read and postprocess the output of the channel dns solver, as well as its associated postprocessing tools.
Originally developed by Jonathan Neuhauser; repackaged for ease of use.

## Installation
Navigate to the repository folder and then hit:
```bash
pip install .
```
