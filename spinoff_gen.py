import os
import toml
import shutil



# case params
#tgt_folder_list = ['r180_s0125', 'r180_s025', 'r180_s05', 'r180_s1', 'r180_s2','r180_s3']
tgt_folder_list = ['r180_s1']
smooth_folder = 'r180_ref'

get_decay = False
last_fid_decay = 500 # last fid to use for initial conditions (from case with slip length)
no_realisations_decay = 30
first_index_decay = 0 # name of first folder

get_formation = True
last_fid_formation = 380 # last fid to use for initial conditions (from case with slip length)
no_realisations_formation = 30
first_index_formation = 0 # name of first folder

# simulation runtime params
my_ifirst = 1
my_ilast = 10000
my_icheckout = 10000
my_statsfreq = 20

# slurm job params
slurm_queue = 'single'
slurm_nodes = '1'
slurm_procs = '40'
slurm_time = '07:00:00'

# constants
FORMATION = 'form'
DECAY = 'deca'



####################################################################################
# script begins here



# generate stuff
internal_fold_names_decay = [str(ii) for ii in range(first_index_decay, first_index_decay + no_realisations_decay)]
tgt_fids_decay = [f'0{jj}000' for jj in range(last_fid_decay-2*(no_realisations_decay-1),last_fid_decay+2,2)]

internal_fold_names_formation = [str(ii) for ii in range(first_index_formation, first_index_formation + no_realisations_formation)]
tgt_fids_formation = [f'0{jj}000' for jj in range(last_fid_formation-2*(no_realisations_formation-1),last_fid_formation+2,2)]

def master_of_parsers(filename, dikt_o_rulez):

    with open(filename,'r') as f:
        lines = f.readlines()

    # check that each rule is matched only once
    for rule in dikt_o_rulez.keys():
        count = 0
        for line in lines:
            if rule[0:20] in line[0:20].replace(' ',''):
                count += 1
        if not count == 1:
            raise KeyError(f"Rule {rule} was matched {count} times.")

    for ii, line in enumerate(lines):
        for rule in dikt_o_rulez.keys():
            if rule[0:20] in line[0:20].replace(' ',''):
                lines[ii] = dikt_o_rulez[rule]

    return lines



rules_for = {
'ifirst=': f'ifirst = {my_ifirst}           ! First iteration\n',
'ilast=': f'ilast = {my_ilast}      ! Last iteration\n',
'irestart=': 'irestart = 1         ! Read initial flow field ?\n',
'ibc_shear_stress_read_dwdy=': 'ibc_shear_stress_read_dwdy = 1\n',
'ibc_slip_length_w=': 'ibc_slip_length_w = 1\n',
'icheckpoint=': f'icheckpoint = {my_icheckout}     ! Frequency for writing backup file\n',
'ioutput=': f'ioutput = {my_icheckout}        ! Frequency for visualization\n',
'istatx=': 'istatx = 2 ! statistics in x-direction (1,2,3: 1st,2nd or 3rd order)\n',
'sfreq2d=': f'sfreq2d={my_statsfreq}   ! sample frequency for 2D statistics (mod(sfreq2d,ofreq2d)=0!)\n',
'ofreq2d=': f'ofreq2d={my_statsfreq}  ! output frequency for 2D statistics (mod(ofreq2d,icheckpoint)=0!)\n',
}
rules_dec = rules_for.copy()
rules_dec['ibc_shear_stress_read_dwdy'] = 'ibc_shear_stress_read_dwdy = 0\n'
rules_dec['ibc_slip_length_w='] = 'ibc_slip_length_w = 0\n'

rules_slurm = {
'-p': f'#SBATCH -p {slurm_queue}\n',
'-N': f'#SBATCH -N {slurm_nodes} -n {slurm_procs}\n',
'--time': f'#SBATCH --time={slurm_time}\n',
}



def copy_files(base_folder, formdec, series_id, restart_fid):

    new_folder = formdec + '_' + base_folder
    if not  os.path.exists(new_folder):
        os.mkdir(new_folder)
    new_folder = new_folder + f'/{series_id}'
    if os.path.exists(new_folder):
       	raise Error('target folder exists already.')
    os.mkdir(new_folder)

    shutil.copytree(base_folder+'/xcompact3d-jh', new_folder+'/xcompact3d-jh', ignore=shutil.ignore_patterns('.git'))

    # deal with extra settings
    xset = toml.load(base_folder + '/extra_settings.toml')
    xset['spinoff'] = {}
    xset['spinoff']['type'] = formdec
    xset['spinoff']['restart_fid'] = restart_fid
    xset['spinoff']['internal_id'] = series_id
    with open(new_folder + '/extra_settings.toml', 'w') as f:
        toml.dump(xset, f)

    if formdec == FORMATION:

        restart_fname = smooth_folder+'/restart'+restart_fid
        shutil.copy(base_folder+'/wstressbc.dat', new_folder)
        shutil.copy(restart_fname, new_folder+'/restart0000000')

        input_lines = master_of_parsers(base_folder+'/input.i3d', rules_for)
        with open(new_folder+'/input.i3d','w') as f:
            f.writelines(input_lines)

        rules_slurm['--job-name'] = f'#SBATCH --job-name={"f"+base_folder.replace("r180_","")}_{series_id}\n'
        slurm_lines = master_of_parsers(base_folder+'/job_bwucl.sh', rules_slurm)
        with open(new_folder+'/job_sim.sh','w') as f:
            f.writelines(slurm_lines)

    elif formdec == DECAY:

        restart_fname = base_folder+'/restart'+restart_fid
        shutil.copy(restart_fname, new_folder+'/restart0000000')

        input_lines = master_of_parsers(base_folder+'/input.i3d', rules_dec)
        with open(new_folder+'/input.i3d','w') as f:
            f.writelines(input_lines)

        rules_slurm['--job-name'] = f'#SBATCH --job-name={"d"+base_folder.replace("r180_","")}_{series_id}\n'
        slurm_lines = master_of_parsers(base_folder+'/job_bwucl.sh', rules_slurm)
        with open(new_folder+'/job_sim.sh','w') as f:
            f.writelines(slurm_lines)

    print(f'Made folder {new_folder} with {restart_fname}')



# actual script
for tgt_folder in tgt_folder_list: # loop over different cases (different values of s or re)
    if get_decay:
        for folder_id, restart_id in zip(internal_fold_names_decay, tgt_fids_decay): # loop over different realisations
            copy_files(tgt_folder, DECAY, folder_id, restart_id)
    if get_formation:
        for folder_id, restart_id in zip(internal_fold_names_formation, tgt_fids_formation): # loop over different realisations
            copy_files(tgt_folder, FORMATION, folder_id, restart_id)
