import argparse
import pandas as pd
import os
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description='Collates and plots all monitor*.txt files from an incompact3d simulation.')
parser.add_argument('sim_path', metavar='simdir/', type=str, nargs=1, help='directory containing the input.i3d file of the simulation to be analysed')
args = parser.parse_args()
base_path = (args.sim_path[0] + '/').replace('//','/')

print("The following columns are available:")
print("ub", "Nu", "tauw", "Tm")
des_col = input("What do you want to plot? ")

fig, ax = plt.subplots()

def plot_all_monitors(fldr,target,clr):

    fldr += '/' # add this just in case user doesn't do it
    
    file_list = [fldr+"monitor.txt"]
    
    # get list of files
    if os.path.exists(fldr+"monitor"):
        to_add = os.listdir(fldr+'/monitor/')
        for elm in to_add:
            file_list.append(fldr+"monitor/"+elm)
    
    # check what is zero instant
    zero = 999999999999999
    for f in file_list:
        rtd = pd.read_csv(f, sep="\s+",header=1)
        rtd.columns = ["t", "ub", "Nu", "tauw", "Tm"]
        if rtd["t"][0] < zero:
            zero = rtd["t"][0]
    
    for f in file_list:
        rtd = pd.read_csv(f, sep="\s+",header=1)
        rtd.columns = ["t", "ub", "Nu", "tauw", "Tm"]
        rtd["t"] -= zero
        rtd.plot(x="t", y=target, ax=ax, color=clr, legend=None)

plot_all_monitors(base_path, des_col, 'black')
plt.show()