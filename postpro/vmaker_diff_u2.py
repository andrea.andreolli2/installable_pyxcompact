from argparse import ArgumentParser
from pyxcompact.simulation import Simulation
import numpy as np
from cmcrameri import cm
from progressbar import progressbar
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import toml, os, shutil, math

#########################################################
### HARD SETTTINGS ######################################
labels_for_qtitites = ['y^+ \\Delta \\tilde{u}^2 / 2']
#########################################################
#########################################################

parser = ArgumentParser(
    prog = 'vmaker_diff_u2',
    description='Creates a video of the difference between the current and initial u2 energy.'
    )
parser.add_argument('target_dir', metavar='sim_folder/', type=str, nargs=1, help='the directory containing the repeated simulations')

# unpack input
settings = parser.parse_args()
base_dir = settings.target_dir[0] + '/'; base_dir = base_dir.replace('//','/')
input_file = base_dir + '0/input.i3d'

# OUTPUT SETTINGS 
#################
# make sure that output dir exists before looping
# results are output in the next session
out_dir = base_dir + 'raw_ppro_output/videos/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
# name for videos
vidname = f'diff_u2'

# load metadata
this_sim = Simulation(input_file)
y = this_sim.y; ny = len(y)
meta = toml.load(base_dir + 'raw_ppro_output/avg/all_you_need.nfo')
re = meta['base']['re']
ncdisp = meta['grid_etc']['nterms_disp']
nt = meta['grid_etc']['nt']; dt = meta['grid_etc']['dt']
t = (np.arange(nt) + 1) * dt
nzd = meta['grid_etc']['nz']; lzd = meta['grid_etc']['lz']
dz = lzd/nzd; zd = np.arange(nzd+1) * dz
ref_meta = toml.load(base_dir.replace('deca_','') + 'raw_ppro_output/avg/treeplosix.nfo')
ref_ncdisp = ref_meta['grid_etc']['nterms_disp']

# load arrays and expand them
#############################
# when after reading, last point in z is created (has same value as the first)
# this is convenient for plotting
disp = np.empty((nt,ny,nzd+1))
disp_on_disk = np.memmap(base_dir + 'raw_ppro_output/avg/evo_disp.bin', dtype=np.float64, mode='r', shape=(nt,ncdisp,ny,nzd))
disp[:,:,:-1] = disp_on_disk[:,0,:,:]
disp[:,:,-1] = disp[:,:,0]
ref_disp = np.empty((ny,nzd+1))
ref_disp_on_disk = np.memmap(base_dir.replace('deca_','') + 'raw_ppro_output/avg/disp.bin', dtype=np.float64, mode='r', shape=(ref_ncdisp,ny,nzd))
ref_disp[:,:-1] = ref_disp_on_disk[0,:,:]
ref_disp[:,-1] = ref_disp[:,0]

# get viscous yp
nyhalf = math.ceil(ny/2) # nyhalf describes half channel
yp = y[:nyhalf] * re

# get quantity you want to plot
to_plot = yp.reshape((-1,1)) * ((disp[:,:nyhalf,:]**2) - (ref_disp[:nyhalf,:]**2))

# plot initialisation
#####################
# get frame
my_dpi = 150
fig, ax = plt.subplots(1,1,figsize=(1080/my_dpi,1080/my_dpi),dpi=my_dpi)

# first plot
# needed to setup colorbar
it = 0
# get clims
mm = np.amax(np.abs(to_plot[it,:,:]))
hu = ax.pcolormesh(zd,yp,to_plot[it], shading='gouraud', cmap=cm.broc, vmin=-mm, vmax=mm)
ax.set_ylim([3,yp[-1]])
ax.set_yscale('log')
cbu = plt.colorbar(hu)


#######################################
# TIME LOOPING AND ENSEMBLE AVERAGING #
#######################################

for it in progressbar(range(nt)): # at any instant in time...

    hu.set_array(to_plot[it,:,:])
    # set new clim
    mm = np.amax(np.abs(to_plot[it]))
    hu.set_clim([-mm,mm])
    cbu.update_normal(hu)

    # set title and save
    fig.suptitle(f'$t = {round(t[it],3)}$ (same units as simulation)')
    fname = out_dir + f'{vidname}_frame_{it}'
    fig.savefig(fname + '.png')

# write script to make video
with open(out_dir + 'make_video.sh','w') as f:
    f.write(f'ffmpeg -r 24 -f image2 -i {vidname}_frame_\%d.png -vcodec libx264 -crf 25 -pix_fmt yuv420p {vidname}.mp4\n')
    f.write(f'mkdir src_{vidname}\n')
    f.write(f'mv *.png src_{vidname}\n')

# copy metadata
shutil.copyfile(base_dir + 'raw_ppro_output/avg/all_you_need.nfo', out_dir + f'meta_{vidname}.nfo')
