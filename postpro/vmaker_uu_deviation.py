from argparse import ArgumentParser
from pyxcompact.simulation import Simulation
import numpy as np
from cmcrameri import cm
from progressbar import progressbar
from pyxcompact.andreas_stuff.io_and_averaging import *
from pyxcompact.andreas_stuff.span_pattern import span_pattern
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import toml, os, shutil, math, gc

#########################################################
### HARD SETTTINGS ######################################
labels_for_qtitites = ['y^+ \\Delta \\tilde{u}^2 / 2']
#########################################################
#########################################################

parser = ArgumentParser(
    prog = 'vmaker_uu_deviation',
    description='Creates a video of the evolution of uu.'
    )
parser.add_argument('target_dir', metavar='sim_folder/', type=str, nargs=1, help='the directory containing the repeated simulations')
parser.add_argument('--premult', '-p', action='store_true', help='premultiply the plot times y+')


# unpack input
settings = parser.parse_args()
base_dir = settings.target_dir[0] + '/'; base_dir = base_dir.replace('//','/')
input_file = base_dir + '0/input.i3d'
premultiply = settings.premult


# OUTPUT SETTINGS 
#################
# make sure that output dir exists before looping
# results are output in the next session
out_dir = base_dir + 'raw_ppro_output/videos/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
# name for videos
vidname = f'uu_deviation'

# load metadata
this_sim = Simulation(input_file)
y = this_sim.y; ny = len(y)
meta = toml.load(base_dir + 'raw_ppro_output/avg/all_you_need.nfo')
re = meta['base']['re']
ncdisp = meta['grid_etc']['nterms_disp']
nt = meta['grid_etc']['nt']; dt = meta['grid_etc']['dt']
t = (np.arange(nt) + 1) * dt
nzd = meta['grid_etc']['nz']; lzd = meta['grid_etc']['lz']
dz = lzd/nzd; zd = np.arange(nzd+1) * dz
ref_meta = toml.load(base_dir.replace('deca_','') + 'raw_ppro_output/avg/treeplosix.nfo')
ref_ncdisp = ref_meta['grid_etc']['nterms_disp']

##################################
# READ TIME EVO OF DISPERSIVE uu #
##################################

uu = np.zeros((nt,ny,nzd+1), dtype=np.float64) # plottable time evo of uu

rst_on_disk = np.memmap(base_dir + 'raw_ppro_output/avg/evo_rst.bin', dtype=np.float64, shape=(nt,6,ny,nzd))

uu[:,:,:-1] = rst_on_disk[:,0,:,:]
uu[:,:,-1] = uu[:,:,0]

# remove spanwise avg
uu -= uu[:,:,:-1].mean(axis=-1).reshape(nt,ny,1)

# get viscous yp
nyhalf = math.ceil(ny/2) # nyhalf describes half channel
yp = y[:nyhalf] * re

# get quantity you want to plot
if premultiply:
    to_plot = yp.reshape((-1,1)) * uu[:,:nyhalf,:]
else:
    to_plot = uu[:,:nyhalf,:]

# plot initialisation
#####################
# get frame
my_dpi = 150
fig, ax = plt.subplots(1,1,figsize=(1080/my_dpi,1080/my_dpi),dpi=my_dpi)

# first plot
# needed to setup colorbar
it = 0
# get clims
mm = np.amax(np.abs(to_plot[it,:,:]))
hu = ax.pcolormesh(zd,yp,to_plot[it], shading='gouraud', cmap=cm.broc, vmin=-mm, vmax=mm)
ax.set_ylim([3,yp[-1]])
ax.set_yscale('log')
cbu = plt.colorbar(hu)


#######################################
# TIME LOOPING AND ENSEMBLE AVERAGING #
#######################################

for it in progressbar(range(nt)): # at any instant in time...

    hu.set_array(to_plot[it,:,:])
    # set new clim
    mm = np.amax(np.abs(to_plot[it]))
    hu.set_clim([-mm,mm])
    cbu.update_normal(hu)

    # set title and save
    fig.suptitle(f'$t = {round(t[it],3)}$ (same units as simulation)')
    fname = out_dir + f'{vidname}_frame_{it}'
    fig.savefig(fname + '.png')

# write script to make video
with open(out_dir + 'make_video.sh','w') as f:
    f.write(f'ffmpeg -r 24 -f image2 -i {vidname}_frame_\%d.png -vcodec libx264 -crf 25 -pix_fmt yuv420p {vidname}.mp4\n')
    f.write(f'mkdir src_{vidname}\n')
    f.write(f'mv *.png src_{vidname}\n')

# copy metadata
shutil.copyfile(base_dir + 'raw_ppro_output/avg/all_you_need.nfo', out_dir + f'meta_{vidname}.nfo')
