from pyxcompact.andreas_stuff.spectra import spectral_tools
from pyxcompact.andreas_stuff.io_and_averaging import *
from pyxcompact.simulation import Simulation
from argparse import ArgumentParser
import numpy as np
import os
import toml

parser = ArgumentParser(
    prog = 'get_mode_history',
    description = 'Calculates history of modes (kx,kz) = (0,[1,...,N]*pi/s) from xcompact statistics files.',
)
parser.add_argument('--multi', '-m', action='store_true', help='if passed, perform ensemble average of multiple simulations')
parser.add_argument('input_file', metavar='input.i3d', type=str, nargs=1,
                    help='the input file of the simulation to be processed')
parser.add_argument('-n', action='store', help='number of odd subharmonics of fundamental wavenumber to track', required=False, type=int, nargs=1)

# unpack input
inarg = parser.parse_args()
if inarg.n:
    N = int(inarg.n[0])
else:
    N = 10
input_file = inarg.input_file[0]
multi = inarg.multi

# process input
if not multi:
    base_dir = input_file.replace('input.i3d','')
    if not base_dir:
        base_dir = './'
        rough_input = input_file.replace('deca_','').replace('form_','')
else:
    base_dir = input_file + '/'
    input_file = base_dir + '0/input.i3d'
    rough_input = base_dir.replace('deca_','').replace('form_','') + 'input.i3d'

# get simulation and spectral details
sim = Simulation(input_file)
psd = spectral_tools(sim)
# process extra_settings
xtra_set = toml.load(input_file.replace('input.i3d','extra_settings.toml'))
first_fid = xtra_set['mode_history']['first_fid']
last_fid = xtra_set['mode_history']['last_fid']
fid_list = fid_gen(first_fid, last_fid)
no_files = len(fid_list)
s = xtra_set['sliplength']['s']
ny = len(sim.y)
# get z index
kz_tgt = np.pi / s
iz_tgt = np.where(psd.kz == kz_tgt)[0]

list_ensemble = [0]
if multi:
    list_ensemble = os.listdir(base_dir)
    for el in list_ensemble.copy():
        if not el.isnumeric():
            list_ensemble.remove(el)
no_realisations = len(list_ensemble)

# preallocate arrays
magni = np.zeros((no_realisations,no_files,3,ny,N), dtype = np.float64)
phase = np.zeros_like(magni, dtype = np.float64)

nzlist = [2*ii+1 for ii in range(N)]

# calculate output
for kk, realisation in enumerate(list_ensemble):
    in_between = ''
    if multi:
        in_between = realisation + '/'
    for ii, fid in enumerate(fid_list):
        filename = base_dir + in_between + 'stats2d/xstats' + fid
        print('Reading file ' + filename)
        fld, content = stats_as_memmap(sim, filename)
        for ic in range(3):
            for iy in range(ny):
                for iz,nz in enumerate(nzlist):

                    # bottom wall
                    transform = np.fft.rfft(fld[ic,:,iy]) / psd.nz

                    magni[kk,ii,ic,iy,iz] = np.absolute(transform[nz*iz_tgt]) ** 2
                    phase[kk,ii,ic,iy,iz] = np.angle(transform[nz*iz_tgt])

# use y-symmetry
# careful: v is antisymmetric in y, but is symmetric!
# careful: for magni, you're not dividing by 2 - see below!
magni[:,:,:,:,:] += magni[:,:,:,::-1,:]
# there should be some multiplication and divisions to do, but they cancel out!
# magni /= 2 # averaging with top wall
# magni *= 2 # real transform - there's a negative wavelength for each one. notice that kz=0 is not saved in magni!
phase[:,:,0,:,:] += phase[:,:,0,::-1,:]
phase[:,:,1,:,:] += phase[:,:,1,::-1,:] # + pi # ideally, because of antisymmetry, there should be +/- pi on the phase; but practically, idk what output of phase is - so idk if it is + or - or what
phase[:,:,2,:,:] += phase[:,:,2,::-1,:]
phase /= 2

# check existence of output folder
out_dir = base_dir + 'raw_ppro_output/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
out_dir = out_dir + 'mode_evolution/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)

# write out file
magni.mean(axis=0).tofile(out_dir + f'magnitude_mean.bin')
phase.mean(axis=0).tofile(out_dir + f'phase_mean.bin')
magni.std(axis=0).tofile(out_dir + f'magnitude_std.bin')
phase.std(axis=0).tofile(out_dir + f'phase_std.bin')

# collect metadata
run_nfo = {}
relisted = [int(jjj) for jjj in list_ensemble]
run_nfo['run_info'] = {} # stuff about this run of the program
run_nfo['run_info']['iz_tgt'] = int(iz_tgt)
run_nfo['run_info']['no_zmodes'] = int(N)
run_nfo['run_info']['no_realisations'] = int(no_realisations)
run_nfo['run_info']['nt'] = int(no_files)
run_nfo['run_info']['min_fold'] = np.amin(relisted)
run_nfo['run_info']['max_fold'] = np.amax(relisted)

run_nfo['misc'] = {} # other stuff - useful for plotting
run_nfo['misc']['type'] = 'decay' if 'deca' in xtra_set['spinoff']['type'] else 'formation'
run_nfo['misc']['s'] = float(s)
run_nfo['misc']['dt'] = float(sim.getInputParam('Statistics/ofreq2d')) * float(sim.getInputParam('BasicParam/dt'))

# write metadata to file
with open(out_dir + f'mode_time_evolution.nfo','w') as f:
    toml.dump(run_nfo, f)
