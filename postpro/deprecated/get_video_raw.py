from pyxcompact.andreas_stuff.span_pattern import span_pattern
from pyxcompact.andreas_stuff.io_and_averaging import *
from pyxcompact.andreas_stuff.plot_tools import equalise
from pyxcompact.simulation import Simulation
from argparse import ArgumentParser
import matplotlib.pyplot as plt
from cmcrameri import cm
import numpy as np
import os
import toml

parser = ArgumentParser(
    prog = 'get_mode_history',
    description = 'Calculates and plots the evolution of the dispersive motion as the ensemble average across different realisations of the same simulation.',
)
parser.add_argument('input_file', metavar='input.i3d', type=str, nargs=1,
                    help='the directory containing the repeated simulations')

# unpack input
inarg = parser.parse_args()
input_file = inarg.input_file[0]

# process input
base_dir = input_file.replace('input.i3d','')
if not base_dir:
    base_dir = './'

# get simulation and spanwise pattern
sim = Simulation(input_file)
ptrn = span_pattern(sim, input_file.replace('input.i3d','extra_settings.toml'))
# process extra_settings
xtra_set = toml.load(input_file.replace('input.i3d','extra_settings.toml'))
first_fid = xtra_set['mode_history']['first_fid']
last_fid = xtra_set['mode_history']['last_fid']
fid_list = fid_gen(first_fid, last_fid)
nt = len(fid_list)
s = xtra_set['sliplength']['s']
ny = len(sim.y)
nz = len(sim.z)
nzd = ptrn.nppp

# preallocate arrays
vfld = np.zeros((3,nz,ny), dtype = np.float64)
disp = np.zeros((3,ny,nzd+1))
zpa=sim.z[0:nzd+1]

# check existence of output folder
out_dir = base_dir + 'raw_ppro_output/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
out_dir = out_dir + 'single_evolution/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)

# initialise plot
fig, ax = plt.subplots(3,1,figsize=(5,5),gridspec_kw={"height_ratios":[0.05, 1, 0.1]})
ax[2].set_ylabel('$\ell_z^+$')
ax[2].set_xlabel('$z/h$')

# first off: plot the secondary motion
# to get clim for pcolormesh
#rin = input_file.replace("deca_","").replace("form_","").replace("0/","")
#rsim = Simulation(rin)
#disp = np.fromfile(rin.replace("input.i3d","raw_ppro_output/dispersive.bin"), dtype=np.float64).reshape((3,ny,nzd+1))
#tf, ta = plt.subplots()
#hpc = ta.pcolormesh(np.arange(nzd+1),sim.y,disp[0,:,:],shading='gouraud',cmap=cm.roma_r)
#clow, chig = hpc.get_clim()

# calculate output
for ii, fid in enumerate(fid_list):
            
    filename = base_dir + 'stats2d/xstats' + fid
    print('Reading file ' + filename)
    stats, content = stats_as_memmap(sim, filename) # ic, iz, iy
    vfld = stats[0:3,:,:].copy()

    vfld[0,:,:] -= vfld[0,:,:].mean(axis=0)

    for ic in range(3):
        disp[ic,:,:-1] = vfld[ic,:nzd,:].transpose()
    disp[:,:,-1] = disp[:,:,0]

    ax[1].clear()
    pch = ax[1].pcolormesh(zpa,sim.y,disp[0,:,:],shading='gouraud',cmap=cm.roma_r) # ,vmin=clow,vmax=chig
    #qz, qy, qw, qv = equalise(zpa,sim.y,w_disp[:,:],v_disp[:,:],30)
    #ax.quiver(qz,qy,qw,qv,pivot='mid',scale=2)

    ax[1].set_xlim([zpa[0],zpa[-1]])
    ax[1].set_ylim([0,2])
    ax[1].set_ylabel('y/h')
    ax[2].fill_between(zpa,ptrn.span_sl_arr[0:len(zpa)]*xtra_set['sliplength']['lp'],-np.ones(len(zpa)),color='black')
    ax[2].set_ylim(-1,12)
    ax[2].set_xlim([zpa[0],zpa[-1]])
    ax[2].set_xlabel('z/h')
    plt.colorbar(pch, cax=ax[0], orientation="horizontal")

    ax[0].xaxis.set_ticks_position('top')
    ax[1].tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False
    )

    # generate output
    fig.savefig(out_dir + f'video_frame_{ii}.png')

# collect metadata
run_nfo = {}
run_nfo['run_info'] = {} # stuff about this run of the program
run_nfo['run_info']['nt'] = int(nt)

run_nfo['misc'] = {} # other stuff - useful for plotting
run_nfo['misc']['type'] = 'decay' if 'deca' in input_file else 'formation'
run_nfo['misc']['s'] = float(s)
run_nfo['misc']['dt'] = float(sim.getInputParam('Statistics/ofreq2d')) * float(sim.getInputParam('BasicParam/dt'))

# write metadata to file
with open(out_dir + f'mode_time_evolution.nfo','w') as f:
    toml.dump(run_nfo, f)

with open(out_dir + 'make_video.sh','w') as f:
    f.write('ffmpeg -r 24 -f image2 -i video_frame_\%d.png -vcodec libx264 -crf 25 -pix_fmt yuv420p video.mp4')
