from argparse import ArgumentParser
from pyxcompact.simulation import Simulation
import pyxcompact.andreas_stuff.spectra as spectra
from pyxcompact.andreas_stuff.io_and_averaging import *
from pyxcompact.andreas_stuff.span_pattern import span_pattern
from pyxcompact.andreas_stuff.plot_tools import streamplotter
import numpy as np
import toml
import os

# parse arguments
parser = ArgumentParser(description='Calculates power spectral density of dispersive motion without using phase average. First and last snapshots of the series to be used are indicated in extra_settings.toml. File directories are inferred automatically from the path of the .i3d file.')
parser.add_argument('info_file', metavar=('info_file'), type=str, nargs=1, help='Xcompact input .i3d file for the simulation being processed.')
settings = parser.parse_args()
input_file = settings.info_file[0]

this_sim_dir = input_file.replace('input.i3d', '')
if not this_sim_dir:
    this_sim_dir = './'

# load files and metadata
this_sim = Simulation(input_file)
this_ptrn = span_pattern(this_sim, this_sim_dir + '/extra_settings.toml')
ppro_settings = toml.load(this_sim_dir + '/extra_settings.toml')

# get params
# global box size
ny = len(this_sim.y); nz = len(this_sim.z)
# files to read
first_fid = ppro_settings['stats']['fid_lo']
last_fid = ppro_settings['stats']['fid_hi']
fid_list = fid_gen(first_fid, last_fid)
nf = len(fid_list)

# array allocation
# time average: all quantities you're possibly interested in
time_avg = np.zeros((3,nz,ny), dtype=np.float64)

# average everything in time
for fid in fid_list:
    stats, _ = read_stats(this_sim, this_sim_dir+'/stats2d/xstats'+fid)
    time_avg += (stats[:3,:,:]/nf)

# remove mean
time_avg[0,:,:] -= time_avg[0,:,:].mean(axis=0)

# get fft of dispersive motion (you want psd)
# at this point, the mean component is still in
# no worries: this simply gives contribution on mode 0
ft_disp = np.fft.rfft(time_avg[0:3,:,:], axis=1) / nz

# get psd
nfz = np.size(ft_disp,axis=1)
psd_disp = np.zeros((6,ny,nfz), dtype=np.float64)
for ic in range(6):
    i = ic; j = ic # this gets normal Reynolds stresses right
    # correct indices for reynolds stresses
    if ic == 3:
        i = 0; j = 1
    if ic == 4:
        i = 0; j = 2
    if ic == 5:
        i = 1; j = 2
    # calculate psd                
    psd_disp[ic,:,:] = (np.conj(ft_disp[i, :, :]) * ft_disp[j, :, :]).real.transpose()
psd_disp[:,:,1:] *= 2
del ft_disp
# you are using y-symm already when calculating dispersive component; no need to do it again

# save mean and disp
# check existence of output folder
print("Saving to disk...")
out_dir = input_file.replace('input.i3d','raw_ppro_output/')
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
out_dir = out_dir + 'psd/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
# write
psd_disp.tofile(out_dir + '/dispersive_nopa_psd.bin')
