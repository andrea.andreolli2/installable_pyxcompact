from pyxcompact.andreas_stuff.spectra import spectral_tools
from pyxcompact.simulation import Simulation
from itertools import product
from argparse import ArgumentParser
import numpy as np
import toml

parser = ArgumentParser(
    prog = 'bisect_psd_evolution',
    description = 'Reads mode evolution as generated by bisect_mode_evolution (as well as psd of reference cases). Then outputs time scales needed for completion of 95\% of the transient.',
)
parser.add_argument('input_folder', metavar='input.i3d', type=str, nargs=1,
                    help='the folder of the set of simulations to be processed')

##############
# PARAMETERS #
##############
def_end_transient = 0.95 # trainsient is defined as time needed to cover this fraction of the jump between expected psds
def_start_checking = 0.2 # start checking for monotony at this fraction of the jump

# unpack input
inarg = parser.parse_args()
input_folder = inarg.input_folder[0]
base_dir = input_folder + '/'; base_dir = base_dir.replace('//','/')
out_dir = base_dir + 'raw_ppro_output/mode_evolution/'
general_outname = 'psd_evo_bisected'
input_file = base_dir + '0/input.i3d'
rough_input = base_dir.replace('deca_','').replace('form_','') + 'input.i3d'
smooth_input = rough_input.split('r180_s')[0] + 'r180_ref/input.i3d'

# get geometry
sim = Simulation(input_file)
ny = len(sim.y)
meta = toml.load(out_dir + 'mode_time_evolution.nfo')
nt = meta['run_info']['nt']
nr = meta['run_info']['no_realisations']
nzc = meta['run_info']['no_zmodes']
dt = meta['misc']['dt']
s = meta['misc']['s']

# read psd evolution and 99.7% unc
da_shape = (nt,3,ny,nzc)
psd_mean = np.fromfile(out_dir + 'magnitude_mean.bin', dtype = np.float64).reshape(da_shape)
psd_unc99 = 3 * np.fromfile(out_dir + 'magnitude_std.bin', dtype = np.float64).reshape(da_shape) / np.sqrt(nr)

# read psd of smooth and rough sim
r_sim = Simulation(rough_input)
r_sp = spectral_tools(r_sim)
r_psd = r_sp.read_psd(rough_input.replace('input.i3d','raw_ppro_output/psd/psd.bin'))
s_sim = Simulation(smooth_input)
s_sp = spectral_tools(s_sim)
s_psd = s_sp.read_psd(smooth_input.replace('input.i3d','raw_ppro_output/psd/psd.bin'))
base_iz = np.where(s_sp.kz == (np.pi/s))[0][0]
iz_eqv = [(2*ii+1)*base_iz for ii in range(nzc)] # converts index of psd_mean to full psd of vel field

# allocate output array
tscale = np.empty((3,ny,nzc,3), dtype=np.float64) # last index: lower bound, mean, upper bound

# determine if this is decay or formation
# get the associated psd at the initial and final state
# sign_tgt indicates whether the definition of the transient is above (+1) or below (-1) the final psd;
#          eg, for the formation, the time scale is defined as the time needed to cover 95% of the jump
#          that is, the time needed to reach final_psd + (-1)*0.95*jump
if 'deca' in base_dir:
    initial_psd = r_psd
    final_psd = s_psd
    sign_tgt = +1
elif 'form' in base_dir:
    initial_psd = s_psd
    final_psd = r_psd
    sign_tgt = -1

# define function to calculate bisection; it also handles a) no crossing of the target b) non-monotonic behaviour in proximity of the crossing
def get_time_scale(time_seq, dt, ipsd, fpsd, sign_tgt):

    # initialise error report
    errlist = []

    # get reference values of psd
    jump = abs(fpsd-ipsd)
    targe_psd = fpsd + sign_tgt*(1-def_end_transient)*jump
    check_psd = fpsd + sign_tgt*(1-def_start_checking)*jump

    # multiply by sign_tgt, so that time signal is "always decaying"
    targe_psd *= sign_tgt
    check_psd *= sign_tgt
    time_seq *= sign_tgt

    find_final = np.where(time_seq < targe_psd)
    find_check = np.where(time_seq < check_psd)

    # get time instant of end of transient
    if find_final[0].size == 0: # this means, transient never ends
        errlist.append('neverends')
        it_final = 0
    else:
        it_final = find_final[0][0]
    
    # get time instant to check for monotony
    if find_check[0].size == 0: # this means, you never cross the line for check of monotony
        errlist.append('neverstarts')
        it_check = 0
    else:
        it_check = find_check[0][0]

    # get duration of transient
    if not 'neverends' in errlist:
        ts = (it_final+0.5) * dt # this would be: it_final + 1 (first instant of time, index 0, but t=dt) - 0.5 (zero crossing happens in between timesteps)
        # check monotony
        if not 'neverstarts' in errlist:
            for it in range(it_check,it_final):
                if time_seq[it] >= time_seq[it+1]:
                    errlist.append('nonmonotonic')
    else:
        ts = 0

    return ts, errlist
    
# loop over everything (after opening log file)
with open(out_dir+general_outname+'.log','w') as log_file:
    for ic,iy,izc,ibm in product(range(3),range(ny),range(nzc),range(3)):

        tseries = psd_mean[:,ic,iy,izc] + (ibm-1)*psd_unc99[:,ic,iy,izc]
        slice_psd = (ic,iy,iz_eqv[izc],0)
        ipsd = initial_psd[slice_psd]; fpsd = final_psd[slice_psd]

        ts, errlist = get_time_scale(tseries, dt, ipsd, fpsd, sign_tgt)
        tscale[ic,iy,izc,ibm] = ts
        for errcode in errlist:
            log_file.write(f'at %< {ic},{iy},{izc},{ibm} >% (vel component, y index, z-harmonic index, 0-1-2 for lower bound - mean - upper bound) - {errcode}'+'\n')

# write binary file
tscale.tofile(out_dir+general_outname+'.bin')
print("All output written to disk.")
