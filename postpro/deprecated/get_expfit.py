from pyxcompact.andreas_stuff.spectra import spectral_tools
from pyxcompact.andreas_stuff.io_and_averaging import *
from pyxcompact.simulation import Simulation
from pyxcompact.andreas_stuff.exponentials import *
from argparse import ArgumentParser
from itertools import product
import numpy as np
import os, toml

parser = ArgumentParser(
    prog = 'get_expfit',
    description = 'Fits an exponential curve to the time evolution of modes (kx,kz) = (0,[1,...,N]*pi/s) from xcompact statistics files.',
)
parser.add_argument('input_file', metavar='input.i3d', type=str, nargs=1,
                    help='the input file of the simulation to be processed')
parser.add_argument('-n', action='store', help='number of odd subharmonics of fundamental wavenumber to track', required=False, type=int, nargs=1)

# unpack input
inarg = parser.parse_args()
if inarg.n:
    N = int(inarg.n[0])
else:
    N = 10
input_file = inarg.input_file[0]

# process input
base_dir = input_file + '/'
input_file = base_dir + '0/input.i3d'
rough_input = base_dir.replace('deca_','').replace('form_','') + 'input.i3d'

# get simulation and spectral details
sim = Simulation(input_file)
psd = spectral_tools(sim)
# process extra_settings
xtra_set = toml.load(input_file.replace('input.i3d','extra_settings.toml'))
first_fid = xtra_set['mode_history']['first_fid']
last_fid = xtra_set['mode_history']['last_fid']
fid_list = fid_gen(first_fid, last_fid)
nt = len(fid_list)
s = xtra_set['sliplength']['s']
ny = len(sim.y)
# get z index
kz_tgt = np.pi / s
iz_tgt = np.where(psd.kz == kz_tgt)[0]

list_ensemble = [0]
list_ensemble = os.listdir(base_dir)
for el in list_ensemble.copy():
    if not el.isnumeric():
        list_ensemble.remove(el)
no_realisations = len(list_ensemble)

# preallocate arrays
magni = np.zeros((no_realisations,nt,3,ny,N), dtype = np.float64)
exp_param = np.empty((3,ny,N,3), dtype = np.float64)
nzlist = [2*ii+1 for ii in range(N)]

# calculate output
for kk, realisation in enumerate(list_ensemble):
    in_between = realisation + '/'
    for ii, fid in enumerate(fid_list):
        filename = base_dir + in_between + 'stats2d/xstats' + fid
        print('Reading file ' + filename)
        fld, content = stats_as_memmap(sim, filename)
        for ic in range(3):
            for iy in range(ny):
                for iz,nz in enumerate(nzlist):

                    # bottom wall
                    transform = np.fft.rfft(fld[ic,:,iy]) / psd.nz

                    magni[kk,ii,ic,iy,iz] = np.absolute(transform[nz*iz_tgt]) ** 2

# use y-symmetry
# careful: v is antisymmetric in y, but is symmetric!
# careful: for magni, you're not dividing by 2 - see below!
magni[:,:,:,:,:] += magni[:,:,:,::-1,:]
# there should be some multiplication and divisions to do, but they cancel out!
# magni /= 2 # averaging with top wall
# magni *= 2 # real transform - there's a negative wavelength for each one. notice that kz=0 is not saved in magni!

# build time series to match shape of allmag
dt = float(sim.getInputParam('Statistics/ofreq2d')) * float(sim.getInputParam('BasicParam/dt'))
t = ( ( np.arange(nt) + 1 ) * dt ) * np.ones((no_realisations,nt))

# interpolate
print('Interpolating...')
for ic, iy, im in product(range(3),range(ny),range(N)):
    try:
        p, _, _ = exp_fit(t.ravel(), magni[:,:,ic,iy,im].ravel(), exp_full)
        exp_param[ic,iy,im,:] = p[:]
    except:
        exp_param[ic,iy,im,:] = ERRCODE

# check existence of output folder
out_dir = base_dir + 'raw_ppro_output/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
out_dir = out_dir + 'mode_evolution/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)

# write out file
print('Writing to disk...')
exp_param.tofile(out_dir + f'expfit_magnitude.bin')

# collect metadata
run_nfo = {}
relisted = [int(jjj) for jjj in list_ensemble]
run_nfo['run_info'] = {} # stuff about this run of the program
run_nfo['run_info']['iz_tgt'] = int(iz_tgt)
run_nfo['run_info']['no_zmodes'] = int(N)
run_nfo['run_info']['no_realisations'] = int(no_realisations)
run_nfo['run_info']['nt'] = int(nt)
run_nfo['run_info']['min_fold'] = np.amin(relisted)
run_nfo['run_info']['max_fold'] = np.amax(relisted)

run_nfo['misc'] = {} # other stuff - useful for plotting
run_nfo['misc']['type'] = 'decay' if 'deca' in xtra_set['spinoff']['type'] else 'formation'
run_nfo['misc']['s'] = float(s)
run_nfo['misc']['dt'] = dt

# write metadata to file
with open(out_dir + f'mode_expfit.nfo','w') as f:
    toml.dump(run_nfo, f)
