from argparse import ArgumentParser
from pyxcompact.simulation import Simulation
import numpy as np
from cmcrameri import cm
from progressbar import progressbar
import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import toml, os, shutil

parser = ArgumentParser(
    prog = 'vmaker_ox_budg',
    description='Creates a video of the time evolution of the terms of the streamwise dispersive vorticity budget; this is calculated by madman.py, that is - the output of madman.py is needed first.'
    )
parser.add_argument('target_dir', metavar='sim_folder/', type=str, nargs=1, help='the directory containing the repeated simulations')
parser.add_argument('--cclim', '-cc', action='store_true', help='keep color limits constant for the whole video')

# unpack input
settings = parser.parse_args()
base_dir = settings.target_dir[0] + '/'; base_dir = base_dir.replace('//','/')
input_file = base_dir + '0/input.i3d'
const_clim = settings.cclim

# OUTPUT SETTINGS 
#################
# make sure that output dir exists before looping
# results are output in the next session
out_dir = base_dir + 'raw_ppro_output/videos/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
# name for videos
vidname = f'ox_budget{"_cc" if const_clim else ""}'

# load metadata
this_sim = Simulation(input_file)
y = this_sim.y; ny = len(y)
meta = toml.load(base_dir + 'raw_ppro_output/avg/all_you_need.nfo')
ncob = meta['grid_etc']['nterms_o_budg']
nt = meta['grid_etc']['nt']; dt = meta['grid_etc']['dt']
t = (np.arange(nt) + 1) * dt
nzd = meta['grid_etc']['nz']; lzd = meta['grid_etc']['lz']
dz = lzd/nzd; zd = np.arange(nzd+1) * dz

# load arrays and expand them
#############################
# when after reading, last point in z is created (has same value as the first)
# this is convenient for plotting
o_budg = np.empty((nt,ncob,ny,nzd+1))
o_budg_on_disk = np.memmap(base_dir + 'raw_ppro_output/avg/evo_budget_ox.bin', dtype=np.float64, mode='r', shape=(nt,ncob,ny,nzd))
o_budg[:,:,:,:-1] = o_budg_on_disk[:,:,:,:]
o_budg[:,:,:,-1] = o_budg[:,:,:,0]

# plot initialisation
#####################
# get frame
my_dpi = 150
fig, axs = plt.subplots(3,3,figsize=(1920/my_dpi,1080/my_dpi),dpi=my_dpi,gridspec_kw={"width_ratios":[1, 1, 1], "height_ratios":[0.05, 1, 1]})
axs[1,2].remove() # remove unnecessary axis

# useful tuples
axr = (axs[1,0], axs[1,1], axs[2,0], axs[2,1], axs[2,2])
hndls = ["", "", "", "", ""]
titles = ('advection', 'dissipation', "contr. of $\\langle v'w'\\rangle$", "contr. of $\\langle v'v'\\rangle$", "contr. of $\\langle w'w'\\rangle$")

# get top larger ax for colorbar
gs = axs[0,0].get_gridspec()
for ax in axs[0,:]: # remove axes
    ax.remove()
axbig = fig.add_subplot(gs[0,:])

# remove ticks from central axes
for ax in axs[1,1:]:
    ax.xaxis.set_tick_params(labelbottom=False)
    ax.yaxis.set_tick_params(labelleft=False)
axs[1,0].xaxis.set_tick_params(labelbottom=False)
for ax in axs[2,1:]:
    ax.yaxis.set_tick_params(labelleft=False)
# labels for axes
for ax in axs[1:,0]:
    ax.set_ylabel('y')
for ax in axs[-1,:]:
    ax.set_xlabel('z')

# set titles of plot
for ic in range(ncob):
    axr[ic].set_title(titles[ic])

# first plot
############
# needed to setup colorbar
it = 0
# get max
mm = np.amax(np.abs(o_budg[:,:,:,:]))
for ic in range(ncob):
    hndls[ic] = axr[ic].pcolormesh(zd,y,o_budg[it,ic,:,:], shading='gouraud', cmap=cm.bam, vmin=-mm, vmax=mm)
    axr[ic].set_ylim([0,1])
cbar = plt.colorbar(hndls[0],cax=axbig,orientation='horizontal')
axbig.xaxis.set_ticks_position('top')

#######################################
# TIME LOOPING AND ENSEMBLE AVERAGING #
#######################################

for it in progressbar(range(nt)): # at any instant in time...

    if not const_clim:
        mm = np.amax(np.abs(o_budg[it,:,:,:]))
    for ic in range(ncob):
        hndls[ic].set_array(o_budg[it,ic,:,:])
        hndls[ic].set_clim([-mm,mm])
    cbar.update_normal(hndls[0])
    axbig.xaxis.set_ticks_position('top')

    # set title and save
    fig.suptitle(f'$t = {round(t[it],3)}$ (same units as simulation)')
    fname = out_dir + f'{vidname}_frame_{it}'
    fig.savefig(fname + '.png')

# write script to make video
with open(out_dir + 'make_video.sh','w') as f:
    f.write(f'ffmpeg -r 24 -f image2 -i {vidname}_frame_\%d.png -vcodec libx264 -crf 25 -pix_fmt yuv420p {vidname}.mp4\n')
    f.write(f'mkdir src_{vidname}\n')
    f.write(f'mv *.png src_{vidname}\n')

# copy metadata
shutil.copyfile(base_dir + 'raw_ppro_output/avg/all_you_need.nfo', out_dir + f'meta_{vidname}.nfo')