from argparse import ArgumentParser
from pyxcompact.simulation import Simulation
from pyxcompact.andreas_stuff.bisector import bisector
import numpy as np
import toml, os

parser = ArgumentParser(
    prog = 'integralist',
    description='Produces the time history of the in-plane- and volume-averaged dispersive kinetic energy, split into two contributions: tilde{u}^2 and (tilde{v}^2 + tilde{w}^2)/2. Requires the output of madman.py.')
parser.add_argument('target_dir', metavar='sim_folder/', type=str, nargs=1,
                    help='the directory containing the repeated simulations')
parser.add_argument('--steady', '-s', action='store_true', help='use this flag when running this program on a steady state simulation.')

# unpack input
settings = parser.parse_args()
target_dir = settings.target_dir[0]
base_dir = target_dir + '/'; base_dir = base_dir.replace('//','/')
sim_is_steady = settings.steady

# read meta and data
if sim_is_steady:
    sim = Simulation(base_dir + 'input.i3d')
    meta = toml.load(base_dir + 'raw_ppro_output/avg/treeplosix.nfo')
    nt = 1
else:
    sim = Simulation(base_dir + '0/input.i3d')
    meta = toml.load(base_dir + 'raw_ppro_output/avg/all_you_need.nfo')
    nt = meta['grid_etc']['nt']
    dt = meta['grid_etc']['dt']
nzd = meta['grid_etc']['nz']; ny = meta['grid_etc']['ny']
ly = meta['grid_etc']['ly']
ntd = meta['grid_etc']['nterms_disp']
fname_read = (base_dir + 'raw_ppro_output/avg/disp.bin') if sim_is_steady else (base_dir + 'raw_ppro_output/avg/evo_disp.bin')
disp = np.memmap(fname_read, dtype=np.float64, shape=(nt,ntd,ny,nzd))

# make sure that output dir exists before looping
# results are output in the next session
out_dir = base_dir + 'raw_ppro_output/integrals/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)

# array allocation
##################
vol_u = np.zeros((nt), dtype=np.float64)
vol_vw = np.zeros((nt), dtype=np.float64)
plane_u = np.zeros((nt,ny), dtype=np.float64)
plane_vw = np.zeros((nt,ny), dtype=np.float64)

# get averages
##############

# in-plane
plane_u[:,:] = ((disp[:,0,:,:]**2)/2).mean(axis=-1)
plane_vw[:,:] = ((disp[:,1,:,:]**2 + disp[:,2,:,:]**2)/2).mean(axis=-1)

# volume
vol_u = np.trapz(plane_u,x=sim.y,axis=-1) / ly
vol_vw = np.trapz(plane_vw,x=sim.y,axis=-1) / ly

# dump data before bisecting (as this changes sign)
vol_u.tofile(out_dir + 'vol_coherent_u.bin')
vol_vw.tofile(out_dir + 'vol_coherent_vw.bin')
plane_u.tofile(out_dir + 'plane_coherent_u.bin')
plane_vw.tofile(out_dir + 'plane_coherent_vw.bin')

# bisect
########

if not sim_is_steady:

    # setup
    b = bisector()
    if 'deca' in meta['base']['type']:
        sgn_type = 1
    elif 'form' in meta['base']['type']:
        sgn_type = -1
    else:
        raise Exception('unrecognised or absent simulation type (formation/decay) in base parameters of all_you_need.nfo.')
    tscale_plane_u = np.empty((ny))
    tscale_plane_vw = np.empty((ny))
    err_pu = []
    err_pvw = []

    # do the bisection
    for iy in range(ny):
        tscale_plane_u[iy], temp_errlog = b.get_time_scale(plane_u[:,iy], dt, plane_u[0,iy], 0, sgn_type)
        for to_append in [f'iy = {iy}, {the_err}\n' for the_err in temp_errlog]:
            err_pu.append(to_append)
        tscale_plane_vw[iy], temp_errlog = b.get_time_scale(plane_vw[:,iy], dt, plane_vw[0,iy], 0, sgn_type)
        for to_append in [f'iy = {iy}, {the_err}\n' for the_err in temp_errlog]:
            err_pvw.append(to_append)
    tscale_vol_u, err_vu = b.get_time_scale(vol_u, dt, vol_u[0], 0, sgn_type)
    tscale_vol_vw, err_vvw = b.get_time_scale(vol_vw, dt, vol_vw[0], 0, sgn_type)

    # dump tscale
    tscale_plane_u.tofile(out_dir + 'tscale_plane_coherent_u.bin')
    tscale_plane_vw.tofile(out_dir + 'tscale_plane_coherent_vw.bin')

# collect metadata
run_nfo = {}
run_nfo['run_info'] = {} # stuff about this run of the program
run_nfo['run_info'] = meta['run_info']
if not sim_is_steady:
    run_nfo['timescale_volume'] = {}
    run_nfo['timescale_volume']['tscale_vol_coherent_u'] = tscale_vol_u
    run_nfo['timescale_volume']['tscale_vol_coherent_vw'] = tscale_vol_vw

    names_errlogs = ('u, plane', 'vw, plane', 'u, volume', 'vw, volume')
    errlogs = (err_pu, err_pvw, err_vu, err_vvw)
    # write metadata to file
    with open(out_dir + f'integralist.nfo','w') as f:
        toml.dump(run_nfo, f)
        for name, log in zip(names_errlogs, errlogs):
            f.write("\n")
            f.write("\n")
            f.write(f"Error log - {name}\n")
            f.writelines(log)
