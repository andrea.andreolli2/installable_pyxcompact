from argparse import ArgumentParser
from pyxcompact.simulation import Simulation
from pyxcompact.andreas_stuff.io_and_averaging import *
from progressbar import progressbar
import numpy as np
import os

parser = ArgumentParser(description='Calculate the mean flow. First and last snapshots of the series to be used are indicated in extra_settings.toml. File directories are inferred automatically from the path of the .i3d file.')
parser.add_argument('info_file', metavar=('info_file'), type=str, nargs=1, help='Xcompact input .i3d file for the simulation being processed.')
settings = parser.parse_args()
input_file = settings.info_file[0]

this_sim_dir = input_file.replace('input.i3d', '')
if not this_sim_dir:
    this_sim_dir = './'

this_sim = Simulation(input_file)
nx = len(this_sim.x); ny = len(this_sim.y); nz = len(this_sim.z)

allfiles = os.listdir(this_sim_dir)
restart_fids = []
for entry in allfiles:
    if 'restart' in entry and '.info' not in entry:
        restart_fids.append(int(entry.replace('restart','')))
restart_fids.sort()
nt = len(restart_fids)

data = np.zeros((nt,4,ny), dtype=np.float64) # 0 -> mean, 1 -> uu, 2 -> vv, 3 -> ww
temp_slab = np.empty((3,nz,ny,nx),dtype=np.float64)

it = 0
for rel in progressbar(restart_fids):
    fname = this_sim_dir + 'restart' + get_fno(rel, this_sim_dir, 'restart')
    # cheekily read velocity only from restart file
    vrpoint = np.memmap(fname,dtype=np.float64,mode='r',offset=0,shape=(3,nz,ny,nx))
    temp_slab[:] = vrpoint[:]
    data[it,0,:] += temp_slab[0,:,:,:].mean(axis=(0,-1))
    temp_slab *= temp_slab
    data[it,1:4,:] += temp_slab.mean(axis=(1,-1)) # average in x,z
    data[it,1,:] -= data[it,0,:]**2
    it += 1

out_dir = input_file.replace('input.i3d','raw_ppro_output/')
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
out_dir = out_dir + 'avg/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)

data.tofile(out_dir + 'history_meanandvar.bin')