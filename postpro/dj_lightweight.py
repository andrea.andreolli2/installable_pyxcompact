from argparse import ArgumentParser
from pyxcompact.simulation import Simulation
from pyxcompact.andreas_stuff.io_and_averaging import *
from pyxcompact.andreas_stuff.span_pattern import span_pattern
import pyxcompact.andreas_stuff.rogue_derive as rd 
from progressbar import progressbar
import numpy as np
import toml, os

parser = ArgumentParser(
    prog = 'dj_lightweight',
    description='Calculates quantities relative to the triple decomposition of a flow from xcompact3d stats files. First and last snapshots of the series to be used are indicated in extra_settings.toml. File directories are inferred automatically from the path of the .i3d file. Easter egg: https://djtreeplo.com/'
    )
parser.add_argument('info_file', metavar=('info_file'), type=str, nargs=1, help='Xcompact input .i3d file for the simulation being processed.')

# unpack input
settings = parser.parse_args()
input_file = settings.info_file[0]

###################
# FOLDERS AND I/O #
###################

# process input
base_dir = input_file.replace('input.i3d', '')
if not base_dir:
    base_dir = './'

# make sure that output dir exists
out_dir = base_dir + 'raw_ppro_output/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
out_for_psd = out_dir + 'psd/'
out_dir = out_dir + 'avg/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
if not os.path.exists(out_for_psd):
    os.mkdir(out_for_psd)

# load files and metadata
this_sim = Simulation(input_file)
this_ptrn = span_pattern(this_sim, base_dir + 'extra_settings.toml')
xtra_set = toml.load(base_dir + 'extra_settings.toml')

# get number of points in time and corresponding file ids
first_fid = xtra_set['stats']['fid_lo']
last_fid = xtra_set['stats']['fid_hi']
fid_list = fid_gen(first_fid, last_fid)
nf = len(fid_list)

######################################################
# PARAMETERS AND ARRAY ALLOCATION; SETUP DERIVATIVES #
######################################################

# box size
ny = len(this_sim.y); nz = len(this_sim.z)
ly = this_sim.getInputParam("BasicParam/yly"); lz = this_sim.getInputParam("BasicParam/zlz")
s = xtra_set['sliplength']['s']
re = this_sim.getInputParam("BasicParam/re")
nzdisp = this_ptrn.nppp # no. pts. dispersive motion
lzdisp = 2*s

# stats files
##############
#        "u", "v", "w", "p", "uu", "uv", "uw", "vv", "vw", "ww", "pp", "up", "vp", "wp"
#         0    1    2    3    4     5     6     7     8     9     10    11    12    13 
nstat = 14 # number of stats (read from disk); see below

# content of stats that is read to memory (things of which you want a phase average)
#####################################################################################
#        "u", "v", "w", "uu", "uv", "uw", "vv", "vw", "ww"
#         0    1    2    3     4     5     6     7     8   
ncont = 3 # cont -> refers to ens_avg, phase_avg
idx_cont2stats = [0, 1, 2] # ith value of this array is the index of stats corresponding to the ith index of cont
ysymms = [1,  -1,   1] # symmetries to use for ith entry of cont
zsymms = [1,   1,  -1] # symmetries to use for ith entry of cont

# array allocation
##################
ens_avg = np.zeros((ncont,nz,ny), dtype=np.float64) # ensemble average: this does not contain all quantities of stats, but only ncont selected quantities
mean = np.zeros((ny), dtype=np.float64) # y-profile only mean
phase_avg = np.zeros((ncont,ny,nzdisp), dtype=np.float64) # phase average of quantities in ens_avg

############################
# AVERAGING AND PROCESSING #
############################

# 1) get the ensemble average
for fid in progressbar(fid_list):
    stats, _ = stats_as_memmap(this_sim, base_dir+'/stats2d/xstats'+fid)
    for ic,ist in enumerate(idx_cont2stats):
        ens_avg[ic,:,:] += (stats[ist,:,:]/nf)

# 2) get phase average and correct quantitites of interest
for ii in range(ncont):
    phase_avg[ii,:,:], _ = phase_average(ens_avg[ii,:,:],this_sim,this_ptrn,ysymm=ysymms[ii],zsymm=zsymms[ii])
# u - THIS NEEDS TO  BE LAST CORRECTION! Otherwise the two previous ones will fail
mean[:] = phase_avg[0,:,:].mean(axis=1)
phase_avg[0,:,:] -= mean.reshape((-1,1))

# dispersive psd after phase average
ft_disp = np.fft.rfft(phase_avg[0:3,:,:], axis=-1) / nzdisp
nfz = (nzdisp // 2) + 1
psd_disp = np.zeros((6,ny,nfz), dtype=np.float64)
for ic in range(6):
    i = ic; j = ic # this gets normal Reynolds stresses right
    # correct indices for reynolds stresses
    if ic == 3:
        i = 0; j = 1
    if ic == 4:
        i = 0; j = 2
    if ic == 5:
        i = 1; j = 2
    # calculate psd                
    psd_disp[ic,:,:] = (np.conj(ft_disp[i, :, :]) * ft_disp[j, :, :]).real
psd_disp[:,:,1:] *= 2

# 6) save all to disk
mean.tofile(out_dir+'mean.bin')
nterms_dispondisk = 3
disp_ondisk = np.memmap(out_dir+'disp.bin', dtype=np.float64, mode="w+", shape=(nterms_dispondisk,ny,nzdisp))
disp_ondisk[0:3,:,:] = phase_avg[0:3,:,:] # u,v,w
psd_disp.tofile(out_for_psd+'dispersive_psd.bin')


# 7) save metadata
run_nfo = {}

run_nfo['run_info'] = {} # stuff about this run of the program
run_nfo['run_info']['no_files'] = int(nf)
run_nfo['run_info']['min_fid'] = first_fid
run_nfo['run_info']['max_fid'] = first_fid

run_nfo['grid_etc'] = {} # time, space and comp grid
run_nfo['grid_etc']['ny'] = int(ny)
run_nfo['grid_etc']['ly'] = float(ly)
run_nfo['grid_etc']['nz'] = int(nzdisp)
run_nfo['grid_etc']['lz'] = float(lzdisp)
run_nfo['grid_etc']['nterms_disp'] = int(nterms_dispondisk)

run_nfo['base'] = {} # base stuff
run_nfo['base']['type'] = 'decay' if 'deca' in input_file else 'formation'
run_nfo['base']['s'] = float(s)
run_nfo['base']['re'] = float(re)

# write metadata to file
with open(out_dir + 'treeplosix.nfo','w') as f:
    toml.dump(run_nfo, f)
