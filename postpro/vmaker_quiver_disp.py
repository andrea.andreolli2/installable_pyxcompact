from argparse import ArgumentParser
from pyxcompact.simulation import Simulation
from pyxcompact.andreas_stuff.span_pattern import span_pattern
from pyxcompact.andreas_stuff.plot_tools import prequiver
import numpy as np
from cmcrameri import cm
from progressbar import progressbar
import matplotlib
import matplotlib.pyplot as plt
import toml, os, shutil


#########################################################
### HARD SETTTINGS ######################################
labels_for_qtitites = ['\\tilde{u}', '\\tilde{v}', '\\tilde{w}', '\\tilde{\omega}_x']
#########################################################
#########################################################

parser = ArgumentParser(
    prog = 'vmaker_mean_and_disp',
    description='Creates a video of the time evolution of the dispersive velocity and vorticity calculated by madman.py.'
    )
parser.add_argument('target_dir', metavar='sim_folder/', type=str, nargs=1, help='the directory containing the repeated simulations')
parser.add_argument('--cclim', '-cc', action='store_true', help='keep color limits constant for the whole video')
parser.add_argument('--scale', '-s', metavar='2', default=[0.5], type=float, nargs=1, help='scale parameter for the quiver plot (default = 2)')
parser.add_argument('--quiv_no', '-q', metavar='no. quivs', default=[10], type=int, nargs=1, help='number of vectors in the wall-normal direction')

# unpack input
settings = parser.parse_args()
base_dir = settings.target_dir[0] + '/'; base_dir = base_dir.replace('//','/')
input_file = base_dir + '0/input.i3d'
const_clim = settings.cclim
sqale = settings.scale[0]
nqy = settings.quiv_no[0]

# OUTPUT SETTINGS 
#################
# make sure that output dir exists before looping
# results are output in the next session
out_dir = base_dir + 'raw_ppro_output/videos/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
# name for videos
vidname = f'quivered_disp{"_cc" if const_clim else ""}'

# load metadata
this_sim = Simulation(input_file)
spp = span_pattern(this_sim, input_file.replace('input.i3d','/extra_settings.toml'))
y = this_sim.y; ny = len(y)
meta = toml.load(base_dir + 'raw_ppro_output/avg/all_you_need.nfo')
ncdisp = meta['grid_etc']['nterms_disp']
nt = meta['grid_etc']['nt']; dt = meta['grid_etc']['dt']
t = (np.arange(nt) + 1) * dt
nzd = meta['grid_etc']['nz']; lzd = meta['grid_etc']['lz']
dz = lzd/nzd; zd = np.arange(nzd+1) * dz
sll = meta['base']['s']

# load arrays and expand them
#############################
# when after reading, last point in z is created (has same value as the first)
# this is convenient for plotting
mean = np.fromfile(base_dir + 'raw_ppro_output/avg/evo_mean.bin', dtype=np.float64).reshape((nt,ny))
disp = np.empty((nt,ncdisp,ny,nzd+1))
disp_on_disk = np.memmap(base_dir + 'raw_ppro_output/avg/evo_disp.bin', dtype=np.float64, mode='r', shape=(nt,ncdisp,ny,nzd))
disp[:,:,:,:-1] = disp_on_disk[:,:,:,:]
disp[:,:,:,-1] = disp[:,:,:,0]

# convenience stuff
###################

def time_round(fpn, field_width, no_dec):

    dec_part = str(fpn).split('.')[-1]
    int_part = str(fpn).split('.')[0]

    # pad dec_part for safety
    for _ in range(no_dec):
        dec_part = dec_part + '0'

    reversed_output = ''
    for ie in range(no_dec):
        reversed_output = reversed_output + dec_part[no_dec-1-ie]
    reversed_output = reversed_output + '.'
    reversed_output = reversed_output + int_part[::-1]

    for _ in range(field_width-len(reversed_output)):
        reversed_output = reversed_output + '_'

    return reversed_output[::-1]

# plot initialisation
#####################

### EDITABLE PARAMS
# subplots = box that contains all axes. figure is larger than subplots
topbot_pad = 0.11 # top and bottom padding around subplots; units = relative to fig height 
intrapad = 0.15 # vertical spacing between axes of subplots; units = mean axis height (height_ratios_arr.mean() * abs_mainax_height)
height_ratios_arr = np.array([0.05, 1, 0.1]) # relative height of the axes of subplots; units = height of main ax
abs_mainax_heigth = 500 # height of main ax in pixels (dpi is provided below)
abs_side_pad = 100 # side padding around subplots; units = pixels
dpi = 150 # dpi (ppi)
### END EDITABLE PARAMS

# infererd params
# get absolute fig height
totheightsp_relfig = 1-2*topbot_pad
totheightsp_relmainax = np.sum(height_ratios_arr) + 2*intrapad*height_ratios_arr.mean()
rel_mainax_height = totheightsp_relfig/totheightsp_relmainax
fig_height = abs_mainax_heigth/rel_mainax_height
# get absolute fig width
abs_mainax_width = abs_mainax_heigth*2*sll
fig_width = abs_mainax_width + 2*abs_side_pad
#get (relative) side pad
side_pad = abs_side_pad/fig_width

# get plot going
fig, axs = plt.subplots(3,1,figsize=(fig_width/dpi,fig_height/dpi),dpi=dpi,gridspec_kw={"height_ratios":height_ratios_arr})
fig.subplots_adjust(left=(side_pad), right=(1-side_pad), bottom=(topbot_pad), top=(1-topbot_pad), hspace=intrapad)

# fixes to axes
axs[1].tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    labelbottom=False
)
axs[2].tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    left=False,      # ticks along the bottom edge are off
    right=False,         # ticks along the top edge are off
    labelleft=False
)
fig.suptitle('$\\tilde u^+$') # this is actually annotation for colorbar
axs[1].set_ylabel('$y/h$')
axs[2].set_xlabel('$\\zeta/h$')
axs[1].set_xlim([zd[0],zd[-1]])
axs[1].set_ylim([0,1])
axs[2].set_ylim([-0.1,1.2])
axs[2].set_xlim([zd[0],zd[-1]])

# first plot
# needed to setup colorbar
it = 0
# get clims
mm = [np.amax(np.abs(disp[:,ii,:,:])) for ii in range(3)]
# show sliplength pattern at the wall
slarr = np.empty(nzd+1)
slarr[:-1] = spp.span_sl_arr[:nzd]; slarr[-1] = slarr[0]
axs[2].fill_between(zd,slarr,-0.01*np.ones(nzd+1),color='black')
# streamwise pattern
hu = axs[1].pcolormesh(zd,y,disp[it,0,:,:], shading='gouraud', cmap=cm.vik, vmin=-mm[0], vmax=mm[0])
cbu = plt.colorbar(hu,cax=axs[0],orientation='horizontal')
axs[0].xaxis.set_ticks_position('top') # always set this AFTER setup of colorbar
# roll mode (quiver plot)
qz, qy, qw, qv = prequiver(zd,y,disp[it,2,:,:],disp[it,1,:,:],2*nqy)
if sll == 0.25:
    hq = axs[1].quiver(qz, qy, qw, qv, width=0.01) # pivot='mid',
else:
    hq = axs[1].quiver(qz, qy, qw, qv, angles='xy', scale_units='xy', scale=sqale) # pivot='mid',
axs[1].annotate(
        f'$t\;u_\\tau/h = 00.00$',
        xy=(1, 1), xycoords='axes fraction',
        ha="right", va="top",
        color="white",
        xytext=(-10, -10), textcoords='offset points',
        bbox=dict(boxstyle="round", fc="1"),
    )
axs[1].annotate(
        f'$t\;u_\\tau/h =$',
        xy=(1, 1), xycoords='axes fraction',
        ha="right", va="top",
        xytext=(-40, -10), textcoords='offset points',
    )
hann = axs[1].annotate(
        f'${time_round(t[it],4,2)}$',
        xy=(1, 1), xycoords='axes fraction',
        ha="right", va="top",
        xytext=(-10, -11), textcoords='offset points',
    )

#######################################
# TIME LOOPING AND ENSEMBLE AVERAGING #
#######################################

for it in progressbar(range(nt)): # at any instant in time...

    hu.set_array(disp[it,0,:,:])
    if not const_clim:
        hu.set_clim([np.amin(disp[it,0,:,:]),np.amax(disp[it,0,:,:])]); cbu.update_normal(hu)
        axs[0].xaxis.set_ticks_position('top')
    _, _, qw, qv = prequiver(zd,y,disp[it,2,:,:],disp[it,1,:,:],2*nqy)
    hq.set_UVC(qw,qv)
    
    # print time and save
    hann.set_text(f'${time_round(t[it],4,2)}$')
    fname = out_dir + f'{vidname}_frame_{it}'
    fig.savefig(fname + '.png')

# write script to make video
with open(out_dir + 'make_video.sh','w') as f:
    f.write(f'ffmpeg -r 24 -f image2 -i {vidname}_frame_\%d.png -vcodec libx264 -crf 25 -pix_fmt yuv420p {vidname}.mp4\n')
    f.write(f'mkdir src_{vidname}\n')
    f.write(f'mv *.png src_{vidname}\n')

# copy metadata
shutil.copyfile(base_dir + 'raw_ppro_output/avg/all_you_need.nfo', out_dir + f'meta_{vidname}.nfo')
