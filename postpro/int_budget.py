from argparse import ArgumentParser
from pyxcompact.simulation import Simulation
import numpy as np
from progressbar import progressbar
import toml, os, shutil

parser = ArgumentParser(
    prog = 'intcont_budget',
    description='Integrates budget terms (as calculated by madman.py) in time to yield their cumulative contribution at a given point in time.')
parser.add_argument('target_dir', metavar='sim_folder/', type=str, nargs=1, help='the directory containing the repeated simulations')
parser.add_argument('--power', '-p', action='store_true', help='multiplies the budget by the corresponding velocity, so that one gets a power budget')

# unpack input
settings = parser.parse_args()
base_dir = settings.target_dir[0] + '/'; base_dir = base_dir.replace('//','/')
input_file = base_dir + '0/input.i3d'
use_power = settings.power

# output dir should already exist (you're reading from it!)
out_dir = base_dir + 'raw_ppro_output/avg/'
out_base_name = f'int{"_power" if use_power else ""}_budget_u'
out_fname = out_dir + out_base_name + '.bin'

# load metadata
this_sim = Simulation(input_file)
y = this_sim.y; ny = len(y)
meta = toml.load(base_dir + 'raw_ppro_output/avg/all_you_need.nfo')
ncub = meta['grid_etc']['nterms_u_budg']
ncdisp = meta['grid_etc']['nterms_disp']
nt = meta['grid_etc']['nt']; dt = meta['grid_etc']['dt']
nzd = meta['grid_etc']['nz']; lzd = meta['grid_etc']['lz']
dz = lzd/nzd; zd = np.arange(nzd+1) * dz

# load arrays and expand them
#############################

############# the time array is modified wrt usual and contains time zero
# ATTENTION # the budget array is also modified, info at time zero is added
############# info at time zero is zero everywhere

t = (np.arange(nt + 1)) * dt
u_budg = np.empty((nt+1,ncub,ny,nzd)) # input array
u_budg[0,:,:,:] = 0 # time zero is set to zero
u_budg_on_disk = np.memmap(base_dir + 'raw_ppro_output/avg/evo_budget_u.bin', dtype=np.float64, mode='r', shape=(nt,ncub,ny,nzd))
u_budg[1:,:,:,:] = u_budg_on_disk[:,:,:,:]
if use_power:
    disp_on_disk = np.memmap(base_dir + 'raw_ppro_output/avg/evo_disp.bin', dtype=np.float64, mode='r', shape=(nt,ncdisp,ny,nzd))
    u_budg[1:,:,:,:] *= disp_on_disk[:,0,:,:].reshape((nt,1,ny,nzd))

u_int = np.empty((nt,ncub,ny,nzd)) # output array has usual shape! so nothing weird when you read it

# looping
#############################
for iot in progressbar(range(nt)): # loop over time entries of OUTPUT array
    iit = iot+1
    u_int[iot,:,:,:] = np.trapz(u_budg[:(iit+1),:,:,:],x=t[:(iit+1)],axis=0) # integrate in time

# write to disk
u_int.tofile(out_fname)