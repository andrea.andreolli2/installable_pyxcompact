from argparse import ArgumentParser
from mpi4py import MPI
from pyxcompact.simulation import Simulation
import numpy
import pyxcompact.andreas_stuff.spectra as spectra_xcompact
from pyxcompact.andreas_stuff.io_and_averaging import *
import os
import toml
import shutil

parser = ArgumentParser(description='Calculate the spectra from xcompact snapshots; supports MPI. First and last snapshots of the series to be used are indicated in extra_settings.toml. File directories are inferred automatically from the path of the .i3d file.')
parser.add_argument('info_file', metavar=('info_file'), type=str, nargs=1, help='Xcompact input .i3d file for the simulation being processed.')
parser.add_argument('-s', '--symm', action='store', nargs=1, type=float, default=1, required=False, help='Pass 1 for symmetry along y-axis, -1 for antisymmetry, None for no symmetry.')
parser.add_argument('-r', '--use_restart_files', action='store_true', help='Use this flag to read data from restart (not snapshot) files.')
settings = parser.parse_args()
input_file = settings.info_file[0]
symm = settings.symm
use_restart_files = settings.use_restart_files

# parse input file
sim = Simulation(input_file)
nx = len(sim.x); ny = len(sim.y); nz = len(sim.z)
bare = input_file.replace('input.i3d','')
if not bare:
    bare = './'

# parse extra_settings file
ppro_settings = toml.load(input_file.replace('input.i3d','extra_settings.toml'))
if use_restart_files:
    first_fid = int(ppro_settings['restarts']['rfid_lo'])
    last_fid = int(ppro_settings['restarts']['rfid_hi'])
    drf = sim.getInputParam('InOutParam/icheckpoint')
    files = [bare + 'restart' + get_fno(rel, bare, 'restart') for rel in numpy.arange(first_fid,last_fid+drf,drf)]
else:
    first_fid = ppro_settings['snapshots']['first_fid']
    last_fid = ppro_settings['snapshots']['last_fid']
    files = [bare + 'data/uu' + fid for fid in fid_gen(first_fid,last_fid)]

# initialise mpi
comm_world = MPI.COMM_WORLD
np = MPI.Comm.Get_size(comm_world)
my_rank = comm_world.Get_rank()

# get domain partition
my_ny = ny // np
remainder = ny % np
first_iy = 0 # preallocation
if my_rank < remainder: # add one process to the first remainder processes
    my_ny += 1
    first_iy = my_ny * my_rank
else:
    first_iy = ((my_ny + 1)*remainder) + ((my_rank-remainder)*my_ny)
last_iy = first_iy + my_ny # non-inclusive

# create subarray describing data on disk
my_arrslice = MPI.DOUBLE.Create_subarray([nz,ny,nx], [nz,my_ny,nx], [0,first_iy,0], order=MPI.ORDER_C)
my_arrslice.Commit()

# mpi function to read snapshots
def mpi_read_snapshot(this_file, buffer, comp):
    lut_comp = ['ux', 'uy', 'uz']
    to_read = this_file.replace('uu',lut_comp[comp])
    fh = MPI.File.Open(comm_world, to_read)
    fh.Set_view(0, MPI.DOUBLE, my_arrslice)
    fh.Read_all(buffer)
    fh.Close()

# mpi function to read restart
def mpi_read_restart(this_file, buffer, comp):
    fh = MPI.File.Open(comm_world, this_file)
    offset = comp * (nz*ny*nx) * buffer.itemsize
    fh.Set_view(offset, MPI.DOUBLE, my_arrslice)
    fh.Read_all(buffer)
    fh.Close()

# initialise class for spectra
sp = spectra_xcompact.spectral_tools(sim, get_psd_nyloc=my_ny)

mpi_read_vel = mpi_read_restart if use_restart_files else mpi_read_snapshot

# calculate psd
psd = numpy.zeros((6, my_ny, sp.nfz, sp.nfx), dtype = numpy.float64)
u = numpy.empty((nz,my_ny,nx), dtype=numpy.float64); v = numpy.empty((nz,my_ny,nx), dtype=numpy.float64); w = numpy.empty((nz,my_ny,nx), dtype=numpy.float64)
for number,file in enumerate(files):
    if my_rank == 0:
        print("Reading " + file + " (" + str(number+1) + "/" + str(len(files)) + ")" )
    # read field
    for icomp, arr in enumerate((u,v,w)):
        mpi_read_vel(file, arr, icomp)
    # get fft
    sp.get_ft_field(u, v, w)
    sp.get_instant_psd()
    # get psd
    psd += sp.psd
psd /= len(files)

# define slice of psd for saving to disk
my_psdslice = MPI.DOUBLE.Create_subarray([6,ny,sp.nfz,sp.nfx], [6,my_ny,sp.nfz,sp.nfx], [0,first_iy,0,0], order=MPI.ORDER_C)
my_psdslice.Commit()

# check existence of output folder
out_dir = input_file.replace('input.i3d','raw_ppro_output/')
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
out_dir = out_dir + 'psd/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
output_file = out_dir + "psd_" + str(first_fid) + "_" + str(last_fid) + ".bin"
output_final = out_dir + "psd.bin"

# save psd to disk
fh = MPI.File.Open(comm_world, output_file, MPI.MODE_WRONLY|MPI.MODE_CREATE)
fh.Set_view(0, MPI.DOUBLE, my_psdslice)
fh.Write_all(psd)

# apply y-symmetry (if needed)
if symm and my_rank == 0:
    full_psd = sp.read_psd(output_file)
    full_psd += full_psd[:,::-1,:,:]
    full_psd /= 2
    full_psd.tofile(output_file)
    
# move output file to "official" psd.bin
if my_rank == 0:
    shutil.copy(output_file, output_final)

# be polite and say goodbye
if my_rank == 0:
        print("All done. Bye!" )
