from argparse import ArgumentParser
import shutil
from pyxcompact.simulation import Simulation
from pyxcompact.andreas_stuff.span_pattern import span_pattern
from progressbar import progressbar
import numpy as np
from cmcrameri import cm
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import toml, os

#########################################################
### HARD SETTTINGS ######################################
no_modes_toplot = 10
labels_for_qtitites = ['$\\tilde{u}$', '$\\tilde{v}$', '$\\tilde{w}$']
#########################################################
#########################################################


parser = ArgumentParser(
    prog = 'vmaker_disp_psd',
    description='Produces a video of the ensemble-averaged psd of the dispersive motion. First and last snapshots of the series to be used are indicated in extra_settings.toml. File directories are inferred automatically from the path of the .i3d file. Requires the output of madman.py.'
    )
parser.add_argument('target_dir', metavar='sim_folder/', type=str, nargs=1,
                    help='the directory containing the repeated simulations')

# unpack input
settings = parser.parse_args()
target_dir = settings.target_dir[0]

# process input
base_dir = target_dir + '/'; base_dir = base_dir.replace('//','/')
input_file = base_dir + '0/input.i3d'

# load files and metadata
this_sim = Simulation(input_file)
this_ptrn = span_pattern(this_sim, base_dir + '0/extra_settings.toml')

# read output of madman
meta = toml.load(base_dir + 'raw_ppro_output/avg/all_you_need.nfo')
nt = meta['grid_etc']['nt']; nzdisp = meta['grid_etc']['nz']; ny = meta['grid_etc']['ny']
ly = meta['grid_etc']['ly']
dt = meta['grid_etc']['dt']
ntd = meta['grid_etc']['nterms_disp']
disp_on_disk = np.memmap(base_dir + 'raw_ppro_output/avg/evo_disp.bin', dtype=np.float64, shape=(nt,ntd,ny,nzdisp))
phase_avg = disp_on_disk[:,0:3,:,:].copy()
s = meta['base']['s']

# make sure that output dir exists before looping
# results are output in the next session
out_dir = base_dir + 'raw_ppro_output/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
out_dir = out_dir + 'videos/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)

# roughness pattern
sl_pattern = this_ptrn.span_sl_arr
sl_pattern = sl_pattern[0:nzdisp]

# plot initialisation
#####################

# read data from steady rough simulation and get useful params
rin = input_file.replace("deca_","").replace("form_","").replace("0/","")
rsim = Simulation(rin)
dkz = np.pi/s
disp_psd = np.fromfile(rin.replace("input.i3d","raw_ppro_output/psd/dispersive_psd.bin"), dtype=np.float64).reshape((6,ny,-1)) / dkz
nfz = np.size(disp_psd, axis=-1)

# prepare kz for pcolormesh, get y
kz_pc = np.arange(nfz+1) * dkz - (dkz/2)
kz = np.arange(nfz) * dkz
y = this_sim.y
yc = np.empty((len(y)+1))
for iyc in range(1,len(y)):
    yc[iyc] = (y[iyc-1]+y[iyc])/2
yc[0] = -yc[1]
yc[-1] = 2+yc[1]

# prepare psd of spectral pattern for further plotting
psd_sp = np.fft.rfft(sl_pattern-sl_pattern.mean()) / (nzdisp) # notice: mean is removed from spanwise pattern!
psd_sp = np.absolute(psd_sp)**2 * 2 / dkz

# actually get clim
tf, ta = plt.subplots()
clims = np.zeros((3,2))
for count in range(3):
    hpc = ta.pcolormesh(kz_pc,yc,disp_psd[count,:,:],shading='auto')
    clims[count,0], clims[count,1] = hpc.get_clim()
plt.close(tf)


#############################################
# initialise and plot first instant in time #
#############################################

# initialise plot
no_modes_toplot += 1
psd = np.empty((3,ny,nfz),dtype=np.float64)
fig, ax = plt.subplots(3,3,figsize=(10,5),gridspec_kw={"width_ratios":[1, 1, 1],"height_ratios":[0.05, 1, 0.1]})
hndls = ["", "", ""]
cbars = hndls.copy()
it = 0
t = (it + 1)*dt

# do Fourier transform; get psd
for ic in range(3):
    psd[ic,:,:] = np.absolute(np.fft.rfft(phase_avg[it,ic,:,:],axis=-1) / (nzdisp))**2 *2 / dkz
    
# PLOTTING LOOP
# outside of loop: plot roughness pattern
for ic in range(3):
    # main plot
    hndls[ic] = ax[1,ic].pcolormesh(kz_pc,yc,psd[ic,:,:],cmap=cm.batlowW_r,vmin=clims[ic,0],vmax=clims[ic,1],shading='auto')
    ax[2,ic].plot(kz,psd_sp,'k')
    for kk in range(1,3):
        ax[kk,ic].set_xlim([0, dkz + no_modes_toplot * dkz])
    ax[1,ic].set_ylim([0,1])
    ax[1,ic].tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False
    )
    ax[2,ic].tick_params(
        axis='y',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        left=False,      # ticks along the bottom edge are off
        right=False,         # ticks along the top edge are off
        labelleft=False
    )
    if ic == 0:
        ax[1,ic].set_ylabel('$y/h$')
    if ic > 0:
        ax[1,ic].tick_params(
            axis='y',          # changes apply to the x-axis
            which='both',      # both major and minor ticks are affected
            left=False,      # ticks along the bottom edge are off
            right=False,         # ticks along the top edge are off
            labelleft=False
        )
    # all about colorbar
    cbars[ic] = plt.colorbar(hndls[ic],cax=ax[0,ic],orientation="horizontal")
    ax[0,ic].xaxis.set_ticks_position('top')
    # all about psd of roughness pattern
    ax[2,ic].set_xlabel('$h\,\kappa_z$')
    
    ax[1,ic].annotate(f'psd ({labels_for_qtitites[ic]})',(0.75,0.92),xycoords='axes fraction')


################
# TIME LOOPING #
################

for it in progressbar(range(nt)): # at any instant in time...

    t = (it + 1)*dt

    # do Fourier transform; get psd
    for ic in range(3):
        psd[ic,:,:] = np.absolute(np.fft.rfft(phase_avg[it,ic,:,:],axis=-1) / (nzdisp))**2 *2 / dkz
        
    # PLOTTING LOOP
    # outside of loop: plot roughness pattern
    for ic in range(3):
        # main plot
        hndls[ic].set_array(psd[ic,:,:])
        hndls[ic].set_clim(clims[ic,:])
        cbars[ic].update_normal(hndls[ic])
        ax[0,ic].xaxis.set_ticks_position('top')

    # set title and save
    fig.suptitle(f'$t\,u_\\tau/h = {round(t,3)}$')
    fname = out_dir + f'disp_psd_evo_frame_{it}'
    fig.savefig(fname + '.png')

# collect metadata
run_nfo = {}
run_nfo['run_info'] = meta['run_info']

run_nfo['misc'] = {} # other stuff - useful for plotting
run_nfo['misc']['type'] = 'decay' if 'deca' in input_file else 'formation'
run_nfo['misc']['s'] = s
run_nfo['misc']['dt'] = dt

# write metadata to file
with open(out_dir + f'feeplo_disponly.nfo','w') as f:
    toml.dump(run_nfo, f)

# write script to make video
with open(out_dir + 'make_video.sh','w') as f:
    f.write(f'ffmpeg -r 24 -f image2 -i disp_psd_evo_frame_\%d.png -vcodec libx264 -crf 25 -pix_fmt yuv420p disp_psd.mp4\n')
    f.write(f'mkdir src_disp_psd\n')
    f.write('mv *.png src_disp_psd')

# copy metadata
shutil.copyfile(base_dir + 'raw_ppro_output/avg/all_you_need.nfo', out_dir + f'meta_disp_psd.nfo')