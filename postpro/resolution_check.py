from argparse import ArgumentParser
from pyxcompact.simulation import Simulation
from pyxcompact.andreas_stuff.io_and_averaging import *
from pyxcompact.andreas_stuff.span_pattern import span_pattern
from pyxcompact.derive import Derivator
from progressbar import progressbar
import numpy as np
import toml, os

parser = ArgumentParser(
    prog = 'resolution_check',
    description='Calculates the ensemble averaged (time dependent) friction velocity; calculates the maximum friction velocity observed in time, and uses it to calculate the worst-case plus-units resolution. Requires the output of madman.py.'
    )
parser.add_argument('target_dir', metavar='sim_folder/', type=str, nargs=1,
                    help='the directory containing the repeated simulations')
# unpack input
settings = parser.parse_args()
target_dir = settings.target_dir[0]

# process input
base_dir = target_dir + '/'; base_dir = base_dir.replace('//','/')
input_file = base_dir + '0/input.i3d'

# load files and metadata
this_sim = Simulation(input_file)
ppro_settings = toml.load(base_dir + '0/extra_settings.toml')

# read output of madman
meta = toml.load(base_dir + 'raw_ppro_output/avg/all_you_need.nfo')
nt = meta['grid_etc']['nt']; nzd = meta['grid_etc']['nz']; ny = meta['grid_etc']['ny']
ly = meta['grid_etc']['ly']
dt = meta['grid_etc']['dt']
ntd = meta['grid_etc']['nterms_disp']
disp_on_disk = np.memmap(base_dir + 'raw_ppro_output/avg/evo_disp.bin', dtype=np.float64, shape=(nt,ntd,ny,nzd))
mean_on_disk = np.memmap(base_dir + 'raw_ppro_output/avg/evo_mean.bin', dtype=np.float64, shape=(nt,ny))
phase_avg = disp_on_disk[:,0,:,:].copy() + mean_on_disk.copy().reshape(nt,ny,1)

# make sure that output dir exists before looping
# results are output in the next session
out_dir = base_dir + 'raw_ppro_output/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)

# allocate derivator
####################
dy = Derivator.create(this_sim,'y',1)

# get the derivative
dudy = dy.derive(phase_avg,1)
retau = this_sim.input['BasicParam/re']
tw = dudy[:,0,:] / retau
utaumax = np.amax(np.sqrt(tw))

# calculate resolution
dx = this_sim.x[1] - this_sim.x[0]; dz = this_sim.z[1] - this_sim.z[0]
dy_arr = np.zeros(len(this_sim.y)-1)
for ii in range(len(dy_arr)):
    dy_arr[ii] = this_sim.y[ii+1] - this_sim.y[ii]
dyw = np.amin(dy_arr)
dyc = np.amax(dy_arr)

# print output to screen
print(f'Correction factor: {utaumax}')
print(f'Worst-case plus-units resolution:')
print(f'dx --- dz --- dyw --- dyc')
print(f'{dx*retau*utaumax} --- {dz*retau*utaumax} --- {dyw*retau*utaumax} --- {dyc*retau*utaumax}')

################
# WRITE OUTPUT #
################

# collect metadata
run_nfo = {}
run_nfo['run_info'] = meta['run_info']

run_nfo['resolution'] = {} # actual results of this script
run_nfo['resolution']['max_utau'] = utaumax
run_nfo['resolution']['steady_state_plusunits'] = {} # nominal resolution (global steady state utau)
run_nfo['resolution']['steady_state_plusunits']['dx'] = dx*retau
run_nfo['resolution']['steady_state_plusunits']['dz'] = dz*retau
run_nfo['resolution']['steady_state_plusunits']['dyc'] = dyc*retau
run_nfo['resolution']['steady_state_plusunits']['dyw'] = dyw*retau
run_nfo['resolution']['worst_case_plusunits'] = {} # worst case resolution (time and space max utau)
run_nfo['resolution']['worst_case_plusunits']['dx'] = dx*retau*utaumax
run_nfo['resolution']['worst_case_plusunits']['dz'] = dz*retau*utaumax
run_nfo['resolution']['worst_case_plusunits']['dyc'] = dyc*retau*utaumax
run_nfo['resolution']['worst_case_plusunits']['dyw'] = dyw*retau*utaumax

# write metadata to file
with open(out_dir + f'resolution.nfo','w') as f:
    toml.dump(run_nfo, f)