from pyxcompact.andreas_stuff.span_pattern import span_pattern
from pyxcompact.andreas_stuff.io_and_averaging import *
from pyxcompact.simulation import Simulation
from itertools import product
from progressbar import ProgressBar
from argparse import ArgumentParser
import numpy as np
import os
import toml

parser = ArgumentParser(
    prog = 'get_mode_evolution_disponly',
    description = 'Calculates history of the first N harmonics of the dispersive motion from xcompact statistics files. The dispersive (secondary) motion is obtained by time-, streamwise- and phase-averaging the velocity field; additionally, known symmetries of the secondary motion are exploited.',
)
parser.add_argument('target_dir', metavar='input.i3d', type=str, nargs=1,
                    help='the input file of the simulation to be processed')
parser.add_argument('-n', action='store', help='number of odd subharmonics of fundamental wavenumber to track', required=False, type=int, nargs=1)
parser.add_argument('-nzs', '--no_z_symm', action='store_true', help='avoid using spanwise symmetries to calculate phase average')
parser.add_argument('-a', '--all', action='store_true', help='save all modes without gaps.')

# unpack input
inarg = parser.parse_args()
N = int(inarg.n[0]) if inarg.n else 10
use_all_modes = inarg.all if inarg.all else False
target_dir = inarg.target_dir[0]
use_z_symm = not inarg.no_z_symm

# process input
base_dir = target_dir + '/'; base_dir = base_dir.replace('//','/')
input_file = base_dir + '0/input.i3d'

# get simulation and spanwise pattern
sim = Simulation(input_file)
ptrn = span_pattern(sim, input_file.replace('input.i3d','extra_settings.toml'))
# process extra_settings
xtra_set = toml.load(input_file.replace('input.i3d','extra_settings.toml'))
first_fid = xtra_set['time_evo']['first_fid']
last_fid = xtra_set['time_evo']['last_fid']
fid_list = fid_gen(first_fid, last_fid)
nt = len(fid_list)
s = xtra_set['sliplength']['s']
ny = len(sim.y)
nz = len(sim.z)

list_ensemble = os.listdir(base_dir)
for el in list_ensemble.copy():
    if not el.isnumeric():
        list_ensemble.remove(el)
no_realisations = len(list_ensemble)

# preallocate arrays
allrls_ft = np.zeros((no_realisations,nt,3,ny,N), dtype = np.complex128)

# check existence of output folder
out_dir = base_dir + 'raw_ppro_output/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
out_dir = out_dir + 'mode_evolution/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)

# calculate output
with ProgressBar(max_value=len(fid_list)) as pbar:
    for (it, fid), (ir, realisation) in product(enumerate(fid_list), enumerate(list_ensemble)):

        pbar.update(it)

        in_between = realisation + '/'

        filename = base_dir + in_between + 'stats2d/xstats' + fid
        stats, content = stats_as_memmap(sim, filename) # ic, iz, iy

        # get dispersive average
        u_disp, zpa = phase_average(stats[0,:,:]-stats[0,:,:].mean(axis=0),sim,ptrn, zsymm=1, ysymm=1) if use_z_symm else phase_average(stats[0,:,:]-stats[0,:,:].mean(axis=0),sim,ptrn,ysymm=1)
        v_disp, _ = phase_average(stats[1,:,:],sim,ptrn, zsymm=1, ysymm=-1) if use_z_symm else phase_average(stats[1,:,:],sim,ptrn,ysymm=-1)
        w_disp, _ = phase_average(stats[2,:,:],sim,ptrn, zsymm=-1, ysymm=1) if use_z_symm else phase_average(stats[2,:,:],sim,ptrn,ysymm=1)

        nzpa = len(zpa)

        fu = np.fft.rfft(u_disp, axis=-1) / nzpa
        fv = np.fft.rfft(v_disp, axis=-1) / nzpa
        fw = np.fft.rfft(w_disp, axis=-1) / nzpa
        fvel = (fu,fv,fw) 

        for ic in range(3):
            for im in range(N):
                iz = im if use_all_modes else 2*im + 1
                allrls_ft[ir,it,ic,:,im] = fvel[ic][:,iz]

# get mean
expected_ft = allrls_ft.mean(axis=0)
std_re = np.std( np.real(allrls_ft), axis=0 )
std_im = np.std( np.imag(allrls_ft), axis=0 )

# get psd and propagate error
psd = 2 * np.absolute(expected_ft)**2
# psd = 2 (re^2 + im^2)
# var_psd = (d_psd/d_re)^2 * var_re + (d_psd/d_im)^2 * var_im
#         = (4 re)^2 * std_re^2 + (4 im)^2 * std_im^2
#         = (4 re std_re)^2 +(4 im std_im)^2
# std_psd = sqrt(var_psd)
psd_std = np.sqrt( ( (4*np.real(expected_ft)*std_re)**2 ) + ( (4*np.imag(expected_ft)*std_im)**2 ) )

# get angle and propagate error
angle = np.angle(expected_ft)
# angle = atan(im/re) # actually: not really, but yh
# var_angle = (d_angle/d_re)^2 * var_re + (d_angle/d_im)^2 * var_re
#           = (-im/(re^2 + im^2))^2 std_re^2 + (re/(re^2 + im^2))^2 std_im^2
#           = (2*im/psd)^2 std_re^2 + (2*re/psd)^2 std_im^2
#           = (2*im*std_re/psd)^2 + (2*re*std_im/psd)^2
# std_angle = sqrt(var_angle)
angle_std = np.sqrt( ((2*np.imag(expected_ft)*std_re/psd)**2) + ((2*np.real(expected_ft)*std_im)**2) )

# check existence of output folder
out_dir = base_dir + 'raw_ppro_output/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
out_dir = out_dir + 'mode_evolution/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)

# write out file
print('Writing to disk...')
psd.tofile(out_dir + f'magnitude_disponly_mean.bin')
psd_std.tofile(out_dir + f'magnitude_disponly_std.bin')
angle.tofile(out_dir + f'phase_disponly_mean.bin')
angle_std.tofile(out_dir + f'phase_disponly_std.bin')

# collect metadata
run_nfo = {}
relisted = [int(jjj) for jjj in list_ensemble]
run_nfo['run_info'] = {} # stuff about this run of the program
run_nfo['run_info']['no_zmodes'] = int(N)
run_nfo['run_info']['no_realisations'] = int(no_realisations)
run_nfo['run_info']['nt'] = int(nt)
run_nfo['run_info']['min_fold'] = np.amin(relisted)
run_nfo['run_info']['max_fold'] = np.amax(relisted)
run_nfo['run_info']['use_z_symm'] = use_z_symm
run_nfo['run_info']['use_all_modes'] = use_all_modes

run_nfo['misc'] = {} # other stuff - useful for plotting
run_nfo['misc']['type'] = 'decay' if 'deca' in xtra_set['spinoff']['type'] else 'formation'
run_nfo['misc']['s'] = float(s)
run_nfo['misc']['dt'] = float(sim.getInputParam('Statistics/ofreq2d')) * float(sim.getInputParam('BasicParam/dt'))

# write metadata to file
with open(out_dir + f'mode_time_evolution_disponly.nfo','w') as f:
    toml.dump(run_nfo, f)

# be polite and say goodbye
print('Done!')