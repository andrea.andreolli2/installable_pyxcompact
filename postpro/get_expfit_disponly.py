from pyxcompact.andreas_stuff.span_pattern import span_pattern
from pyxcompact.andreas_stuff.io_and_averaging import *
from pyxcompact.andreas_stuff.exponentials import *
from pyxcompact.simulation import Simulation
from argparse import ArgumentParser
from itertools import product
import numpy as np
import os, toml

parser = ArgumentParser(
    prog = 'get_expfit_disponly',
    description = 'Fits an exponential curve to the results of get_mode_evol_disponly.py. This is essentially an alternative way to measure the duration of the transient.',
)
parser.add_argument('target_dir', metavar='input.i3d', type=str, nargs=1,
                    help='the input file of the simulation to be processed')

# unpack input
inarg = parser.parse_args()
target_dir = inarg.target_dir[0]

# process input
base_dir = target_dir + '/'; base_dir = base_dir.replace('//','/')
input_file = base_dir + '0/input.i3d'

# get sizes
sim = Simulation(input_file)
meta = toml.load(base_dir + '/raw_ppro_output/mode_evolution/mode_time_evolution_disponly.nfo')
ny = len(sim.y)
nt = meta['run_info']['nt']
nz = meta['run_info']['no_zmodes']
ssl = meta['misc']['s']
kfz = np.pi / ssl

# read mean psd
da_shape = (nt,3,ny,nz)
mean_psd = np.fromfile(base_dir + '/raw_ppro_output/mode_evolution/magnitude_disponly_mean.bin', dtype = np.float64).reshape(da_shape) / kfz

# allocate stuff for exponentials
exp_par = np.empty((3,ny,nz,2), dtype = np.float64)
# build time series to match shape of allmag
dt = float(sim.getInputParam('Statistics/ofreq2d')) * float(sim.getInputParam('BasicParam/dt'))
t = ( ( np.arange(nt) + 1 ) * dt )
# get exponential function
if 'deca' in meta['misc']['type']:
    ef = exp_decay_disponly
elif 'form' in meta['misc']['type']:
    ef = exp_formation_disponly

# interpolate
print('Interpolating...')
for ic, iy, im in product(range(3),range(ny),range(nz)):
    try:
        p, _, _ = exp_fit(t, mean_psd[:,ic,iy,im], ef)
        exp_par[ic,iy,im,:] = p[:]
    except:
        exp_par[ic,iy,im,:] = ERRCODE
    

# write out file
print('Writing to disk...')
out_dir = base_dir + 'raw_ppro_output/mode_evolution/'
exp_par.tofile(out_dir + f'expfit_magnitude_disponly.bin')
print('Done!')
