from argparse import ArgumentParser
from pyxcompact.simulation import Simulation
from pyxcompact.andreas_stuff.io_and_averaging import *
from progressbar import progressbar
import numpy as np
import toml
import os

parser = ArgumentParser(description='Calculate the mean flow. First and last snapshots of the series to be used are indicated in extra_settings.toml. File directories are inferred automatically from the path of the .i3d file.')
parser.add_argument('info_file', metavar=('info_file'), type=str, nargs=1, help='Xcompact input .i3d file for the simulation being processed.')
parser.add_argument('--read_from_restart', '-r', action='store_true', help='restart files are used to calculate var instead of stats')
settings = parser.parse_args()
input_file = settings.info_file[0]
read_from_restart = settings.read_from_restart

this_sim_dir = input_file.replace('input.i3d', '')
if not this_sim_dir:
    this_sim_dir = './'

ppro_settings = toml.load(this_sim_dir + '/extra_settings.toml')

first_fid = ppro_settings['stats']['fid_lo']
last_fid = ppro_settings['stats']['fid_hi']
fid_list = fid_gen(first_fid, last_fid)
nf = len(fid_list)

this_sim = Simulation(input_file)
nx = len(this_sim.x); ny = len(this_sim.y); nz = len(this_sim.z)

mean = np.zeros((ny), dtype=np.float64)
var = np.zeros((3,ny), dtype=np.float64)

if not read_from_restart:
    for fid in fid_list:
        stats, _ = read_stats(this_sim, this_sim_dir+'/stats2d/xstats'+fid)
        mean += (stats[0,:,:].mean(axis=0)/nf)
        for iva,ist in zip(range(3),[4,7,9]):
            var[iva,:] += stats[ist,:,:].mean(axis=0)/nf

temp_slab = np.empty((3,nz,ny,nx),dtype=np.float64)
if read_from_restart:
    first_rfid = int(ppro_settings['restarts']['rfid_lo'])
    last_rfid = int(ppro_settings['restarts']['rfid_hi'])
    drf = this_sim.getInputParam('InOutParam/icheckpoint')
    rlist = np.arange(first_rfid,last_rfid+drf,drf)
    nf = len(rlist)
    for rel in progressbar(rlist):
        fname = this_sim_dir + 'restart' + get_fno(rel, this_sim_dir, 'restart')
        # cheekily read velocity only from restart file
        vrpoint = np.memmap(fname,dtype=np.float64,mode='r',offset=0,shape=(3,nz,ny,nx))
        temp_slab[:,:,:,:] = vrpoint[:,:,:,:]
        mean += temp_slab[0,:,:,:].mean(axis=(0,-1))/nf
        temp_slab *= temp_slab
        var[:,:] += temp_slab.mean(axis=(1,-1))/nf # average in x,z

var[0,:] -= mean**2

out_dir = input_file.replace('input.i3d','raw_ppro_output/')
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
out_dir = out_dir + 'avg/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)

mean.tofile(out_dir + 'mean.bin')
var.tofile(out_dir + 'var.bin')