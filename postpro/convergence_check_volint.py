import numpy as np
import toml
from pyxcompact.simulation import Simulation
from progressbar import progressbar
from itertools import product
import matplotlib.pyplot as plt
from argparse import ArgumentParser
import os



class file_list_gen:
    
    def __init__(self, first_id, last_id):
        self.fid = int(first_id); self.lid = int(last_id)
        self.idlen = len(last_id)
                
    def __iter__(self):
        self.curr_iter = self.fid - 1
        return self
    
    def __next__(self):
        self.curr_iter += 1
        if self.curr_iter > self.lid:
            raise StopIteration
        new_fid = ''
        trailing = str(self.curr_iter)
        no_pad_zeros = self.idlen - len(trailing)
        for izero in range(no_pad_zeros):
            new_fid = new_fid + '0'
        new_fid = new_fid + trailing
        return new_fid



def get_int_history(target_case, ic, no_samples):

    # read settings (and max time)
    sim = Simulation(target_case + '0/input.i3d')
    nx, ny, nz = sim.getMeshSize()
    xs = toml.load(target_case + '0/extra_settings.toml')
    fid_i = xs['time_evo']['first_fid']
    fid_f = xs['time_evo']['last_fid']
    fid_gen = file_list_gen(fid_i, fid_f)
    
    ens_avg = np.zeros((nz,ny))
    span_avg = np.zeros((ny))
    vol_avg = np.zeros(int(fid_f))
    
    for it,fit in enumerate(progressbar(fid_gen)):
        
        #get ensemble avg
        ens_avg *= 0
        for ismpl in range(no_samples):
            fname = target_case + str(ismpl) + '/stats2d/xstats' + fit
            stat_on_disk = np.memmap(fname,dtype=np.float64,shape=(14,nz,ny))
            ens_avg += stat_on_disk[ic,:,:] / no_samples
        ens_avg -= ens_avg.mean(axis=0)    
        
        span_avg[:] = np.mean(0.5 * ens_avg**2, axis=0)
        vol_avg[it] = np.trapz(span_avg,sim.y) / (sim.y[-1] - sim.y[0])
    
    return vol_avg



############################# actual script



# parse input
parser = ArgumentParser(
    prog = 'convergence_check_volint',
    description='Checks convergence of volume-averaged energy of dispersive motions.'
    )
parser.add_argument('target_dir', metavar='sim_folder/', type=str, nargs=1,
                    help='the directory containing the repeated simulations')
# unpack input
settings = parser.parse_args()
base_dir = settings.target_dir[0]
if not base_dir[-1] == '/':
    base_dir = base_dir + '/'

# determine max no iterations
list_ensemble = os.listdir(base_dir)
for el in list_ensemble.copy():
    if not el.isnumeric():
        list_ensemble.remove(el)
no_samples_max = len(list_ensemble)

# prepare output folder
out_dir = base_dir + 'raw_ppro_output/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
out_dir = out_dir + 'conv_check/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
cmps = ['u','v','w']
addstr = 'mmo' # max, mmo

# do the actual thing
cntr = 1
for ic, no_samples in product(range(3),[no_samples_max-1, no_samples_max]):
    print(f'{cntr}/6')
    vavg = get_int_history(base_dir, ic, no_samples)
    vavg.tofile(base_dir + f'raw_ppro_output/conv_check/volint_{cmps[ic]}_{addstr}.bin')
    if addstr == 'mmo':
        addstr = 'max'
    else:
        addstr = 'mmo'
    cntr += 1
