from argparse import ArgumentParser
from pyxcompact.simulation import Simulation
from pyxcompact.andreas_stuff.io_and_averaging import *
from pyxcompact.andreas_stuff.span_pattern import span_pattern
import pyxcompact.andreas_stuff.rogue_derive as rd 
from progressbar import progressbar
import numpy as np
import toml, os

parser = ArgumentParser(
    prog = 'dj_treeplo',
    description='Calculates quantities relative to the triple decomposition of a flow from xcompact3d stats files. First and last snapshots of the series to be used are indicated in extra_settings.toml. File directories are inferred automatically from the path of the .i3d file. Easter egg: https://djtreeplo.com/'
    )
parser.add_argument('info_file', metavar=('info_file'), type=str, nargs=1, help='Xcompact input .i3d file for the simulation being processed.')
parser.add_argument('--restart', '-r', action='store_true', help='read data from restart files (insead of stat files)')

# unpack input
settings = parser.parse_args()
input_file = settings.info_file[0]
use_restarts = settings.restart

###################
# FOLDERS AND I/O #
###################

# process input
base_dir = input_file.replace('input.i3d', '')
if not base_dir:
    base_dir = './'

# make sure that output dir exists
out_dir = base_dir + 'raw_ppro_output/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
out_for_psd = out_dir + 'psd/'
out_dir = out_dir + 'avg/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
if not os.path.exists(out_for_psd):
    os.mkdir(out_for_psd)

# load files and metadata
this_sim = Simulation(input_file)
this_ptrn = span_pattern(this_sim, base_dir + 'extra_settings.toml')
xtra_set = toml.load(base_dir + 'extra_settings.toml')

# get number of points in time and corresponding file ids
if use_restarts:
    first_fid = int(xtra_set['restarts']['rfid_lo'])
    last_fid = int(xtra_set['restarts']['rfid_hi'])
    drf = this_sim.getInputParam('InOutParam/icheckpoint')
    files = [base_dir + 'restart' + get_fno(rel, base_dir, 'restart') for rel in np.arange(first_fid,last_fid+drf,drf)]
    nf = len(files)
else:
    first_fid = xtra_set['stats']['fid_lo']
    last_fid = xtra_set['stats']['fid_hi']
    fid_list = fid_gen(first_fid, last_fid)
    nf = len(fid_list)

######################################################
# PARAMETERS AND ARRAY ALLOCATION; SETUP DERIVATIVES #
######################################################

# box size
ny = len(this_sim.y); nz = len(this_sim.z); nx = len(this_sim.x)
ly = this_sim.getInputParam("BasicParam/yly"); lz = this_sim.getInputParam("BasicParam/zlz")
s = xtra_set['sliplength']['s']
re = this_sim.getInputParam("BasicParam/re")
nzdisp = this_ptrn.nppp # no. pts. dispersive motion
lzdisp = 2*s

# stats files
##############
#        "u", "v", "w", "p", "uu", "uv", "uw", "vv", "vw", "ww", "pp", "up", "vp", "wp"
#         0    1    2    3    4     5     6     7     8     9     10    11    12    13 
nstat = 14 # number of stats (read from disk); see below

# content of stats that is read to memory (things of which you want a phase average)
#####################################################################################
#        "u", "v", "w", "uu", "uv", "uw", "vv", "vw", "ww"
#         0    1    2    3     4     5     6     7     8   
ncont = 9 # cont -> refers to ens_avg, phase_avg
idx_cont2stats = [0, 1, 2, 4, 5, 6, 7, 8, 9] # ith value of this array is the index of stats corresponding to the ith index of cont
ysymms = [1,  -1,   1,    1,   -1,    1,    1,   -1,    1] # symmetries to use for ith entry of cont
zsymms = [1,   1,  -1,    1,    1,   -1,    1,   -1,    1] # symmetries to use for ith entry of cont

# array allocation
##################
ens_avg = np.zeros((ncont,nz,ny), dtype=np.float64) # ensemble average: this does not contain all quantities of stats, but only ncont selected quantities
mean = np.zeros((ny), dtype=np.float64) # y-profile only mean
phase_avg = np.zeros((ncont,ny,nzdisp), dtype=np.float64) # phase average of quantities in ens_avg
o_x = np.zeros((ny,nzdisp), dtype=np.float64) # streamwise vorticity field
u_derivs = np.zeros((2,ny,nzdisp), dtype=np.float64) # derivatives (y,z) of u
o_derivs = np.zeros((2,ny,nzdisp), dtype=np.float64) # derivatives (y,z) of u
# terms of u budget - LHS means left hand side (same side as time derivative)
# time derivative is not computed
#     RHS                      RHS                RHS            RHS                  RHS                           RHS
#   [ v du/dy + w du/dz ] ~~~ [1/Re lapl(u)] ~~~ [-v dU/dy] ~~~ [d/dy z_avg(uv)] ~~~ [-d/dy (1 - z_avg)<u'v'>] ~~~ [-d/dz <u'w'>]
#    0                         1                  2              3                    4                             5
nterms_u = 6
budg_u = np.zeros((nterms_u,ny,nzdisp), dtype=np.float64)
# terms of omega_x budget - LHS means left hand side (same side as time derivative)
# time derivative is not computed
#     RHS                      RHS                RHS                             RHS                  RHS                        
#   [ v dO/dy + w dO/dz ] ~~~ [1/Re lapl(O)] ~~~ [- (d2/dy2 - d2/dz2)<v'w'>] ~~~ [d2/dydz <v'v'>] ~~~ [-d2/dydz <w'w'>]
#    0                         1                  2                               3                    4                          
nterms_o = 5
budg_o = np.zeros((nterms_o,ny,nzdisp), dtype=np.float64)

# derivatives setup
dy = rd.Derivator.create(this_sim, "y", ny, ly, 1)
dyy = rd.Derivator.create(this_sim, "y", ny, ly, 2, dy)
dz = rd.Derivator.create(this_sim, "z", nzdisp, lzdisp, 1)
dzz = rd.Derivator.create(this_sim, "z", nzdisp, lzdisp, 2)

############################
# AVERAGING AND PROCESSING #
############################

# 1) get the ensemble average
if use_restarts:

        # stats files
        ##############
        #        "u", "v", "w", "p", "uu", "uv", "uw", "vv", "vw", "ww", "pp", "up", "vp", "wp"
        #         0    1    2    3    4     5     6     7     8     9     10    11    12    13 
        #nstat = 14 # number of stats (read from disk); see below
        
        # content of stats that is read to memory (things of which you want a phase average)
        #####################################################################################
        #        "u", "v", "w", "uu", "uv", "uw", "vv", "vw", "ww"
        #         0    1    2    3     4     5     6     7     8   
    
    for trf in progressbar(files):
        # this call to memmap is a bit tricky
        # technically, after the 3 velocity components you have the pressure
        # but pressure has a different grid, so you can't fit it in the same array (different no pts)
        # so you pretend the file terminates earlier
        rest_on_disk = np.memmap(trf, dtype=np.float64, shape=(3,nz,ny,nx))
        ens_avg[0,:,:] += rest_on_disk[0,:,:,:].mean(axis=-1) / nf
        ens_avg[1,:,:] += rest_on_disk[1,:,:,:].mean(axis=-1) / nf
        ens_avg[2,:,:] += rest_on_disk[2,:,:,:].mean(axis=-1) / nf
        ens_avg[3,:,:] += (rest_on_disk[0,:,:,:]**2).mean(axis=-1) / nf
        ens_avg[4,:,:] += (rest_on_disk[0,:,:,:] * rest_on_disk[1,:,:,:]).mean(axis=-1) / nf
        ens_avg[5,:,:] += (rest_on_disk[0,:,:,:] * rest_on_disk[2,:,:,:]).mean(axis=-1) / nf
        ens_avg[6,:,:] += (rest_on_disk[1,:,:,:]**2).mean(axis=-1) / nf
        ens_avg[7,:,:] += (rest_on_disk[1,:,:,:] * rest_on_disk[2,:,:,:]).mean(axis=-1) / nf
        ens_avg[8,:,:] += (rest_on_disk[2,:,:,:]**2).mean(axis=-1) / nf

else:

    for fid in progressbar(fid_list):
    
        stats, _ = stats_as_memmap(this_sim, base_dir+'/stats2d/xstats'+fid)
        for ic,ist in enumerate(idx_cont2stats):
            ens_avg[ic,:,:] += (stats[ist,:,:]/nf)

# 2) get phase average and correct quantitites of interest
for ii in range(ncont):
    phase_avg[ii,:,:], _ = phase_average(ens_avg[ii,:,:],this_sim,this_ptrn,ysymm=ysymms[ii],zsymm=zsymms[ii])
# correct Reynolds stresses
phase_avg[3,:,:] -= phase_avg[0,:,:] * phase_avg[0,:,:]
phase_avg[4,:,:] -= phase_avg[0,:,:] * phase_avg[1,:,:] # <u'v'>
phase_avg[5,:,:] -= phase_avg[0,:,:] * phase_avg[2,:,:] # <u'w'>
phase_avg[6,:,:] -= phase_avg[1,:,:] * phase_avg[1,:,:] # <v'v'>
phase_avg[7,:,:] -= phase_avg[1,:,:] * phase_avg[2,:,:] # <v'w'>
phase_avg[8,:,:] -= phase_avg[2,:,:] * phase_avg[2,:,:] # <w'w'>
# u - THIS NEEDS TO  BE LAST CORRECTION! Otherwise the two previous ones will fail
mean[:] = phase_avg[0,:,:].mean(axis=1)
phase_avg[0,:,:] -= mean.reshape((-1,1))

# dispersive psd after phase average
ft_disp = np.fft.rfft(phase_avg[0:3,:,:], axis=-1) / nzdisp
nfz = (nzdisp // 2) + 1
psd_disp = np.zeros((6,ny,nfz), dtype=np.float64)
for ic in range(6):
    i = ic; j = ic # this gets normal Reynolds stresses right
    # correct indices for reynolds stresses
    if ic == 3:
        i = 0; j = 1
    if ic == 4:
        i = 0; j = 2
    if ic == 5:
        i = 1; j = 2
    # calculate psd                
    psd_disp[ic,:,:] = (np.conj(ft_disp[i, :, :]) * ft_disp[j, :, :]).real
psd_disp[:,:,1:] *= 2

# 3) omega_x and derivatives
o_x = dy.derive(phase_avg[2,:,:],0) - dz.derive(phase_avg[1,:,:],1) # dw/dy - dv/dz
u_derivs[0,:,:] = dy.derive(phase_avg[0,:,:],0); u_derivs[1,:,:] = dz.derive(phase_avg[0,:,:],1)
o_derivs[0,:,:] = dy.derive(o_x,0); o_derivs[1,:,:] = dz.derive(o_x,1)

# 4) budget for u
budg_u[0,:,:] = - phase_avg[1,:,:]*u_derivs[0,:,:] - phase_avg[2,:,:]*u_derivs[1,:,:]
budg_u[1,:,:] = 1/re * ( dyy.derive(phase_avg[0,:,:],0) + dzz.derive(phase_avg[0,:,:],1) )
budg_u[2,:,:] = -phase_avg[1,:,:] * dy.derive(mean,0).reshape((-1,1))
budg_u[3,:,:] = dy.derive((phase_avg[0,:,:]*phase_avg[1,:,:]).mean(axis=1),0).reshape((-1,1))
budg_u[4,:,:] = -dy.derive( phase_avg[4,:,:] - phase_avg[4,:,:].mean(axis=1).reshape((-1,1)), 0)
budg_u[5,:,:] = -dz.derive(phase_avg[5,:,:],1)

# 5) budget for o_x
budg_o[0,:,:] = - phase_avg[1,:,:]*o_derivs[0,:,:] - phase_avg[2,:,:]*o_derivs[1,:,:]
budg_o[1,:,:] = 1/re * ( dyy.derive(o_x,0) + dzz.derive(o_x,1) )
budg_o[2,:,:] = - dyy.derive(phase_avg[7,:,:],0) + dzz.derive(phase_avg[7,:,:],1)
budg_o[3,:,:] = dy.derive( dz.derive(phase_avg[6,:,:], 1), 0)
budg_o[4,:,:] = - dy.derive( dz.derive(phase_avg[8,:,:], 1), 0)

# 6) save all to disk
mean.tofile(out_dir+'mean.bin')
nterms_dispondisk = 8
disp_ondisk = np.memmap(out_dir+'disp.bin', dtype=np.float64, mode="w+", shape=(nterms_dispondisk,ny,nzdisp))
disp_ondisk[0:3,:,:] = phase_avg[0:3,:,:] # u,v,w
disp_ondisk[3,:,:] = o_x
disp_ondisk[4:6,:,:] = u_derivs
disp_ondisk[6:8,:,:] = o_derivs
budg_u.tofile(out_dir+'budget_u.bin')
budg_o.tofile(out_dir+'budget_o.bin')
phase_avg[3:,:,:].tofile(out_dir+'rst.bin')
psd_disp.tofile(out_for_psd+'dispersive_psd.bin')

# 7) save metadata
run_nfo = {}

run_nfo['run_info'] = {} # stuff about this run of the program
run_nfo['run_info']['no_files'] = int(nf)
run_nfo['run_info']['min_fid'] = first_fid
run_nfo['run_info']['max_fid'] = first_fid

run_nfo['grid_etc'] = {} # time, space and comp grid
run_nfo['grid_etc']['ny'] = int(ny)
run_nfo['grid_etc']['ly'] = float(ly)
run_nfo['grid_etc']['nz'] = int(nzdisp)
run_nfo['grid_etc']['lz'] = float(lzdisp)
run_nfo['grid_etc']['nterms_u_budg'] = int(nterms_u)
run_nfo['grid_etc']['nterms_o_budg'] = int(nterms_o)
run_nfo['grid_etc']['nterms_disp'] = int(nterms_dispondisk)

run_nfo['base'] = {} # base stuff
run_nfo['base']['type'] = 'decay' if 'deca' in input_file else 'formation'
run_nfo['base']['s'] = float(s)
run_nfo['base']['re'] = float(re)

# write metadata to file
with open(out_dir + 'treeplosix.nfo','w') as f:
    toml.dump(run_nfo, f)
