from argparse import ArgumentParser
from pyxcompact.simulation import Simulation
from pyxcompact.andreas_stuff.io_and_averaging import *
from pyxcompact.andreas_stuff.span_pattern import span_pattern
import pyxcompact.andreas_stuff.rogue_derive as rd 
from progressbar import progressbar
import numpy as np
import toml, os
#import psutil # DEBUG: memory usage

parser = ArgumentParser(
    prog = 'madman',
    description='Calculates statistics (mainly dispersive, phase averaged) of the time evolving simulations and outputs them to disk. It calculates the mean, dispersive field as well as the vorticity; also, all terms needed for the budget equations of the dispersive streamwise velocity and vorticity.'
    )
parser.add_argument('target_dir', metavar='sim_folder/', type=str, nargs=1,
                    help='the directory containing the repeated simulations')
parser.add_argument('-nzs', '--no_z_symm', action='store_true', help='avoid using spanwise symmetries to calculate phase average')

# unpack input
settings = parser.parse_args()
target_dir = settings.target_dir[0]
use_z_symm = not settings.no_z_symm

###################
# FOLDERS AND I/O #
###################

# process input
base_dir = target_dir + '/'; base_dir = base_dir.replace('//','/')
input_file = base_dir + '0/input.i3d'

# get folders for ensemble average
list_ensemble = os.listdir(base_dir)
for el in list_ensemble.copy():
    if not el.isnumeric():
        list_ensemble.remove(el)
no_realisations = len(list_ensemble)

# make sure that output dir exists
out_dir = base_dir + 'raw_ppro_output/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
out_dir = out_dir + 'avg/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)

# load files and metadata
this_sim = Simulation(input_file)
this_ptrn = span_pattern(this_sim, base_dir + '0/extra_settings.toml')
xtra_set = toml.load(base_dir + '0/extra_settings.toml')

# get number of points in time and corresponding file ids
first_fid = xtra_set['time_evo']['first_fid']
last_fid = xtra_set['time_evo']['last_fid']
fid_list = fid_gen(first_fid, last_fid)
nt = len(fid_list)
dt = float(this_sim.getInputParam('Statistics/ofreq2d')) * float(this_sim.getInputParam('BasicParam/dt')) # timestep

######################################################
# PARAMETERS AND ARRAY ALLOCATION; SETUP DERIVATIVES #
######################################################

# box size
ny = len(this_sim.y); nz = len(this_sim.z)
ly = this_sim.getInputParam("BasicParam/yly"); lz = this_sim.getInputParam("BasicParam/zlz")
s = xtra_set['sliplength']['s']
re = this_sim.getInputParam("BasicParam/re")
nzdisp = this_ptrn.nppp # no. pts. dispersive motion
lzdisp = 2*s

# stats files
##############
#        "u", "v", "w", "p", "uu", "uv", "uw", "vv", "vw", "ww", "pp", "up", "vp", "wp"
#         0    1    2    3    4     5     6     7     8     9     10    11    12    13 
nstat = 14 # number of stats (read from disk); see below

# content of stats that is read to memory (things of which you want a phase average)
#####################################################################################
#        "u", "v", "w", "uu", "uv", "uw", "vv", "vw", "ww"
#         0    1    2    3     4     5     6     7     8   
ncont = 9 # cont -> refers to ens_avg, phase_avg
idx_cont2stats = [0, 1, 2, 4, 5, 6, 7, 8, 9] # ith value of this array is the index of stats corresponding to the ith index of cont
ysymms = [1,  -1,   1,    1,   -1,    1,    1,   -1,    1] # symmetries to use for ith entry of cont
zsymms = [1,   1,  -1,    1,    1,   -1,    1,   -1,    1] # symmetries to use for ith entry of cont

# array allocation
##################
ens_avg = np.zeros((ncont,nz,ny), dtype=np.float64) # ensemble average: this does not contain all quantities of stats, but only ncont selected quantities
mean = np.zeros((ny), dtype=np.float64) # y-profile only mean
phase_avg = np.zeros((ncont,ny,nzdisp), dtype=np.float64) # phase average of quantities in ens_avg
o_x = np.zeros((ny,nzdisp), dtype=np.float64) # streamwise vorticity field
u_derivs = np.zeros((2,ny,nzdisp), dtype=np.float64) # derivatives (y,z) of u
o_derivs = np.zeros((2,ny,nzdisp), dtype=np.float64) # derivatives (y,z) of u
# terms of u budget - LHS means left hand side (same side as time derivative)
# time derivative is not computed
#     RHS                      RHS                RHS            RHS                  RHS                           RHS
#   [ v du/dy + w du/dz ] ~~~ [1/Re lapl(u)] ~~~ [-v dU/dy] ~~~ [d/dy z_avg(uv)] ~~~ [-d/dy (1 - z_avg)<u'v'>] ~~~ [-d/dz <u'w'>]
#    0                         1                  2              3                    4                             5
nterms_u = 6
budg_u = np.zeros((nterms_u,ny,nzdisp), dtype=np.float64)
# terms of omega_x budget - LHS means left hand side (same side as time derivative)
# time derivative is not computed
#     RHS                      RHS                RHS                             RHS                  RHS                        
#   [ v dO/dy + w dO/dz ] ~~~ [1/Re lapl(O)] ~~~ [- (d2/dy2 - d2/dz2)<v'w'>] ~~~ [d2/dydz <v'v'>] ~~~ [-d2/dydz <w'w'>]
#    0                         1                  2                               3                    4                          
nterms_o = 5
budg_o = np.zeros((nterms_o,ny,nzdisp), dtype=np.float64)

# derivatives setup
dy = rd.Derivator.create(this_sim, "y", ny, ly, 1)
dyy = rd.Derivator.create(this_sim, "y", ny, ly, 2, dy)
dz = rd.Derivator.create(this_sim, "z", nzdisp, lzdisp, 1)
dzz = rd.Derivator.create(this_sim, "z", nzdisp, lzdisp, 2)

###########################
# MEMMAPS TO OUTPUT FILES #
###########################
mean_ondisk = np.memmap(out_dir+'evo_mean.bin', dtype=np.float64, mode="w+", shape=(nt,ny))
# dispondisk
# u_disp ~~~ v_disp ~~~ w_disp ~~~ omega_x_disp ~~~ d/dy u_disp ~~~ d/dz u_disp ~~~ d/dy o_x ~~~ d/dz o_x
#  0          1          2          3                4               5               6            7
nterms_dispondisk = 8
disp_ondisk = np.memmap(out_dir+'evo_disp.bin', dtype=np.float64, mode="w+", shape=(nt,nterms_dispondisk,ny,nzdisp))
ubudg_ondisk = np.memmap(out_dir+'evo_budget_u.bin', dtype=np.float64, mode="w+", shape=(nt,nterms_u,ny,nzdisp))
obudg_ondisk = np.memmap(out_dir+'evo_budget_ox.bin', dtype=np.float64, mode="w+", shape=(nt,nterms_o,ny,nzdisp))
rst_ondisk = np.memmap(out_dir+'evo_rst.bin', dtype=np.float64,mode="w+",shape=(nt,6,ny,nzdisp))

#######################################
# TIME LOOPING AND ENSEMBLE AVERAGING #
#######################################

for it in progressbar(range(nt)): # at any instant in time...

    fid = fid_list[it]
    t = (it+1)*dt

    # 1) get the ensemble average
    ens_avg[:,:,:] = 0
    for ens_dir in list_ensemble:
        stats, _ = stats_as_memmap(this_sim, base_dir+ens_dir+'/stats2d/xstats'+fid)
        for ic,ist in enumerate(idx_cont2stats):
            ens_avg[ic,:,:] += (stats[ist,:,:]/no_realisations)

    # 2) get phase average and correct quantitites of interest
    for ii in range(ncont):
        phase_avg[ii,:,:], _ = phase_average(ens_avg[ii,:,:],this_sim,this_ptrn,ysymm=ysymms[ii],zsymm=zsymms[ii]) if use_z_symm else phase_average(ens_avg[ii,:,:],this_sim,this_ptrn,ysymm=ysymms[ii])
    # correct Reynolds stresses
    phase_avg[3,:,:] -= phase_avg[0,:,:] * phase_avg[0,:,:]
    phase_avg[4,:,:] -= phase_avg[0,:,:] * phase_avg[1,:,:] # <u'v'>
    phase_avg[5,:,:] -= phase_avg[0,:,:] * phase_avg[2,:,:] # <u'w'>
    phase_avg[6,:,:] -= phase_avg[1,:,:] * phase_avg[1,:,:] # <v'v'>
    phase_avg[7,:,:] -= phase_avg[1,:,:] * phase_avg[2,:,:] # <v'w'>
    phase_avg[8,:,:] -= phase_avg[2,:,:] * phase_avg[2,:,:] # <w'w'>
    # u - THIS NEEDS TO  BE LAST CORRECTION! Otherwise the two previous ones will fail
    mean[:] = phase_avg[0,:,:].mean(axis=1)
    phase_avg[0,:,:] -= mean.reshape((-1,1))

    # 3) omega_x and derivatives
    o_x = dy.derive(phase_avg[2,:,:],0) - dz.derive(phase_avg[1,:,:],1) # dw/dy - dv/dz
    u_derivs[0,:,:] = dy.derive(phase_avg[0,:,:],0); u_derivs[1,:,:] = dz.derive(phase_avg[0,:,:],1)
    o_derivs[0,:,:] = dy.derive(o_x,0); o_derivs[1,:,:] = dz.derive(o_x,1)

    # 4) budget for u
    budg_u[0,:,:] = - phase_avg[1,:,:]*u_derivs[0,:,:] - phase_avg[2,:,:]*u_derivs[1,:,:]
    budg_u[1,:,:] = 1/re * ( dyy.derive(phase_avg[0,:,:],0) + dzz.derive(phase_avg[0,:,:],1) )
    budg_u[2,:,:] = -phase_avg[1,:,:] * dy.derive(mean,0).reshape((-1,1))
    budg_u[3,:,:] = dy.derive((phase_avg[0,:,:]*phase_avg[1,:,:]).mean(axis=1),0).reshape((-1,1))
    budg_u[4,:,:] = -dy.derive( phase_avg[4,:,:] - phase_avg[4,:,:].mean(axis=1).reshape((-1,1)), 0)
    budg_u[5,:,:] = -dz.derive(phase_avg[5,:,:],1)

    # 5) budget for o_x
    budg_o[0,:,:] = - phase_avg[1,:,:]*o_derivs[0,:,:] - phase_avg[2,:,:]*o_derivs[1,:,:]
    budg_o[1,:,:] = 1/re * ( dyy.derive(o_x,0) + dzz.derive(o_x,1) )
    budg_o[2,:,:] = - dyy.derive(phase_avg[7,:,:],0) + dzz.derive(phase_avg[7,:,:],1)
    budg_o[3,:,:] = dy.derive( dz.derive(phase_avg[6,:,:], 1), 0)
    budg_o[4,:,:] = - dy.derive( dz.derive(phase_avg[8,:,:], 1), 0)

    # 6) save everything to disk
    mean_ondisk[it,:] = mean
    disp_ondisk[it,0:3,:,:] = phase_avg[0:3,:,:] # u,v,w
    disp_ondisk[it,3,:,:] = o_x
    disp_ondisk[it,4:6,:,:] = u_derivs
    disp_ondisk[it,6:8,:,:] = o_derivs
    ubudg_ondisk[it,:,:,:] = budg_u
    obudg_ondisk[it,:,:,:] = budg_o
    rst_ondisk[it,:,:,:] = phase_avg[3:,:,:]
    
#    # DEBUG: memory usage
#    print(f"Memory usage in MB: {psutil.Process(os.getpid()).memory_info().rss / 1024 ** 2}")

# collect metadata
run_nfo = {}
relisted = [int(jjj) for jjj in list_ensemble]

run_nfo['run_info'] = {} # stuff about this run of the program
run_nfo['run_info']['no_realisations'] = int(no_realisations)
run_nfo['run_info']['min_fold'] = np.amin(relisted)
run_nfo['run_info']['max_fold'] = np.amax(relisted)

run_nfo['grid_etc'] = {} # time, space and comp grid
run_nfo['grid_etc']['nt'] = int(nt)
run_nfo['grid_etc']['dt'] = dt
run_nfo['grid_etc']['ny'] = int(ny)
run_nfo['grid_etc']['ly'] = float(ly)
run_nfo['grid_etc']['nz'] = int(nzdisp)
run_nfo['grid_etc']['lz'] = float(lzdisp)
run_nfo['grid_etc']['nterms_u_budg'] = int(nterms_u)
run_nfo['grid_etc']['nterms_o_budg'] = int(nterms_o)
run_nfo['grid_etc']['nterms_disp'] = int(nterms_dispondisk)

run_nfo['base'] = {} # base stuff
run_nfo['base']['type'] = 'decay' if 'deca' in input_file else 'formation'
run_nfo['base']['s'] = float(s)
run_nfo['base']['re'] = float(re)

# write metadata to file
with open(out_dir + 'all_you_need.nfo','w') as f:
    toml.dump(run_nfo, f)

