from argparse import ArgumentParser
from pyxcompact.simulation import Simulation
import numpy as np
from cmcrameri import cm
from progressbar import progressbar
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import toml, os, shutil

#########################################################
### HARD SETTTINGS ######################################
labels_for_qtitites = ['\\tilde{u}', '\\tilde{v}', '\\tilde{w}', '\\tilde{\omega}_x']
#########################################################
#########################################################

parser = ArgumentParser(
    prog = 'vmaker_mean_and_disp',
    description='Creates a video of the time evolution of the dispersive velocity and vorticity calculated by madman.py.'
    )
parser.add_argument('target_dir', metavar='sim_folder/', type=str, nargs=1, help='the directory containing the repeated simulations')
parser.add_argument('--cclim', '-cc', action='store_true', help='keep color limits constant for the whole video')

# unpack input
settings = parser.parse_args()
base_dir = settings.target_dir[0] + '/'; base_dir = base_dir.replace('//','/')
input_file = base_dir + '0/input.i3d'
const_clim = settings.cclim

# OUTPUT SETTINGS 
#################
# make sure that output dir exists before looping
# results are output in the next session
out_dir = base_dir + 'raw_ppro_output/videos/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
# name for videos
vidname = f'mean_and_disp{"_cc" if const_clim else ""}'

# load metadata
this_sim = Simulation(input_file)
y = this_sim.y; ny = len(y)
meta = toml.load(base_dir + 'raw_ppro_output/avg/all_you_need.nfo')
ncdisp = meta['grid_etc']['nterms_disp']
nt = meta['grid_etc']['nt']; dt = meta['grid_etc']['dt']
t = (np.arange(nt) + 1) * dt
nzd = meta['grid_etc']['nz']; lzd = meta['grid_etc']['lz']
dz = lzd/nzd; zd = np.arange(nzd+1) * dz

# load arrays and expand them
#############################
# when after reading, last point in z is created (has same value as the first)
# this is convenient for plotting
mean = np.fromfile(base_dir + 'raw_ppro_output/avg/evo_mean.bin', dtype=np.float64).reshape((nt,ny))
disp = np.empty((nt,ncdisp,ny,nzd+1))
disp_on_disk = np.memmap(base_dir + 'raw_ppro_output/avg/evo_disp.bin', dtype=np.float64, mode='r', shape=(nt,ncdisp,ny,nzd))
disp[:,:,:,:-1] = disp_on_disk[:,:,:,:]
disp[:,:,:,-1] = disp[:,:,:,0]

# plot initialisation
#####################
# get frame
my_dpi = 150
fig, axs = plt.subplots(4,3,figsize=(1920/my_dpi,1080/my_dpi),dpi=my_dpi,gridspec_kw={"width_ratios":[0.3, 1, 1], "height_ratios":[0.05, 1, 1, 0.05]})
for ax in axs[1:3,1]:
    ax.xaxis.set_tick_params(labelbottom=False)
    ax.yaxis.set_tick_params(labelleft=False)
for ax in axs[1:3,2]:
    ax.xaxis.set_tick_params(labelbottom=False)
    ax.yaxis.set_tick_params(labelleft=False)

gs = axs[1,0].get_gridspec()
# remove axes
for ax in axs[:,0]:
    ax.remove()
axbig = fig.add_subplot(gs[1:3,0])
axbig.yaxis.set_tick_params(labelleft=False)

# first plot
# needed to setup colorbar
it = 0
hp = axbig.plot(mean[it,:],y)
# get clims
mm = [np.amax(np.abs(disp[:,ii,:,:])) for ii in range(4)]
hu = axs[1,1].pcolormesh(zd,y,disp[it,0,:,:], shading='gouraud', cmap=cm.broc, vmin=-mm[0], vmax=mm[0]); cbu = plt.colorbar(hu,cax=axs[0,1],orientation='horizontal')
hv = axs[1,2].pcolormesh(zd,y,disp[it,1,:,:], shading='gouraud', cmap=cm.cork, vmin=-mm[1], vmax=mm[1]); cbv = plt.colorbar(hv,cax=axs[0,2],orientation='horizontal')
for ax in axs[0,1:]:
    ax.xaxis.set_ticks_position('top')
hw = axs[2,1].pcolormesh(zd,y,disp[it,2,:,:], shading='gouraud', cmap=cm.vik, vmin=-mm[2], vmax=mm[2]); cbw = plt.colorbar(hw,cax=axs[3,1],orientation='horizontal')
ho = axs[2,2].pcolormesh(zd,y,disp[it,3,:,:], shading='gouraud', cmap=cm.bam, vmin=-mm[3], vmax=mm[3]); cbo = plt.colorbar(ho,cax=axs[3,2],orientation='horizontal')

#######################################
# TIME LOOPING AND ENSEMBLE AVERAGING #
#######################################

for it in progressbar(range(nt)): # at any instant in time...

    hp[0].set_xdata(mean[it,:])
    hu.set_array(disp[it,0,:,:])
    hv.set_array(disp[it,1,:,:])
    if not const_clim:
        hu.set_clim([np.amin(disp[it,0,:,:]),np.amax(disp[it,0,:,:])]); cbu.update_normal(hu)
        hv.set_clim([np.amin(disp[it,1,:,:]),np.amax(disp[it,1,:,:])]); cbv.update_normal(hv)
        for ax in axs[0,1:]:
            ax.xaxis.set_ticks_position('top')
    hw.set_array(disp[it,2,:,:])
    ho.set_array(disp[it,3,:,:])
    if not const_clim:
        hw.set_clim([np.amin(disp[it,2,:,:]),np.amax(disp[it,2,:,:])]); cbw.update_normal(hw)
        ho.set_clim([np.amin(disp[it,3,:,:]),np.amax(disp[it,3,:,:])]); cbo.update_normal(ho)
    
    # set title and save
    fig.suptitle(f'$t = {round(t[it],3)}$ (same units as simulation)')
    fname = out_dir + f'{vidname}_frame_{it}'
    fig.savefig(fname + '.png')

# write script to make video
with open(out_dir + 'make_video.sh','w') as f:
    f.write(f'ffmpeg -r 24 -f image2 -i {vidname}_frame_\%d.png -vcodec libx264 -crf 25 -pix_fmt yuv420p {vidname}.mp4\n')
    f.write(f'mkdir src_{vidname}\n')
    f.write(f'mv *.png src_{vidname}\n')

# copy metadata
shutil.copyfile(base_dir + 'raw_ppro_output/avg/all_you_need.nfo', out_dir + f'meta_{vidname}.nfo')
