from argparse import ArgumentParser
from pyxcompact.simulation import Simulation
from pyxcompact.andreas_stuff.io_and_averaging import *
from pyxcompact.andreas_stuff.span_pattern import span_pattern
import pyxcompact.andreas_stuff.spectra as spectra
from progressbar import progressbar
from itertools import product
import numpy as np
from cmcrameri import cm
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import toml, os

#########################################################
### HARD SETTTINGS ######################################
qtities_of_interest = ['uu','vv','ww']
labels_for_qtitites = ['\\langle\\hat{u}^{\\dagger}\\hat{u}(\\kappa_x=0)\\rangle', '\\langle\\hat{v}^{\\dagger}\\hat{v}(\\kappa_x=0)\\rangle', '\\langle\\hat{w}^{\\dagger}\\hat{w}(\\kappa_x=0)\\rangle']
indeces_in_stats = [0,1,2]
#########################################################
#########################################################

parser = ArgumentParser(
    prog = 'get_feeplo',
    description='Produces a video of the evolution of the kx=0 mode of the Fourier transform of the velocity; a second panel also shows the same evolution as a difference with the reference case. First and last snapshots of the series to be used are indicated in extra_settings.toml. File directories are inferred automatically from the path of the .i3d file. Spinoff of dj_treeplo.py'
    )
parser.add_argument('target_dir', metavar='sim_folder/', type=str, nargs=1,
                    help='the directory containing the repeated simulations')

# unpack input
settings = parser.parse_args()
target_dir = settings.target_dir[0]

# process input
base_dir = target_dir + '/'; base_dir = base_dir.replace('//','/')
input_file = base_dir + '0/input.i3d'

# load files and metadata
this_sim = Simulation(input_file)
this_ptrn = span_pattern(this_sim, base_dir + '0/extra_settings.toml')
ppro_settings = toml.load(base_dir + '0/extra_settings.toml')

# get number of points in time and corresponding file ids
xtra_set = toml.load(input_file.replace('input.i3d','extra_settings.toml'))
first_fid = xtra_set['time_evo']['first_fid']
last_fid = xtra_set['time_evo']['last_fid']
fid_list = fid_gen(first_fid, last_fid)
nt = len(fid_list)
dt = float(this_sim.getInputParam('Statistics/ofreq2d')) * float(this_sim.getInputParam('BasicParam/dt')) # timestep

# get folders for ensemble average
list_ensemble = os.listdir(base_dir)
for el in list_ensemble.copy():
    if not el.isnumeric():
        list_ensemble.remove(el)
no_realisations = len(list_ensemble)

# get params
# global box size
ny = len(this_sim.y); nz = len(this_sim.z)

# make sure that output dir exists before looping
# results are output in the next session
out_dir = base_dir + 'raw_ppro_output/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
out_dir = out_dir + 'videos/'
if not os.path.exists(out_dir):
    os.mkdir(out_dir)

# array allocation
##################
# time average: all quantities you're possibly interested in
nzft = nz//2 + 1
ens_psd = np.zeros((3,ny,nzft), dtype=np.float64)
read_buf = np.empty((ny,nz),dtype=np.float64)

# roughness pattern 
sl_pattern = this_ptrn.span_sl_arr*xtra_set['sliplength']['lp']


# plot initialisation
#####################

# first off: plot the secondary motion
# to get clim for pcolormesh

# load rough files
rin = input_file.replace("deca_","").replace("form_","").replace("0/","")
rsim = Simulation(rin)
rsp = spectra.spectral_tools(rsim)
# read and slice - shape of psd: psd[component,iy,iz,ix]
rpsd = rsp.read_psd(rin.replace('input.i3d','raw_ppro_output/psd/psd.bin'))[:3,:,:,0]
# premultiply and divide by spectral resolution
rpsd *= rsp.kz / rsp.dkz

# load smooth files
sin = rin.split('r180_')[0] + 'r180_ref/input.i3d'
ssim = Simulation(sin)
ssp = spectra.spectral_tools(ssim)
# read and slice - shape of psd: psd[component,iy,iz,ix]
spsd = ssp.read_psd(sin.replace('input.i3d','raw_ppro_output/psd/psd.bin'))[:3,:,:,0]
# premultiply and divide by spectral resolution
spsd *= ssp.kz / ssp.dkz

# psd of pattern
sl_psd = 2 * np.abs(np.fft.rfft(sl_pattern) / nz)**2
sl_psd *= ssp.kz / ssp.dkz

# initialise plot; get clims
clims = np.zeros((3,2,2)) # (components, plots_of_interest, lo_hi)
# plotting loop
fig, ax = plt.subplots(3,2,figsize=(14,7),gridspec_kw={"height_ratios":[0.05, 1, 0.1]})
for ic in range(3):
    # plot psd of span pattern
    for ip in range(2):
        ax[2,ip].pcolormesh(2*np.pi/rsp.kz[1:],[0,1],np.ones((2,1))*sl_psd[1:],shading='gouraud',cmap='gray_r')
        ax[2,ip].set_xscale('log')
    # main plot (absolute)
    hmain = ax[1,0].pcolormesh(2*np.pi/rsp.kz[1:],rsim.y,rpsd[ic,:,1:],shading='gouraud',cmap=cm.batlowW_r)
    ax[1,0].set_ylim([0,1])
    ax[1,0].set_xscale('log')
    # alt plot (relative)
    diff = rpsd - spsd
    cc = np.amax(np.abs(diff.ravel()))
    clims[ic,1,0] = -cc; clims[ic,1,1] = cc
    halt = ax[1,1].pcolormesh(2*np.pi/rsp.kz[1:],rsim.y,diff[ic,:,1:],shading='gouraud',cmap=cm.bam,vmin=clims[ic,1,0],vmax=clims[ic,1,1])
    ax[1,1].set_ylim([0,1])
    ax[1,1].set_xscale('log')
    # disable horizontal ticks on main and alt
    for ia in range(2):
        ax[1,ia].tick_params( # disable ticks for main axis
                axis='x',          # changes apply to the x-axis
                which='both',      # both major and minor ticks are affected
                bottom=False,      # ticks along the bottom edge are off
                top=False,         # ticks along the top edge are off
                labelbottom=False
            )
    # plot colorbars
    plt.colorbar(hmain, cax=ax[0,0], orientation="horizontal")
    plt.colorbar(halt, cax=ax[0,1], orientation="horizontal")
    ax[0,0].xaxis.set_ticks_position('top'); ax[0,1].xaxis.set_ticks_position('top')
    # labels
    for ia in range(2):
        ax[2,ia].set_xlabel("$\lambda_z/h$")
    ax[1,0].set_ylabel("$y/h$")
    ax[0,0].set_title("absolute")
    ax[0,1].set_title("relative")
    fig.suptitle(f"$\kappa_z\\,{labels_for_qtitites[ic]}^+")
    # save the clims
    clims[ic,0,0], clims[ic,0,1] = hmain.get_clim()

#######################################
# TIME LOOPING AND ENSEMBLE AVERAGING #
#######################################

fig, ax = plt.subplots(3,2,figsize=(14,7),gridspec_kw={"height_ratios":[0.05, 1, 0.1]})

for it in progressbar(range(nt)): # at any instant in time...

    fid = fid_list[it]
    t = (it+1)*dt

    # 1) get the ensemble average
    ens_psd[:,:,:] = 0
    for ens_dir in list_ensemble:
        stats, _ = stats_as_memmap(this_sim, base_dir+ens_dir+'/stats2d/xstats'+fid)
        for ic in range(3):
            read_buf = stats[ic,:,:].transpose()
            ens_psd[ic,:,:] += (2 * np.abs(np.fft.rfft(read_buf,axis=-1)/nz)**2) / no_realisations
    # make symmetric
    ens_psd += ens_psd[:,::-1,:]
    ens_psd /= 2
    # premultiply
    for ic in range(3):
        ens_psd[ic,:,:] *= ssp.kz / ssp.dkz

    # 2) plot
    for ic in range(3):
        # to be on the safe side, clear all axes
        for ir,ip in product(range(3),range(2)):
            ax[ir,ip].clear()
        # plot psd of span pattern
        for ip in range(2):
            ax[2,ip].pcolormesh(2*np.pi/rsp.kz[1:],[0,1],np.ones((2,1))*sl_psd[1:],shading='gouraud',cmap='gray_r')
            ax[2,ip].set_xscale('log')
        # main plot (absolute)
        hmain = ax[1,0].pcolormesh(2*np.pi/rsp.kz[1:],rsim.y,ens_psd[ic,:,1:],shading='gouraud',cmap=cm.batlowW_r,vmin=clims[ic,0,0],vmax=clims[ic,0,1])
        ax[1,0].set_ylim([0,1])
        ax[1,0].set_xscale('log')
        # alt plot (relative)
        diff = ens_psd - spsd
        halt = ax[1,1].pcolormesh(2*np.pi/rsp.kz[1:],rsim.y,diff[ic,:,1:],shading='gouraud',cmap=cm.bam,vmin=clims[ic,1,0],vmax=clims[ic,1,1])
        ax[1,1].set_ylim([0,1])
        ax[1,1].set_xscale('log')
        # disable horizontal ticks on main and alt
        for ia in range(2):
            ax[1,ia].tick_params( # disable ticks for main axis
                    axis='x',          # changes apply to the x-axis
                    which='both',      # both major and minor ticks are affected
                    bottom=False,      # ticks along the bottom edge are off
                    top=False,         # ticks along the top edge are off
                    labelbottom=False
                )
        # plot colorbars
        plt.colorbar(hmain, cax=ax[0,0], orientation="horizontal")
        plt.colorbar(halt, cax=ax[0,1], orientation="horizontal")
        ax[0,0].xaxis.set_ticks_position('top'); ax[0,1].xaxis.set_ticks_position('top')
        # to be on the safe side, forcefully set the xlims
        for ir,ip in product(range(1,3),range(2)):
            ax[ir,ip].set_xlim([2*np.pi/rsp.kz[-1],2*np.pi/rsp.kz[1]])
        # labels
        for ia in range(2):
            ax[2,ia].set_xlabel("$\lambda_z/h$")
        ax[1,0].set_ylabel("$y/h$")
        ax[0,0].set_title("absolute")
        ax[0,1].set_title("relative")
        fig.suptitle(f"$\kappa_z\\,{labels_for_qtitites[ic]}^+\,(t\,u_\\tau/h = {round(t,3)})$")
        # set title and save
        fname = out_dir + f'fideo_frame_{qtities_of_interest[ic]}_{it}'
        fig.savefig(fname + '.png')

# collect metadata
run_nfo = {}
relisted = [int(jjj) for jjj in list_ensemble]
run_nfo['run_info'] = {} # stuff about this run of the program
run_nfo['run_info']['no_realisations'] = int(no_realisations)
run_nfo['run_info']['nt'] = int(nt)
run_nfo['run_info']['min_fold'] = np.amin(relisted)
run_nfo['run_info']['max_fold'] = np.amax(relisted)

run_nfo['misc'] = {} # other stuff - useful for plotting
run_nfo['misc']['type'] = 'decay' if 'deca' in input_file else 'formation'
run_nfo['misc']['s'] = float(xtra_set['sliplength']['s'])
run_nfo['misc']['dt'] = dt

# write metadata to file
with open(out_dir + f'feeplo.nfo','w') as f:
    toml.dump(run_nfo, f)

# write script to make video
with open(out_dir + 'make_video.sh','w') as f:
    for q in qtities_of_interest:
        f.write(f'ffmpeg -r 24 -f image2 -i fideo_frame_{q}_\%d.png -vcodec libx264 -crf 25 -pix_fmt yuv420p psd_{q}.mp4\n')
    f.write('rm *.png')
