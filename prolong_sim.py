from argparse import ArgumentParser
import os

# MANUAL SETTINGS
#########################################
first_iter_fid = 10001
last_iter_fid = 16000
#########################################

# parse arguments
parser = ArgumentParser(description='Prolongs the duration of a specified simulation')
parser.add_argument('info_file', metavar=('info_file'), type=str, nargs=1, help='Xcompact input .i3d file for the simulation being processed.')
settings = parser.parse_args()
input_file = settings.info_file[0]

# check existence of needed restart file
restart_is_there = False
base_dir = input_file.replace('input.i3d','')
if not base_dir:
    base_dir = './'
flist = os.listdir(base_dir)
for f in flist:
    if 'restart' in f and not '.info' in f:
        fid = int(f.replace('restart',''))
        if fid+1 == first_iter_fid:
            restart_is_there = True
if not restart_is_there:
    print(f'WARNING - missing restart file! Cwd: {os.getcwd()} - input file: {input_file}')

# read input file
with open(input_file,'r') as i3f:
    lines = i3f.readlines()

# do requested changes
for ii,line in enumerate(lines):
    if 'ifirst =' in line:
        lines[ii] = f'ifirst = {first_iter_fid}           ! First iteration\n'
    if 'ilast =' in line:
        lines[ii] = f'ilast = {last_iter_fid}      ! Last iteration\n'

# rewrite input file
print(f"Rewriting file {input_file}")
os.remove(input_file)
with open(input_file,'w') as inif:
    inif.writelines(lines)