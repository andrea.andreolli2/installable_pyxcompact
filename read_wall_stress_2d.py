from pyxcompact.simulation import Simulation, MultiSimulation
from pyxcompact.filechecker import Stats2DManager, MonitorManager, CheckpointManager, ypManager, BCManager
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path

sim = Simulation("/media/data/ISTM/wall_friction/variable/checkerboard/input.i3d")
sim.compute_coordinates()
a = Stats2DManager(sim, "x")
b = CheckpointManager(sim)
#c = ypManager(sim)


res = a.TimeAverageInterval(500, 6000).computeReynoldsStresses()

print(f"Total wall stress: {res.getWallStress()}")
wall_stress = res.getWallStress(flatten=False, degree=sim.getInputParam('BasicParam/ibc_shear_stress'))
print(f"Row-wise wall stress: {wall_stress}")
#print(res.getWallStress(flatten=False))
bc = BCManager(sim)
bc.files[0].loadFile()
re_cent = 1 / res.get_nu()
re = sim.getInputParam('BasicParam/re')
bdudy = re**2 / re_cent * res.get_nu()

expected = np.average(bc.files[0].contents["ubc"], 0) * bdudy
print(f"Expected: {expected}")

print(f"Allclose? {np.allclose(expected, wall_stress)}")

plt.figure()
#plt.plot(sim.y, res.contents["<u>"])
a.files[0].loadFile()
#plt.imshow(b.files[0].contents["ux"][1])
plt.plot(sim.y, res.contents["<u>"])
plt.show()

