from pyxcompact.simulation import Simulation, MultiSimulation
from pyxcompact.filechecker import Stats2DManager, MonitorManager, CheckpointManager, PlanesManager
from pyxcompact.simulationfile import WallStressBCFile
from pyxcompact.andreas_stuff.span_pattern import span_pattern
import matplotlib.pyplot as plt
import numpy as np
from argparse import ArgumentParser
import toml

# parse command line input
parser = ArgumentParser(description='Generate the slip length boundary condtion input file.')
parser.add_argument('sim_input_file', metavar=('input.i3d'), type=str, nargs=1, help='Path to input.i3d file of current simulation.')
settings = parser.parse_args()
sim_input_file = settings.sim_input_file[0]

# read parameters from extra_settings.toml
extra_settings = toml.load(sim_input_file.replace('input.i3d','extra_settings.toml'))
retau = extra_settings['sliplength']['retau']
s = extra_settings['sliplength']['s']
lp = extra_settings['sliplength']['lp']

# fetch simulation details and number of points
sim  = Simulation(sim_input_file)
n = {coord : sim.getInputParam(f"BasicParam/n{coord}") for coord in ["x", "y", "z"]}

# initialise array
w = WallStressBCFile(None, sim, False, "w")
_, _, lz = sim.getPhysicalSize()

# get spanwise pattern
print(s,lz,n['z'])
sp = span_pattern(sim,sim_input_file.replace('input.i3d','extra_settings.toml'))

# print to screen
print("lz, s, no. strips, pts. per strip:", lz, s, sp.ns, sp.npps)
print("(1 strip = half period)")

# write file
array = sp.get_bc_array(n['x'],n['z'],lp/retau)
w.contents = {"wbc" : array}
w.isLoaded = True
w.writeFile(overwrite=True)
print(w.CheckForPlausibility(sim))
print(array)
