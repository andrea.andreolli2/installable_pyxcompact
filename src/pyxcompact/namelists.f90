  NAMELIST /BasicParam/ p_row, p_col, nx, ny, nz, istret, beta, xlx, yly, zlz, &
       itype, iin, re, ray, u1, u2, init_noise, inflow_noise, &
       dt, ifirst, ilast, irestart, &
       numscalar, iibm, ilmn, &
       ilesmod, iscalar, &
       nclx1, nclxn, ncly1, nclyn, nclz1, nclzn, &
       ivisu, ipost, &
       gravx, gravy, gravz, &
       icpg, icfr, iopen, &
       wrotation,spinup_time, &
       ithetaform, &
       ibc_shear_stress, ibc_shear_stress_read_dudy, ibc_shear_stress_read_dwdy, ibc_shear_stress_u, &
       ibc_shear_stress_w, ibc_slip_length_u, ibc_slip_length_w
  NAMELIST /NumOptions/ ifirstder, isecondder, itimescheme, nu0nu, cnu, fpi2, ipinter
  NAMELIST /InOutParam/ icheckpoint, ioutput, nvisu, iprocessing, &
       istatx,istatz,istat3d, &
       ofplane2d, isavevel2d, isavepre2d, isavephi2d, &
       nplx2d,nply2d,nplz2d, &
       imonitor, fmonitor, iprobes, fprobes, nprobes
  NAMELIST /Statistics/ initstat, nstat, &
       sfreq2d,ofreq2d,sfreq3d,ofreq3d
  NAMELIST /Visu2D/ xp_plx2d, yp_ply2d, zp_plz2d
  NAMELIST /ScalarParam/ sc, ri, uset, cp, &
       nclxS1, nclxSn, nclyS1, nclySn, nclzS1, nclzSn, &
       scalar_lbound, scalar_ubound, &
       phibcy1, phibcyn
  NAMELIST /LESModel/ jles, smagcst, walecst, maxdsmagcst, iwall
  NAMELIST /WallModel/ smagwalldamp
  NAMELIST /Tripping/ itrip,A_tr,xs_tr_tbl,ys_tr_tbl,ts_tr_tbl,x0_tr_tbl
  NAMELIST /ibmstuff/ cex,cey,ra,nobjmax,nraf,nvol,iforces
  NAMELIST /ForceCVs/ xld, xrd, yld, yud
  NAMELIST /LMN/ dens1, dens2, prandtl, ilmn_bound, ivarcoeff, ilmn_solve_temp, &
       massfrac, mol_weight, imultispecies, primary_species, &
       Fr, ibirman_eos
  NAMELIST /Probes/ xyzprobes
  NAMELIST /CASE/ tgv_twod, pfront