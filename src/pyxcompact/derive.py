import numpy as np
from numba import jit
import pyxcompact.simulation as sim
import matplotlib.pyplot as plt
import einsum2
class Derivator:
    def create(sim, direction : str, order : int, firstderivator=None):
        key = "first" if order == 1 else "second"
        if sim.getInputParam(f"NumOptions/i{key}der") == 4:
            return SixthOrderCompact(sim, direction, order, firstderivator)
        else:
            return NotImplemented
    
    def __init__(self, sim, direction, order, firstderivator=None):
        self.sim = sim
        self.direction = direction
        self.order = order
        if (sim.getInputParam(f"BasicParam/istret") != 0 and direction == "y" and order == 2):
            if firstderivator is None:
                raise ValueError("Second order derivator with ystretching requires a first order derivator as optional parameter")
        self.firstderivator = firstderivator
        self.GetMatrices()
    def getBoundaryType(self, left=True):
        key = f"BasicParam/ncl{self.direction}{'1' if left else 'n'}"
        return self.sim.getInputParam(key)
    def GetMatrices(self):
        # Finite compact difference schemes work as follows (for both first and second derivative, where dx becomes dx^2):
        # alpha * f'(i-1) + f'(i) + alpha * f'(i+1) = 1/dx *[a * (f(i+1) - f(i-1)) + b * (f(i+2) - f(i-2)) + ...]
        # This means we can generally describe the system as
        # A f' = B f, where A, B are diagonal / tridiagonal / pentadiagonal / ... matrices (except for the first and last column(s), i.e. boundary treatment)
        # In order to generalize the boundary treatment, the subclasses prepare the vectors that serve as entries alpha, a, b, ... as follows:
        # self.alpha (self.r) = [[boundary_0], [boundary_1], [boundary_2]], where boundary_i are lists of vectors of length leftstencil (rightstencil) that will be convolved with f' / f'' (f)
        # The first vector in the list corresponds to the first row (last row), the second entry to the second (second-to-last) row and so on.
        # boundary_0 corresponds to symmetric boundary = non-boundary nodes, boundary_1 = open channel (free surface flow), boundary_2 = Dirichlet bc (no-slip condition)
        
        n = self.sim.getInputParam(f"BasicParam/n{self.direction}")
        A = np.zeros((n, n))
        B = np.zeros((n, n))
        # fill up the bulk
        def fill(array, stencil, i, flipstencil=False, flipsign=False):
            if (flipstencil):
                stencil = stencil[::-1]
            stencilsize = len(stencil)
            for j in range(stencilsize):
                offset = (stencilsize - 1)//2
                array[i, (i+j-offset) % array.shape[1]] = stencil[j] * (-1 if flipsign else 1)
        for i in range(n):
            fill(A, self.alpha[0][0], i)
            fill(B, self.r[0][0], i)
        # boundary treatment
        for boundary in [True, False]:
            index_transform = lambda index: index if boundary else -(1+index)
            bl = int(self.getBoundaryType(boundary))
            for i in range(len(self.alpha[bl])):
                fill(A, self.alpha[bl][i], index_transform(i), not(boundary))
            for i in range(len(self.r[bl])):
                fill(B, self.r[bl][i], index_transform(i), not(boundary), not(boundary) and self.order == 1)
        # multiply the right hand side matrix with 1/dx for first order and 1/dx² for second order
        dx = self.sim.getInputParam(f"BasicParam/{self.direction}l{self.direction}") / self.sim.getInputParam(f"BasicParam/n{self.direction}")
        if (self.direction == "y"):
            dx = self.sim.getInputParam(f"BasicParam/yly") / (self.sim.getInputParam("BasicParam/ny") - 1)
        self.dx = dx
        bfac = 1.0/(dx if self.order==1 else dx**2) 

        B = B * bfac
        self.A = A
        self.B = B
        # we can now compute the derivative by Af' = B f -> f' = A^-1 B f = C f  with C = A^-1 B
        # it could be accelerated a bit using the Thomas algorithm... but for postprocessing that's probably overkill.
        self.C = np.linalg.inv(A) @ B
        self.ppy, self.pp2y, self.pp4y = sim.ystretching(self.sim.getInputParam("BasicParam/yly"), self.sim.getInputParam("BasicParam/ny"),
                             self.sim.getInputParam("BasicParam/beta"), self.sim.getInputParam("BasicParam/istret"), True)[0:3]
        
    def derive(self, array, axis):
        if array.shape[axis] != self.C.shape[0]:
            raise ValueError("Wrong array shape for derivator")
        # build the einsum syntax. we want the following: ij,kjm->kim, j being at position axis. 
        r_ein = list("klm")[0:array.ndim]
        r_ein[axis] = "j"
        r_ein = "".join(r_ein)
        out_ein = r_ein.replace("j", "i")
        syntax = f"ij,{r_ein}->{out_ein}" 
        result = einsum2.einsum2(syntax, self.C, array)
        if (self.direction == 'y'):
            slc = [None for i in range(array.ndim)]
            slc[axis] = slice(None)
            slc = tuple(slc)
            #print(result)
            if (self.order == 1):
                result = result * self.ppy[slc]
            else:
                first_derivative = self.firstderivator.derive(array, axis)
                result = result * self.pp2y[slc] - first_derivative * self.pp4y[slc]
        return result

class SixthOrderCompact(Derivator):
    def __init__(self, sim, direction, order, firstderivator = None):
        self.sim = sim
        fpi2 = sim.getInputParam("NumOptions/fpi2")
        self.leftstencil = 3
        
        if (order == 1):
            self.rightstencil = 5
            self.alpha = [[[1.0/3, 1, 1.0/3]], [], [[0, 1, 2.0], [1.0/4, 1, 1.0/4]]]
            self.r = [[[-1.0/36, -7.0/9, 0, 7.0/9, 1.0/36]], [], [[0, 0, -5.0/2, 2.0, 1.0/2], [0, -3.0/4, 0, 3.0/4, 0]]]
        elif (order == 2):
            self.rightstencil = 7
            alpha_base = (45.0*fpi2*np.pi**2-272.0)/(90.0*fpi2*np.pi**2 - 416.0)
            self.alpha = [[[alpha_base, 1, alpha_base]], [], [[0, 1.0, 11.0], [1.0/10, 1, 1.0/10], [2.0/11, 1, 2.0/11], [2.0/11, 1, 2.0/11]]]
            a = (6.0-9.0*alpha_base)/4
            b = (-3.0+24*alpha_base)/20
            c = (2.0-11.0*alpha_base)/180
            def fill_r(av, bv, cv):
                #print(-2*(av + bv + cv))
                return [cv, bv, av, -2*(av + bv + cv), av, bv, cv]
            self.r = [[fill_r(a, b, c)], [], [[0, 0, 0, 13.0, -27.0, 15.0, -1.0], [0, 0, 6.0/5, -12.0/5, 6.0/5, 0, 0], fill_r(12.0/11, 3.0/44, 0),  fill_r(12.0/11, 3.0/44, 0)]]
        else:
            raise ValueError("Order must be 1 or 2")
        super().__init__(sim, direction, order, firstderivator)

    
np.set_printoptions(precision=4, linewidth=100000)

def test_derivative_y():
    sim = simulation.Simulation("/media/lds/tests/input.i3d")
    sim.compute_coordinates()
    dy = Derivator.create(sim, "y", 1)
    dyy = Derivator.create(sim, "y", 2, dy)
    print(dy.A)
    x = np.linspace(0, 8, 17)
    y = sim.y
    field = np.sin(y)
    print(dy.B.dot(field))
    print(dyy.B)
    #res = np.linalg.inv(dy.A).dot(dy.B.dot(field))
    deriv = dyy.derive(field, 0)
    plt.plot(y, dy.derive(field, 0), label="f'")
    plt.plot(y, dyy.derive(field, 0), label="f''")
    plt.plot(y, field, label="f")
    plt.plot(y, np.gradient(field, y, edge_order=2), label="numpy f'")
    plt.plot(y, np.gradient(np.gradient(field, y, edge_order=2), y, edge_order=2), label="numpy f''")
    plt.plot(y, np.cos(y), "*", label="exact f'")
    plt.plot(y, -np.sin(y), "*", label="exact f''")
    plt.legend()

    plt.savefig("output/example sine.png")
    plt.show()
def test_derivative_x():
    sim = Simulation("/media/lds/tests/input.i3d")
    sim.compute_coordinates()
    dx = Derivator.create(sim, "x", 1)
    dxx = Derivator.create(sim, "x", 2, dx)
    print(dx.A)
    x = sim.x
    field = np.sin(x/8*np.pi*2)
    field2 = np.zeros((len(sim.y), len(sim.x)))
    field = field[None, :] + field2
    print(dxx.B)
    plt.imshow(dx.C)
    plt.figure()
    plt.plot(x, dx.derive(field, 1)[1], label="f'")
    plt.plot(x, dxx.derive(field, 1)[1], label="f''")
    field = field[0, :]
    plt.plot(x, field, label="f")
    plt.plot(x, np.gradient(field, x, edge_order=2), label="numpy f'")
    plt.plot(x, np.gradient(np.gradient(field, x, edge_order=2), x, edge_order=2), label="numpy f''")
    plt.plot(x, 1/4 * np.pi * np.cos(x/8*np.pi*2), "*", label="exact f'")
    plt.plot(x, (1/4 * np.pi)**2 * -np.sin(x/8*np.pi*2), "*", label="exact f''")
    plt.legend()
    plt.show()

#test_stretching()
#test_derivative_x()
#test_derivative_y()


#plt.show()