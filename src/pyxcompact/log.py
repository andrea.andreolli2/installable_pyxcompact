from enum import Enum

class OrderedEnum(Enum):
     def __ge__(self, other):
         if self.__class__ is other.__class__:
             return self.value >= other.value
         return NotImplemented
     def __gt__(self, other):
         if self.__class__ is other.__class__:
             return self.value > other.value
         return NotImplemented
     def __le__(self, other):
         if self.__class__ is other.__class__:
             return self.value <= other.value
         return NotImplemented
     def __lt__(self, other):
         if self.__class__ is other.__class__:
             return self.value < other.value
         return NotImplemented
class LogLevel(OrderedEnum):
    Verbose = 0
    Warning = 1
    Error = 2

class TerminalColors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class Log:
    def __init__(self, LogLevel):
        self.LogLevel = LogLevel
    def warn(self, msg):
        if self.LogLevel <= LogLevel.Warning:
            self.print(msg,TerminalColors.WARNING)
    def error(self, msg):
        self.print(msg, TerminalColors.FAIL)
    def info(self, msg):
        if self.LogLevel == LogLevel.Verbose:
            print(msg)
    def info_heading(self, msg):
        if self.LogLevel == LogLevel.Verbose:
            self.print(msg, TerminalColors.HEADER)
    def print(self, msg, color):
        print(color + msg + TerminalColors.ENDC)

log = Log(LogLevel.Warning)