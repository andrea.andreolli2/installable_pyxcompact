ro = 99999999._mytype
  angle = zero
  u1 = 2
  u2 = 1
  init_noise = zero
  inflow_noise = zero
  iin = 0
  itimescheme = 4
  istret = 0
  ipinter=3
  beta = 0
  iscalar = 0
  cont_phi = 0
  filepath = './data/'
  irestart = 0
  itime0 = 0
  t0 = zero
  datapath = './data/'
  fpi2 = (48._mytype / seven) / (PI**2)

  !! IBM stuff
  nraf = 0
  nobjmax = 0

  nvol = 0
  iforces = 0
  itrip = 0
  wrotation = zero
  irotation = 0
  itest=1

  !! Gravity field
  gravx = zero
  gravy = zero
  gravz = zero

  !! LMN stuff
  ilmn = .FALSE.
  ilmn_bound = .TRUE.
  pressure0 = one
  prandtl = one
  dens1 = one
  dens2 = one
  ivarcoeff = .FALSE.
  npress = 1 !! By default people only need one pressure field
  ilmn_solve_temp = .FALSE.
  imultispecies = .FALSE.
  Fr = zero
  ibirman_eos = .FALSE.

  primary_species = -1

  !! Channel
  icpg = 0
  icfr = 1
  iopen = 0

  !! Statistics
  istatistics = 0
  istatx  = 0
  istatz  = 0
  istat2d = 0
  istat3d = 0
  sfreq2d = huge(i)
  ofreq2d = huge(i)
  sfreq3d = huge(i)
  ofreq3d = huge(i)

  ! monitor
  imonitor = 0
  fmonitor = huge(i)

  ! monitor
  iprobes = 0
  fprobes = huge(i)
  nprobes = 0

  !! IO
  ivisu = 1
  ipost = 0
  iprocessing = huge(i)
  initstat = huge(i)

  save_ux = 0
  save_uy = 0
  save_uz = 0
  save_phi = 0
  save_uxm = 0
  save_uym = 0
  save_uzm = 0
  save_phim = 0
  save_w = 0
  save_w1 = 0
  save_w2 = 0
  save_w3 = 0
  save_qc = 0
  save_pc = 0
  save_V = 0
  save_dudx = 0
  save_dudy = 0
  save_dudz = 0
  save_dvdx = 0
  save_dvdy = 0
  save_dvdz = 0
  save_dwdx = 0
  save_dwdy = 0
  save_dwdz = 0
  save_dphidx = 0
  save_dphidy = 0
  save_dphidz = 0
  save_pre = 0
  save_prem = 0
  save_dmap = 0
  save_utmap = 0
  save_ibm = 0

  ! VISU2D
  ivisu2d = 0
  isavevel2d = 0
  isavepre2d = 0
  isavephi2d = 0
  nplx2d = 0
  nply2d = 0
  nplz2d = 0

  ipost = 0
  iibm=0
  npif = 2
  izap = 1

  imodulo2 = 1

  !! CASE specific variables
  tgv_twod = .FALSE.

  !! TRIPPING
  A_tr=0.0
  xs_tr_tbl=1.402033
  ys_tr_tbl=0.350508
  ts_tr_tbl=1.402033
  x0_tr_tbl=3.505082

  ibc_shear_stress = 0
  ibc_shear_stress_read_dudy = 0
  ibc_shear_stress_read_dwdy = 0
  ibc_shear_stress_u = -1
  ibc_slip_length_u = -1
  ibc_slip_length_w = -1
  ibc_shear_stress_w = -1