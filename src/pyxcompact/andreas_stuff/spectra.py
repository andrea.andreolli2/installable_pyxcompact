from pyxcompact.simulation import Simulation
import numpy
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap



class spectral_tools:

    
    
    def __init__(self, sim, **kwargs):
        
        self.nx = len(sim.x); self.ny = len(sim.y); self.nz = len(sim.z)
        self.lx = sim.getPhysicalSize()[0]; self.lz = sim.getPhysicalSize()[2]
        
        self.nfx = (self.nx // 2) + 1 # number of pts after real fourier transform
        self.bz = self.nz // 2 # number of nonzero positive frequencies in z
        self.nfz = self.bz + 1 # number of pts after averaging neg/pos
        
        self.ipzf = 1 # first positive frequency in z
        self.ipzl = self.nfz # last positive frequency in z
        
        # first negative frequency in z: need to handle case in which no pts is even/uneven
        self.inzf = self.ipzl - 1 # minus one cause backwards slicing is used -> not inclusive index!
        if (self.nz % 2) == 0:
            self.inzf -= 1
        self.inzl = -1
        
        self.dkx = 2*numpy.pi/self.lx; self.dkz = 2*numpy.pi/self.lz
        self.kx = numpy.arange(self.nfx) * self.dkx; self.kz = numpy.arange(self.nfz) * self.dkz
        self.y = sim.y

        # temporary data for calculating spectra
        self.nyloc = kwargs.get('get_psd_nyloc',False)
        if self.nyloc:
            self.slab = numpy.zeros((3, self.nz, self.nyloc, self.nfx), dtype = numpy.complex128)
            self.psd = numpy.zeros((6, self.nyloc, self.nfz, self.nfx), dtype = numpy.float64)
    
    
    
    def get_ft_field(self, uu, vv, ww):
        
        nyloc = self.nyloc

        for ic, arr in enumerate((uu,vv,ww)):        
            for iy in range(nyloc):
            
                # x-transform
                for iz in range(self.nz):
                    xrow = arr[iz,iy,:]
                    self.slab[ic,iz,iy,:] = numpy.fft.rfft(xrow)

                # z-transform
                for ix in range(self.nfx):
                    self.slab[ic,:,iy,ix] = numpy.fft.fft(self.slab[ic,:,iy,ix],norm=None)

        # normalize
        self.slab *= 1/(self.nx * self.nz)
    
    
    
    def get_instant_psd(self):
        
        nyloc = self.nyloc
        
        for ic in range(6):
            
            i = ic; j = ic # this gets normal Reynolds stresses right
            
            if ic == 3:
                i = 0; j = 1
            if ic == 4:
                i = 0; j = 2
            if ic == 5:
                i = 1; j = 2
                        
            for iy in range(nyloc):
                self.psd[ic,iy,:,:] = (numpy.conj(self.slab[i, 0:self.nfz, iy, :]) * self.slab[j, 0:self.nfz, iy, :]).real
                self.psd[ic,iy,1:,:] += (numpy.conj(self.slab[i, self.inzl:self.inzf:-1, iy, :]) * self.slab[j, self.inzl:self.inzf:-1, iy, :]).real
                
        for ix in range(self.nfx):
            if not ix == 0:
                self.psd[:,:,:,ix] *= 2
    
    
    
    def read_psd(self, psd_file):
        
        psd_bin = numpy.fromfile(psd_file, dtype=numpy.float64).reshape((6, self.ny, self.nfz, self.nfx))
        
        return psd_bin
    
    

    def plot_cumulative(self, all_spectra, component, **kwargs):
    
        # unpack input
        dirstr = kwargs.get('dir','z')
        save_size = kwargs.get('save_size', (1,1)); w = save_size[0]; h = save_size[1]
        save_fig = kwargs.get('save_fig', False) # you can pass a string to this (the name of the file that you want to save)
        visc_units = kwargs.get('visc_units', False)
        y_symm = kwargs.get('y_symm', True)
        retau = kwargs.get('retau', 0)
        dclim = kwargs.get('clim', 0)
        re = kwargs.get('re', 0)
        ylog = kwargs.get('ylog', False)
        custom_title = kwargs.get('custom_title', '') + ',   '
        custom_cmap = kwargs.get('custom_cmap', None)
        cmp = gcmp(component)
        
        # apply new colormap if any
        if custom_cmap:
            my_cmap = custom_cmap
        else:
            my_cmap = inferno_wr
        
        # get stuff from class
        kk = self.kz.copy(); beta = self.dkz
        if dirstr == 'x':
            kk = self.kx.copy(); beta = self.dkx
        y = self.y.copy()
    
        # check that both retau and re are passed
        if visc_units and not (retau and re):
            raise Exception('Please provide both re and retau as arguments.')
        if visc_units:
            # get utau
            utau = retau/re
            # perform nondimensionalisation
            all_spectra /= (utau**2)
            y *= retau
            kk /= retau
        # calculate ymiddle
        ymiddle = int(numpy.ceil(len(y)/2))-1
    
        # check that clim is a vector
        if dclim:
            if not len(dclim) == 2:
                raise Exception('clim needs to be iterable with two elements.')
    
        # function specific parameters
        labels = (r'$k_z$', r'$y$')
        xlabel_alt = r'$\lambda_z$'
        fig_title = custom_title + r'$k_z\sum_{k_x} \langle \hat{'+cmp[0]+r'}^\dagger\hat{'+cmp[1]+r'} \rangle$'
        if dirstr == 'x':
            labels = (r'$k_x$', r'$y$')
            xlabel_alt = r'$\lambda_x$'
            fig_title = r'$k_z\sum_{k_z} \langle \hat{'+cmp[0]+r'}^\dagger\hat{'+cmp[1]+r'} \rangle$'
        
        if isinstance(save_fig, str):
            save_name = save_fig
        else:
            addstr = '_cumulative_zy'
            if dirstr == 'x':
                addstr = '_cumulative_xy'
            save_name = cmp + addstr
    
        # convert component to index
        idx = get_comp_idx(component)
        
        # select desired spectrum component
        spectrum = all_spectra[idx, :, :, :]
    
        
        if dirstr == 'x': # sum along z-axis
            spectrum = spectrum.sum(axis=(-2))
        else: # sum along x-axis
            spectrum = spectrum.sum(axis=(-1))
    
        # premultiply and divide by beta
        premultiplied = spectrum * abs(kk) / beta
    
        # prepare figure for plotting
        rfig, (rax,cb_ax) = plt.subplots(ncols=2,figsize=(10,7),gridspec_kw={"width_ratios":[1, 0.05]})
    
        # set scales to be logarithmic where requested
        rax.set_xscale("log") # x axis always logarithmic!
        if ylog:
            rax.set_yscale("log")
    
        # when plotting, 0 modes are excluded
        pos = rax.pcolormesh(kk[1:], y, premultiplied[:,1:], linewidth=0, rasterized=True,shading='gouraud',cmap=my_cmap)
        if dclim:
            pos.set_clim(dclim[0],dclim[1])
        rax.set_xlabel(labels[0])
        rax.set_ylabel(labels[1])
    
        # add secondary x axis with wavelength
        secaxx = rax.secondary_xaxis('top', functions=(get_wavelength_fwr,get_wavelength_inv))
        secaxx.set_xlabel(xlabel_alt)
    
        # last details for plotting
        rfig.colorbar(pos, cax=cb_ax)
        rax.set_title(fig_title)
        if y_symm:
            rax.set_ylim(y[1], y[ymiddle])
    
        # save
        if save_fig:
            fig = plt.figure(111,frameon=False)
            fig.set_size_inches(10*w,10*h)
            ax = plt.Axes(fig, [0., 0., 1., 1.])
            ax.set_axis_off()
            fig.add_axes(ax)
            ax.set_xscale("log")
            if ylog:
                ax.set_yscale("log")
            plt_handle = ax.pcolormesh(kk[1:], y, premultiplied[:,1:], linewidth=0, rasterized=True,shading='gouraud',cmap=my_cmap)
            if dclim:
                plt_handle.set_clim(dclim[0],dclim[1])
            if y_symm:
                ax.set_ylim(y[1], y[ymiddle])
            # save figure
            plt.savefig(save_name+'.png', format='png', bbox_inches='tight', pad_inches=0)
            # save tikz code
            clim_min, clim_max = plt_handle.get_clim()
            generate_tikz(save_name, save_size, labels, fig_title, ax.get_xlim(), ax.get_ylim(), clim_min, clim_max, xlog=True, ylog=ylog)
            # close figure, so that it does not get plotted
            plt.close(111)
    
        return rfig, rax



    def plot_odd(self, all_spectra, component, **kwargs):
    
        # unpack input
        dirstr = 'z'
        save_size = kwargs.get('save_size', (1,1)); w = save_size[0]; h = save_size[1]
        save_fig = kwargs.get('save_fig', False) # you can pass a string to this (the name of the file that you want to save)
        y_symm = kwargs.get('y_symm', True)

        dclim = kwargs.get('clim', 0)
        custom_title = kwargs.get('custom_title', '') + ',   '
        custom_cmap = kwargs.get('custom_cmap', None)
        cmp = gcmp(component)
        
        # apply new colormap if any
        if custom_cmap:
            my_cmap = custom_cmap
        else:
            my_cmap = inferno_wr
        
        # get stuff from class
        kk = self.kz.copy(); beta = self.dkz
        y = self.y.copy()
    
        # calculate ymiddle
        ymiddle = int(numpy.ceil(len(y)/2))-1
    
        # check that clim is a vector
        if dclim:
            if not len(dclim) == 2:
                raise Exception('clim needs to be iterable with two elements.')
    
        # function specific parameters
        labels = (r'$k_z$', r'$y$')
        xlabel_alt = r'$\lambda_z$'
        fig_title = custom_title + r'$k_z\sum_{k_x} \langle \hat{'+cmp[0]+r'}^\dagger\hat{'+cmp[1]+r'} \rangle$'
        
        if isinstance(save_fig, str):
            save_name = save_fig
        else:
            addstr = '_psd_zy'
            save_name = cmp + addstr
    
        # convert component to index
        idx = get_comp_idx(component)
        
        # select desired spectrum component
        spectrum = all_spectra[idx, :, :, :]
        spectrum = spectrum.sum(axis=(-1))
    
    
        # prepare figure for plotting
        rfig, (rax,cb_ax) = plt.subplots(ncols=2,figsize=(10,7),gridspec_kw={"width_ratios":[1, 0.05]})
    
        # when plotting, 0 modes are excluded
        pos = rax.pcolormesh(kk[1:], y, spectrum[:,1:], linewidth=0, rasterized=True,shading='gouraud',cmap=my_cmap)
        if dclim:
            pos.set_clim(dclim[0],dclim[1])
        rax.set_xlabel(labels[0])
        rax.set_ylabel(labels[1])
    
        # add secondary x axis with wavelength
        secaxx = rax.secondary_xaxis('top', functions=(get_wavelength_fwr,get_wavelength_inv))
        secaxx.set_xlabel(xlabel_alt)
    
        # last details for plotting
        rfig.colorbar(pos, cax=cb_ax)
        rax.set_title(fig_title)
        if y_symm:
            rax.set_ylim(y[1], y[ymiddle])
    
        # save
        if save_fig:
            fig = plt.figure(111,frameon=False)
            fig.set_size_inches(10*w,10*h)
            ax = plt.Axes(fig, [0., 0., 1., 1.])
            ax.set_axis_off()
            fig.add_axes(ax)
            ax.set_xscale("log")
            if ylog:
                ax.set_yscale("log")
            plt_handle = ax.pcolormesh(kk[1:], y, premultiplied[:,1:], linewidth=0, rasterized=True,shading='gouraud',cmap=my_cmap)
            if dclim:
                plt_handle.set_clim(dclim[0],dclim[1])
            if y_symm:
                ax.set_ylim(y[1], y[ymiddle])
            # save figure
            plt.savefig(save_name+'.png', format='png', bbox_inches='tight', pad_inches=0)
            # save tikz code
            clim_min, clim_max = plt_handle.get_clim()
            generate_tikz(save_name, save_size, labels, fig_title, ax.get_xlim(), ax.get_ylim(), clim_min, clim_max, xlog=True, ylog=ylog)
            # close figure, so that it does not get plotted
            plt.close(111)
    
        return rfig, rax
    
    
    
    def plot_premultiplied_at(self, all_spectra, component, y_tgt, **kwargs):
    
        # unpack input
        save_size = kwargs.get('save_size', (1,1)); w = save_size[0]; h = save_size[1]
        save_fig = kwargs.get('save_fig', False) # you can pass a string to this (the name of the file that you want to save)
        retau = kwargs.get('retau', 0)
        visc_units = kwargs.get('visc_units', False)
        re = kwargs.get('re', 0)
        dclim = kwargs.get('clim', 0)
        custom_title = kwargs.get('custom_title', '') + ',   '
        custom_cmap = kwargs.get('custom_cmap', None)
        cmp = gcmp(component)
        
        # apply new colormap if any
        if custom_cmap:
            my_cmap = custom_cmap
        else:
            my_cmap = inferno_wr
        
        # get stuff from class
        kz = self.kz.copy(); beta = self.dkz
        kx = self.kx.copy(); alpha = self.dkx
        y = self.y.copy()
        
        # TRICK FOR SHOWING kx, kz = 0 MODES
        kz[0] = kz[1]; kx[0] = kx[1]
    
        # check that both retau and re are passed
        if visc_units and not (retau and re):
            raise Exception('Please provide both re and retau as arguments.')
        if visc_units:
            # get utau
            utau = retau/re
            # perform nondimensionalisation
            all_spectra /= (utau**2)
            y *= retau
            kx /= retau; kz /= retau
        
        # find y
        diff = abs(self.y - y_tgt)
        iytgt = numpy.argmin(diff)
        real_ytgt = self.y[iytgt]
        print("iy of target: ", iytgt)
    
        # check that clim is a vector
        if dclim:
            if not len(dclim) == 2:
                raise Exception('clim needs to be iterable with two elements.')
    
        # function specific parameters
        labels = (r'$k_z$', r'$k_x$')
        xlabel_alt = r'$\lambda_z$'
        ylabel_alt = r'$\lambda_x$'
        fig_title = custom_title + r'$k_z\, k_x\, \langle \hat{'+cmp[0]+r'}^\dagger\hat{'+cmp[1]+r'} \rangle'
        if retau:
            fig_title = fig_title + r' (y^+=' + str(round(real_ytgt*retau,3))
        else:
            fig_title = fig_title + r' (y=' + str(round(real_ytgt,3))
        fig_title = fig_title + ')$'
        
        if isinstance(save_fig, str):
            save_name = save_fig
        else:
            addstr = '_premultiplied_y' + str(real_ytgt)
            save_name = cmp + addstr
    
        # convert component to index
        idx = get_comp_idx(component)
        
        # select desired spectrum component
        spectrum = all_spectra[idx, iytgt, :, :]
    
        # premultiply and divide by beta
        bcast_premult = abs(kz.reshape((-1, 1)) * kx)
        premultiplied = bcast_premult * spectrum / beta / alpha
    
        # prepare figure for plotting
        rfig, axs = plt.subplots(2, 2, figsize=(10,7), gridspec_kw={"width_ratios":[1, 0.05],"height_ratios":[1, 0.05]}, sharex='col')
        rax = axs[0,0]; cb_ax = axs[0,1]; x0_ax = axs[1,0]; empty_ax = axs[1,1]
        empty_ax.axis('off')
        
        # set scales to be logarithmic
        rax.set_xscale("log")
        x0_ax.set_xscale("log")
        rax.set_yscale("log")
    
        # when plotting, 0 modes are excluded
        pos = rax.pcolormesh(kz[1:], kx[1:], premultiplied[1:,1:].transpose(), linewidth=0, rasterized=True, shading='gouraud', cmap=my_cmap)
        x0pos = x0_ax.pcolormesh(kz[1:], [0,1], [premultiplied[1:,0].transpose(),premultiplied[1:,0].transpose()], linewidth=0, rasterized=True, shading='gouraud', cmap=my_cmap)
        if dclim:
            pos.set_clim(dclim[0],dclim[1])
        x0pos.set_clim(pos.get_clim())
        rax.tick_params(axis='x',labelbottom='off')
        x0_ax.set_xlabel(labels[0])
        rax.set_ylabel(labels[1])
        
        # add secondary x axis with wavelength
        secaxx = rax.secondary_xaxis('top', functions=(get_wavelength_fwr,get_wavelength_inv))
        secaxx.set_xlabel(xlabel_alt)
        secayy = rax.secondary_yaxis('right', functions=(get_wavelength_fwr,get_wavelength_inv))
        secayy.set_ylabel(ylabel_alt)
    
        # last details for plotting
        rfig.colorbar(pos, cax=cb_ax)
        rax.set_title(fig_title)
        x0_ax.get_yaxis().set_visible(False)
        
        # save
        if save_fig:
            
            # main plot
            fig = plt.figure(111,frameon=False)
            fig.set_size_inches(10*w,10*h)
            ax = plt.Axes(fig, [0., 0., 1., 1.])
            ax.set_axis_off()
            fig.add_axes(ax)
            ax.set_xscale("log"); ax.set_yscale("log")
            plt_handle = ax.pcolormesh(kz[1:], kx[1:], premultiplied[1:,1:].transpose(), linewidth=0, rasterized=True, shading='gouraud', cmap=my_cmap)
            if dclim:
                plt_handle.set_clim(dclim[0],dclim[1])
            ax.set_xlim(kz[1], kz[-1])
            ax.set_ylim(kx[1], kx[-1])
            # get plot params and save them
            tikz_par_dict = {}
            tikz_par_dict['cmin'], tikz_par_dict['cmax'] = plt_handle.get_clim()
            tikz_par_dict['kzmin'], tikz_par_dict['kzmax'] = ax.get_xlim()
            tikz_par_dict['kxmin'], tikz_par_dict['kxmax'] = ax.get_ylim()
            with open(save_name+'_settings.tex','w') as of:
                for key in tikz_par_dict:
                    of.write('\\renewcommand{\\' + key +'}{' + str(tikz_par_dict[key]) + '}\n')
            # save the fig
            plt.savefig(save_name+'.png', format='png', bbox_inches='tight', pad_inches=0)
            # close plot
            plt.close(111)

            # second plot (kx=0)
            fig = plt.figure(111,frameon=False)
            fig.set_size_inches(10*w,h)
            ax = plt.Axes(fig, [0., 0., 1., 1.])
            ax.set_axis_off()
            fig.add_axes(ax)
            ax.set_xscale("log")
            plt_handle = ax.pcolormesh(kz[1:], [0,1], [premultiplied[1:,0].transpose(),premultiplied[1:,0].transpose()], linewidth=0, rasterized=True, shading='gouraud', cmap=my_cmap)
            plt_handle.set_clim(tikz_par_dict['cmin'],tikz_par_dict['cmax'])
            ax.set_xlim(kz[1], kz[-1])
            ax.set_ylim([0,1])
            # save figure
            plt.savefig(save_name+'_kx0.png', format='png', bbox_inches='tight', pad_inches=0)
                        
            # close figure, so that it does not get plotted
            plt.close(111)
    
        return rfig, rax

    
    
def gcmp(component):
    label = 'uu'
    component = component.lower()
    if component == 'y' or component == 'vv' or component == 'v':
        label = 'vv'
    elif component == 'z' or component == 'ww' or component == 'w':
        label = 'ww'
    elif component == 'xy' or component == 'yx' or component == 'uv' or component == 'vu':
        label = 'uv'
    elif component == 'xz' or component == 'zx' or component == 'uw' or component == 'wu':
        label = 'uw'
    elif component == 'zy' or component == 'yz' or component == 'wv' or component == 'vw':
        label = 'vw'
    elif not component == 'x' or component == 'uu' or component == 'u':
        print('Error: invalid component. Please pass either "x", "y", "z" as the second argument.')
    return label



def get_comp_idx(component):
    idx = 0
    component = component.lower()
    if component == 'y' or component == 'vv' or component == 'v':
        idx = 1
    elif component == 'z' or component == 'ww' or component == 'w':
        idx = 2
    elif component == 'xy' or component == 'yx' or component == 'uv' or component == 'vu':
        idx = 3
    elif component == 'xz' or component == 'zx' or component == 'uw' or component == 'wu':
        idx = 4
    elif component == 'zy' or component == 'yz' or component == 'wv' or component == 'vw':
        idx = 5
    elif not component == 'x' or component == 'uu' or component == 'u':
        print('Error: invalid component. Please pass either "x", "y", "z" as the second argument.')
    return idx



def get_wavelength_fwr(x):
    wvl = numpy.zeros_like(x)
    x_is_zero = x==0
    wvl[x_is_zero] = 1e+308
    wvl[~x_is_zero] = 2*numpy.pi/x[~x_is_zero]
    return wvl



def get_wavelength_inv(x):
    return 2*numpy.pi/x



###################
# CUSTOM COLORMAP #
###################

# colormap settings
white_length = 7 # as a percentage (%)
white_blending = 4 # as a percentage (%)

# preprocess
factor = 3
no_samples = 100 * factor
white_length *= factor
white_blending *= factor

# generate new colormap
inferno = cm.get_cmap('inferno_r')
newcolors = inferno(numpy.linspace(0, 1, no_samples - white_length))
white_array = numpy.ones((white_length,4)) # one row of this reads 1,1,1,1 - which is white in RGBA

# apply blending
if white_blending > 0:
    for ii in range(white_length-white_blending, white_length):
        weight = ii/(white_length+1)
        white_array[ii,:] *= (1-weight)
        white_array[ii,:] += weight*newcolors[0,:]

# generate colormap
newcolors = numpy.concatenate((white_array,newcolors),axis=0)
inferno_wr = ListedColormap(newcolors)