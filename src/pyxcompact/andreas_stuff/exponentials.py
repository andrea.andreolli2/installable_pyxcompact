import numpy as np
from scipy.optimize import curve_fit
from warnings import warn


ERRCODE = 999999

# model functions for decay/formation
# first argument is the independent variable, then parameters

def exp_decay_disponly(x, tau, m):
    return m*(np.exp(-x/tau))

def jacm_decay_disponly(x, tau, m):
    d1 = (x / (tau*tau)) * exp_decay_disponly(x, tau, m) # d / dtau
    d2 = exp_decay_disponly(x, tau, m) / m
    return [d1,d2]

def exp_delayedecay_disponly(x, delta, tau, m):
    return m*(np.exp(-(x-delta)/tau))

def jacm_delayedecay_disponly(x, delta, tau, m):
    d1 = ((x-delta) / (tau*tau)) * exp_decay_disponly((x-delta), tau, m) # d / dtau
    d2 = exp_decay_disponly((x-delta), tau, m) / m
    return [d1,d2]

def exp_formation_disponly(x, tau, m):
    return m*(1 - np.exp(-x/tau))

def jacm_formation_disponly(x,tau,m):
    # notice: for d1 the decay function is referenced - THIS IS CORRECT! (it's changed in sign)
    d1 = - (x / (tau*tau)) * exp_decay_disponly(x, tau, m) # d / dtau
    d2 = exp_formation_disponly(x, tau, m) / m
    return [d1,d2]

def exp_full(x, tau, ipsd, fpsd):
    return (ipsd-fpsd)*(np.exp(-x/tau)) + fpsd



# wrapper for the nonlinear least-squares fit
def exp_fit(xd, yd, ef, j=None, unc=None):
    if hasattr(j,'iter') and hasattr(unc,'iter'):
        optimum, covar = curve_fit(ef, xd, yd, jac=j, sigma=unc)
    elif hasattr(unc,'iter'):
        optimum, covar = curve_fit(ef, xd, yd, sigma=unc)
    elif hasattr(j,'iter'):
        optimum, covar = curve_fit(ef, xd, yd, jac=j)
    else:
        optimum, covar = curve_fit(ef, xd, yd)
    return optimum, covar, ef



def first_minorflex(arr):
    found = False
    for ii in range(2,len(arr)-2):
        rv = ii
        # check local minimum
        if arr[ii] < arr[ii-1] and arr[ii] < arr[ii+1]:
            found = True
            break
    if not found:
        warn('No local minimum found. Defaulting to position of maximum.')
        rv = 0
    return rv



# delayed exponential fit
def fit_exp_delayed(t,data2fit,dataunc,nhood_size=300,plot_diag=None):

    # first off: get position of maximum - this gives initial estimate of delay
    imax = np.argmax(data2fit)

    # define look up table for indeces
    idx_lut = np.arange(max(0,imax),min(imax+nhood_size,len(t)))
    nn = len(idx_lut)

    # initialise arrays
    delta = np.zeros((nn))
    max_covar = np.zeros_like(delta)
    avg_err = np.zeros_like(delta)
    odd_err = np.zeros_like(delta)
    tau = np.zeros_like(max_covar)
    m = np.zeros_like(max_covar)

    # for each possible initial time, fit the curve
    for ii,first in enumerate(idx_lut):
        delta = t[first]-t[0]
        # opt: first number is tau, second is m
        # covar: 2x2 matrix - guess you need diagonal, so [0,0] is sigma_tau and [1,1] is sigma_m
        delayed = lambda td, taud, md : exp_delayedecay_disponly(td,delta,taud,md)
        delayed_j = lambda td, taud, md : jacm_delayedecay_disponly(td,delta,taud,md)
        opt, covar, _ = exp_fit(t[first:], data2fit[first:], delayed, delayed_j, dataunc)

        tau[ii] = opt[0]; m[ii] = opt[1]; max_covar[ii] = np.amax(np.diag(covar))

        # get avg err
        data = data2fit[first:]
        pred = exp_decay_disponly(t[first:]-delta,tau[ii],m[ii])
        err = (data-pred)**2
        avg_err[ii] = err.mean()
        odd_err[ii] = avg_err[ii]/np.sqrt(len(t[first:]))


    # get optimum
    itemp = first_minorflex(max_covar)
    idelta = idx_lut[itemp]; m_opt = m[itemp]; tau_opt = tau[itemp]
    delta = t[idelta]-t[0]

    # cast warning if idelta is last element of lut
    if idelta == idx_lut[-1]:
        warn('Optimum is on the boundary of the considered region')

    # plot diagnostics if requested
    if plot_diag:
        # clean up unconverged stuff
        for ii, mc in enumerate(max_covar):
            if mc > 10:
                max_covar[ii] = 0
                tau[ii] = 0
                m[ii] = 0
        # do the actual plot
        import matplotlib.pyplot as plt
        _, ax = plt.subplots(1,3)
        ax[0].plot(idx_lut,max_covar)
        ax[0].set_ylabel('max(covar)')
        ax[1].plot(idx_lut,avg_err)
        ax[1].set_ylabel('avg(err)')
        ax[2].plot(idx_lut,tau/np.amax(tau))
        ax[2].plot(idx_lut,m/np.amax(m))
        ax[2].set_ylabel('tau/max(tau), m/max(m)')
        
        

    return idelta, delta, m_opt, tau_opt