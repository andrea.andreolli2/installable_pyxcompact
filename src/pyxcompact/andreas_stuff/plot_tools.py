import numpy as np


def streamplotter(x,y,u,v,ax,**kwargs):

    clr = kwargs.get("color","white")
    
    new_y = np.linspace(y[0],y[-1],num=1.5*len(y))
    
    new_u = np.zeros((len(new_y),len(x)))
    new_v = np.zeros_like(new_u)
    
    for ix in range(len(x)):
        new_u[:,ix] = np.interp(new_y,y,u[:,ix])
        new_v[:,ix] = np.interp(new_y,y,v[:,ix])
        
    ax.streamplot(x,new_y,new_u,new_v,color=clr)



def equalise(x,y,u,v,n):

    new_x = np.linspace(x[0],x[-1],num=n)
    new_y = np.linspace(y[0],y[-1],num=n)

    temp_u = np.zeros((n,len(x)))
    temp_v = np.zeros_like(temp_u)

    for ix in range(len(x)):
        temp_u[:,ix] = np.interp(new_y,y,u[:,ix])
        temp_v[:,ix] = np.interp(new_y,y,v[:,ix])

    new_u = np.zeros((n,n))
    new_v = np.zeros_like(new_u)

    for iy in range(n):
        new_u[iy,:] = np.interp(new_x,x,temp_u[iy,:])
        new_v[iy,:] = np.interp(new_x,x,temp_v[iy,:])

    return new_x, new_y, new_u, new_v



def prequiver(x,y,u,v,n):
    
    # assume n is no. pts. in y (y-height is always the same)
    ny = n
    nx = int((x[-1]-x[0])*n * 0.5 )

    new_x = np.linspace(x[0],x[-1],num=nx)
    new_y = np.linspace(y[0],y[-1],num=ny)

    temp_u = np.zeros((ny,len(x)))
    temp_v = np.zeros_like(temp_u)

    for ix in range(len(x)):
        temp_u[:,ix] = np.interp(new_y,y,u[:,ix])
        temp_v[:,ix] = np.interp(new_y,y,v[:,ix])

    new_u = np.zeros((ny,nx))
    new_v = np.zeros_like(new_u)

    for iy in range(ny):
        new_u[iy,:] = np.interp(new_x,x,temp_u[iy,:])
        new_v[iy,:] = np.interp(new_x,x,temp_v[iy,:])

    return new_x, new_y, new_u, new_v