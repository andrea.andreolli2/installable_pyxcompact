import numpy as np
import toml



class span_pattern:



    def __init__(self,sim,extra_set_file):

        nz = len(sim.z)
        lz = sim.getPhysicalSize()[2]

        xset = toml.load(extra_set_file)
        s = xset['sliplength']['s']

        if not lz % (2*s) == 0:
            print("ERROR: lz should contain s an integer number of time.")
            exit()

        self.span_sl_arr = np.zeros((nz)); # normalised distribution of spanwise slip length along spanwise axis (preallocation)

        self.ns = int(lz/s) # number of strips (1 strip = half period)
        self.np = int(lz/(2*s)) # number of periods

        if not nz % (self.ns) == 0:
            print("ERROR: nz should be a multiple of the number of strips.")
            exit()

        self.npps = int(nz/self.ns) # number of points per strip
        self.nppp = 2*self.npps # number of points per period
        
        for iz in range(nz):
            if iz % self.npps == 0:
                self.span_sl_arr[iz] = 0.5
            elif iz % (2*self.npps) < self.npps:
                self.span_sl_arr[iz] = 1



    def get_bc_array(self,nx,nz,l):

        bc_array = np.zeros((nx, nz))

        for ix in range(nx):
            bc_array[ix,:] = l*self.span_sl_arr

        return bc_array
