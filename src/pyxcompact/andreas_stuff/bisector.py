import numpy as np

class bisector:

    def_end_transient = 0.95

    def __init__(self):
        pass

    def get_time_scale(self, time_seq, dt, ival, fval, sign_tgt):
        # sign tgt is 1 for decay, -1 for formation

        # initialise error report
        errlist = []

        # get reference values of psd
        jump = abs(fval-ival)
        target = fval + sign_tgt*(1-self.def_end_transient)*jump

        # multiply by sign_tgt, so that time signal is "always decaying"
        target *= sign_tgt
        time_seq *= sign_tgt

        find_final = np.where(time_seq > target)[0]

        # get time instant of end of transient
        if find_final.size == 0:
            errlist.append('neverstarts')
            it_final = 0; ts = 0
        elif find_final[-1] == (len(time_seq)-1): # this means, transient never ends
            errlist.append('neverends')
            it_final = 0; ts = 0
        else:
            it_final = find_final[-1]
            ts = (it_final+1.5) * dt # this would be: it_final + 1 (first instant of time, index 0, but t=dt) - 0.5 (zero crossing happens in between timesteps)

        return ts, errlist
    