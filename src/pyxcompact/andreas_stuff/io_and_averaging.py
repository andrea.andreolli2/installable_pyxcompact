import numpy as np
from pyxcompact.simulation import Simulation
from pyxcompact.andreas_stuff.span_pattern import span_pattern
import os



def get_fno(fid, search_dir, bare_name):
    '''Returns a string containing the file number pre-padded with zeros to match the names of the files output by xcompact.
    
    | Input description:
    | - fid: number of file as an integer (not preceded by zeros)
    | - search_dir: folder where to look for the files; the number of figures used for naming
    |               is read from here
    | - bare_name: bare name of the file from which number of zeros is inferred
    '''
    
    filelist = os.listdir(search_dir)
    for entry in filelist:
        if bare_name in entry and '.' not in entry:
            lenno = len(entry)-len(bare_name)
            break
            
    fid_string = str(fid)
    no_zeros = lenno - len(fid_string)
    full_id = ""
    for ii in range(no_zeros):
        full_id += str(0)
    full_id += fid_string
    
    return full_id



def fid_gen(first,last): # both indeces are INCLUSIVE
    
    n = len(first)
    
    # get a string of zeros
    ref = ''
    for i in range(n):
        ref += '0'
        
    start = int(first)
    stop = int(last) + 1
        
    fidz = [ref for ii in range(start,stop)]
    
    for ifid, fid in enumerate(range(start,stop)):
        sfid = str(fid)
        fidz[ifid] = fidz[ifid][:-len(sfid)]
        fidz[ifid] += sfid
    
    return fidz



def read_field(fname, sd):
    '''Reads a checkpoint file to memory. The shape of such file is (nz,ny,nx).
    
    | Input description:
    | - fname: path and name of file to read
    | - sd: instance of the class pyxcompact.simulation.Simulation containing 
    |       simulation details
    '''
    
    nx = len(sd.x); ny = len(sd.y); nz = len(sd.z)
    read_fld = np.fromfile(fname, dtype=np.float64).reshape((nz,ny,nx))

    return read_fld



def field_as_memmap(sim, fld_name):
    
    nx = len(sim.x); ny = len(sim.y); nz = len(sim.z)
    fld = np.memmap(fld_name, dtype=np.float64, shape=(nz,ny,nx))
    
    return fld



def read_stats(sim, filename):
    
    ny = len(sim.y); nz = len(sim.z)
    
    content = ["u", "v", "w", "p", "uu", "uv", "uw", "vv", "vw", "ww", "pp", "up", "vp", "wp", "uuu", "uuv", "uuw", "uvv", "uvw", "uww", "vvv", "vvw", "vww", "www"]

    a = np.fromfile(filename).reshape((-1,nz,ny))
    
    return a, content



def stats_as_memmap(sim, filename):
    
    ny = len(sim.y); nz = len(sim.z)
    
    content = ["u", "v", "w", "p", "uu", "uv", "uw", "vv", "vw", "ww", "pp", "up", "vp", "wp", "uuu", "uuv", "uuw", "uvv", "uvw", "uww", "vvv", "vvw", "vww", "www"]
    
    fsize = os.stat(filename).st_size
    ndim = int(fsize/8/nz/ny)

    a = np.memmap(filename, dtype=np.float64, shape=(ndim,nz,ny))
    
    return a, content



def phase_average(to_average,sim,sp,**kwargs):

    zsymm_coeff = kwargs.get('zsymm', None)
    ysymm_coeff = kwargs.get('ysymm', None)

    # get y
    y = sim.y
    ny = len(y)

    nz_new = sp.nppp # number of points per period
    z_new = np.arange(nz_new) * sim.dz

    pa = np.zeros((ny,nz_new), dtype=np.float64)
    # WATCH OUT: this is transposed wrt velocity field!
    
    # transpose and average (phase average + top/bottom wall average)
    for ip in range(sp.np):
        pa += to_average[(ip*nz_new):((ip+1)*nz_new),:].transpose()
        if ysymm_coeff:
            pa += (ysymm_coeff * to_average[(ip*nz_new):((ip+1)*nz_new),::-1].transpose())
    pa /= (sp.np*2) if ysymm_coeff else sp.np

    # apply z-symmetry if necessary
    if zsymm_coeff:
        # this average is now split in two parts
        # everything is symmetric around the center of each strip!
        nls = sp.npps + 1 # no pts left strip: this strip will include both points where slip is 1/2
        nrs = sp.npps - 1 # no pts right strip: this strip does NOT include points with slip = 1/2!
        pa[:,:nls] += zsymm_coeff*pa[:,(nls-1)::-1]
        pa[:,:nls] /= 2
        pa[:,nls:(nls+nrs)] += zsymm_coeff*pa[:,(nls+nrs-1):(nls-1):-1]
        pa[:,nls:(nls+nrs)] /= 2
    
    return pa, z_new



def phase_average_forplot(to_average,sim,sp,**kwargs):
# this variant returns one more point in z, so that effectively one of the extremes is repeated

    zsymm_coeff = kwargs.get('zsymm', None)
    ysymm_coeff = kwargs.get('ysymm', None)

    # get y
    y = sim.y
    ny = len(y)

    nz_new = sp.nppp # number of points per period
    z_new = np.arange(nz_new+1) * sim.dz # WATCH OUT: there is one more point in z - this is the repeated point (due to the periodic bc)

    pa = np.zeros((ny,nz_new+1), dtype=np.float64)
    # WATCH OUT: this is transposed wrt velocity field!
    # also, there is one more point in z: this is the repeated point (due to the periodic bc)
    
    # transpose and average (phase average + top/bottom wall average)
    for ip in range(sp.np):
        pa[:,0:-1] += to_average[(ip*nz_new):((ip+1)*nz_new),:].transpose()
        if ysymm_coeff:
            pa[:,0:-1] += (ysymm_coeff * to_average[(ip*nz_new):((ip+1)*nz_new),::-1].transpose())
    pa /= (sp.np*2) if ysymm_coeff else sp.np

    # apply z-symmetry if necessary
    if zsymm_coeff:
        # this average is now split in two parts
        # everything is symmetric around the center of each strip!
        nls = sp.npps + 1 # no pts left strip: this strip will include both points where slip is 1/2
        nrs = sp.npps - 1 # no pts right strip: this strip does NOT include points with slip = 1/2!
        pa[:,:nls] += zsymm_coeff*pa[:,(nls-1)::-1]
        pa[:,:nls] /= 2
        pa[:,nls:(nls+nrs)] += zsymm_coeff*pa[:,(nls+nrs-1):(nls-1):-1]
        pa[:,nls:(nls+nrs)] /= 2
    
    pa[:,-1] = pa[:,0] # copy first column into last column (periodic)

    return pa, z_new
