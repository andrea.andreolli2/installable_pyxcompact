from __future__ import annotations
from pyxcompact.log import log, TerminalColors
from pathlib import Path
from pyxcompact import simulationfile
from pyxcompact import simulation
import os.path
import numpy as np
from abc import ABCMeta, abstractmethod
import math
from typing import Union, Optional, List, Tuple
import shutil
import subprocess

class TimeDependentMixin:
    def getAtTimestep(self, timestep: float, tol: float=0, simplify:bool=True) -> Optional[Union[simulationfile.SimulationFile, List[simulationfile.SimulationFile] ]]:
        """Returns the files of the current FileManager where abs(file_timestep - timestep) <= tol.
        If simplify, 
         - no files returns None 
         - one file returns just the file
        In all other cases, the list of found files is returned

        Args:
            timestep (float): the timestep to look for files
            tol (float, optional): Tolerance for file finding. Defaults to 0.
            simplify (bool, optional): Simplfies the output of the method. Defaults to True.

        Returns:
            [NoneType, SimulationFile, List[SimulationFile]]: the located files
        """
        result = []
        for file in self.files:
            if (abs(file.timestep - timestep) <= tol):
                log.info(f"Found file at timestep {file.timestep}")
                result += [file]
        if simplify:
            if len(result) == 0:
                return None
            elif len(result) == 1:
                return result[0]
        return result
    def getBetweenTimesteps(self, start: int, end: int, inclusive: bool = True) -> List[simulationfile.SimulationFile]:
        """Returns all files of the current FileManager between start and end (inclusive)

        Args:
            start (int): Start timestep
            end (int): End timestep
            inclusive (bool, optional): Whether to include files at the start and end of the interval. Defaults to True.

        Returns:
            List[SimulationFile]: A list of the simulation files matching the conditions
        """
        return self.getAtTimestep((end-start)/2 + start, (end-start)/2 + (0.00001 if inclusive else -0.00001), simplify=False) # type: ignore
class FileManager:
    __metaclass__ = ABCMeta
    def __init__(self, sim : Simulation, init=True):
        log.info_heading(self.GetHeading())
        files = []
        self.sim = sim
        if not init:
            return
        fn = self.GetExpectedFileNames(sim)
        for s, f in fn:
            full_names = [s.path.parent / fn2 for fn2 in f]
            if not(FileManager.CheckExistence(full_names)):
                continue
            if len(full_names) == 0:
                continue
            #log.info(f"{type(self).__name__}: Trying to create handle for {f}")
            m = self.GetHandle(full_names, s)
            files.append(m)
        self.files = files
    @staticmethod
    def CheckExistence(fn):
        if not(isinstance(fn, list)):
            fn = [fn]
        for f in fn:
            if not os.path.exists(f):
                log.error(f"Expected file {f} not found")
                return False
        return True
    @abstractmethod
    def GetExpectedFileNames(self, sim) -> List[Tuple[Simulation, List]]:
        pass
    @abstractmethod
    def GetHandle(self, fn, sim) -> simulationfile.SimulationFile:
        pass
    @abstractmethod
    def GetHeading(self) -> str:
        pass
    

class MonitorManager(FileManager, TimeDependentMixin):
    def GetExpectedFileNames(self, sim: Simulation) -> List[Tuple[Simulation, List]]:
        imonitor = sim.getInputParams("InOutParam/imonitor")
        result = []
        for sim, im in imonitor:
            if im == 0:
                log.warn("Monitor file not requested")
                return []
            result += [(sim, ["monitor.txt"])]
        return result
    def GetHandle(self, fn, sim):
        return simulationfile.MonitorFile(fn, sim)
    def GetHeading(self):
        return "Read monitor file"
    def getAtTimestep(self, timestep: float, tol: float=0, simplify:bool=True):
        return self.files[0].getAtTimestep(timestep, tol, simplify) # type: ignore

class CheckpointManager(FileManager, TimeDependentMixin):
    def __init__(self, sim):
        self.files: List[simulationfile.CheckpointFile]
        super().__init__(sim)
    def GetExpectedFileNames(self, sim : Simulation) -> List[Tuple[Simulation, List]]:
        timesteps = sim.getTimesteps("InOutParam/icheckpoint")
        result = []
        if len(timesteps) == 0:
            log.warn("Checkpoint files not requested")
        for sim, ts in timesteps:
            result += [(sim, [f"restart{int(ts):07d}", f"restart{int(ts):07d}.info"])]
        return result
    def GetHandle(self, fn, sim):
        return simulationfile.CheckpointFile(fn, sim)
    def GetHeading(self):
        return "Read monitor file"
    def extractVirtualPlanes(self, direction : str, indices : List[int]) -> PlanesManager:
        pm = PlanesManager(self.sim, init=False)
        pm.files = []
        for file in self.files:
            pm.files += [file.extractPlanes(direction, indices)]
        return pm
    def getTimeAveragedPseudoDissipation(self, start, end, *args, inclusive=True, **kvargs) -> np.ndarray:
        rootpath = self.sim.path
        if (isinstance(rootpath, list)):
            rootpath = Path(rootpath[0])
        filename = rootpath.parent / f"psdiss_{start}_{end}.npz"
        if (os.path.exists(filename)):
            with np.load(filename) as data:
                return data['psdiss']
        files : List[simulationfile.CheckpointFile] = self.getBetweenTimesteps(start, end, inclusive) # type: ignore
        pdiss = None
        for file in files:
            file.loadFile()
            result = file.computePseudoDissipation(*args, **kvargs).contents["psdiss"] / len(files)
            file.unloadContent()
            if pdiss is None:
                pdiss = result
            else:
                pdiss += result
        np.savez(filename, psdiss=pdiss)
        return pdiss



class ypManager(FileManager):
    def GetExpectedFileNames(self, sim: Simulation) -> List[Tuple[Simulation, List]]:
        istret = sim.getInputParams("BasicParam/istret")
        result = []
        for sim, istr in istret:
            if istr == 0:
                log.warn("Stretching not active, no yp file written")
                return []
            result += [(sim, ["yp.dat"])]
        return result
    def GetHandle(self, fn, sim):
        return simulationfile.StretchingFile(fn, sim)
    def GetHeading(self):
        return "Read y stretching file"

class PlanesManager(FileManager, TimeDependentMixin):
    def __init__(self, sim, init=True):
        self.timearray = np.zeros((0, 3))
        super().__init__(sim, init=init)
    def GetExpectedFileNames(self, sim : Simulation) -> List[Tuple[Simulation, List]]:
        filelist = []
        for sim, isavevel2d in sim.getInputParams("InOutParam/isavevel2d"):
            isavephi2d = sim.getInputParam("InOutParam/isavephi2d")
            numscalar = sim.getInputParam("BasicParam/numscalar")
            ofplane2d = sim.getInputParam("InOutParam/ofplane2d")
            iplanes = 3*int(isavevel2d) + numscalar*isavephi2d
            steps = [t / ofplane2d for s, t in sim.getTimesteps("InOutParam/ofplane2d")]

        
            planesfolder = Path("planes")
            if (iplanes > 0):
                # at least the time file should be there
                timefile = sim.path.parent / planesfolder / "time.info"
            
                if (sim.getInputParam("InOutParam/nplx2d") > 0):
                    filelist += [(sim, [planesfolder / f"xpl{int(i):07d}", planesfolder / "xpl.ini"]) for i in steps]
                if (sim.getInputParam("InOutParam/nply2d") > 0):
                    filelist += [(sim, [planesfolder / f"ypl{int(i):07d}", planesfolder / "ypl.ini"]) for i in steps]
                if (sim.getInputParam("InOutParam/nplz2d") > 0):
                    filelist += [(sim, [planesfolder / f"zpl{int(i):07d}", planesfolder / "zpl.ini"]) for i in steps]
            else:
                log.warn("Visu2d not active, no planes written")
            self.timearray = np.append(self.timearray, np.loadtxt(timefile, skiprows=1), axis=0)
        return filelist
    def GetHandle(self, fn, sim):
        return simulationfile.PlaneFile(fn, sim, self.timearray, check=False)
    def GetHeading(self):
        return "Read plane files"
    def GetTimeAveragedPhiTensor(self, start, end, direction, inclusive=True) -> Tuple[List, np.ndarray]:
        files : List[simulationfile.PlaneFile] = self.getBetweenTimesteps(start, end, inclusive) # type: ignore
        files = [file for file in files if file.axis == direction] # filter by direction
        # find out size of result array
        files[0].loadFile()
        result = np.zeros((*(files[0].contents["ux"].shape[0:2]), 3, 3, len(files[0].index_pl)))
        total = len(files)
        for file in files:
            file.loadFile()
            result += file.computePhiTensor().contents["Phi"] / total
            file.unloadContent()
        return (files[0].index_pl, result)



            
class SnapshotManager(FileManager, TimeDependentMixin):
    def GetExpectedFileNames(self, sim : Simulation) -> List[Tuple[Simulation, List]]:
        filelist = []
        datafolder = Path("data")
        for sim, ioutput in sim.getInputParams("InOutParam/ioutput"):
            snapshots = [i / int(ioutput) for s, i in sim.getTimesteps("InOutParam/ioutput")]
            for i in snapshots:
                current_files = simulationfile.SnapshotFile.getExpectedFiles(sim, i, datafolder)
                filelist += [(sim, current_files)]
        return filelist
    
    def GetHandle(self, fn, sim):
        return simulationfile.SnapshotFile(fn, sim)
    def GetHeading(self):
        return "Read shapshot files"
    def split_sims(self, files: List[simulationfile.SimulationFile]) -> Tuple[List[simulation.Simulation], List[List[simulationfile.SimulationFile]]]:
        sims = []
        simfiles : List[List] = []
        # we have to sort by simulation first (in case of multiple runs)
        for file in files:
            if file.sim not in sims:
                sims.append(file.sim)
                simfiles.append([file])
            else:
                simfiles[sims.index(file.sim)].append(file)
        return (sims, simfiles)
    def updateFortranPostProcessing(self, start, end, path_to_fpost, inclusive=True, itkebudget=0, iyderiv=0):
        files : List[simulationfile.CheckpointFile] = self.getBetweenTimesteps(start, end, inclusive) # type: ignore
        sims, simfiles = self.split_sims(files)
        # We have to generate a mean_average file so Fortran can compute the instantaneous fluctuations.
        averaged = Stats2DManager(self.sim, "x").TimeAverageInterval(start, end, inclusive=True)
        for sim, files in zip(sims, simfiles):
            statspath = sim.path.parent / "stats"
            fresultpath = statspath / "statmean"
            uptodate = True
            postpath = sim.path.parent / "inputPost.i3d"
            first = files[0].fn[0].stem[2:]
            last = files[-1].fn[0].stem[2:]
            step = int(max(1, (int(last) - int(first)) / len(files)))
            if not(os.path.exists(postpath)):
                uptodate = False
            else:
                inputparam = simulation.Simulation.parseInputfile(postpath)
                if not(inputparam["PostParam/idstart"] == int(first) and inputparam["PostParam/idend"] == int(last) and inputparam["PostParam/idstep"] == step) \
                   or not(os.path.exists(fresultpath)) or os.path.getmtime(fresultpath) < os.path.getmtime(postpath):
                    uptodate = False
            if (uptodate): 
                log.info("Fortran postprocessing is already up to date.")
                continue 
            if os.path.exists(postpath):
                os.remove(postpath)
            shutil.copy(sim.path.parent / "input.i3d", postpath)
            with open(postpath, "a") as myfile:
                myfile.write(f"""\n!================\n&PostParam\n!================\nitke_budget={itkebudget}\niyderiv={iyderiv}\nidstart={first}\nidend={last}\nidstep={step}\nistatistic2d={sim.getInputParam("InOutParam/istatx")}\n/End\n""")
            # write means file to simulation/stats2d/xstats_mean
            averaged.writeFile(filename=sim.path.parent / "stats2d" / "xstats_mean", as_mean=True, overwrite=True)
            # the current fortran code doesn't check this.
            if not os.path.exists(statspath):
                os.mkdir(statspath)
            # Now call Fortran postprocessing
            subprocess.run([path_to_fpost], cwd=str(sim.path.parent))
    def getTimeAveragedTKEBudget(self, start, end, path_to_fpost,  inclusive=True) -> np.ndarray:
        files : List[simulationfile.CheckpointFile] = self.getBetweenTimesteps(start, end, inclusive) # type: ignore
        sims, _ = self.split_sims(files)
        self.updateFortranPostProcessing(start, end, path_to_fpost, inclusive=True, itkebudget=1)

        dims = tuple([self.sim.getInputParam(f"BasicParam/n{i}") for i in "xyz"])
        result = np.zeros(dims)
        for sim in sims:
            statspath = sim.path.parent / "stats"
            ar = np.fromfile(statspath / "statmean", '<f8')
            ar = ar.reshape(dims, order="F")
            result += ar
        avg = result / len(sims)
        res = {"psdiss" : np.sum(avg[0:9], axis=0), "mconv" : np.sum(avg[9:11], axis=0), "P" : np.sum(avg[11:17], axis=0), "ttrans" : np.sum(avg[17:23], axis=0), "viscdiff" : np.sum(avg[23:25], axis=0), "ptr" : np.sum(avg[25:27], axis=0)}
        return res
    def getFortranDerivativesPaths(self, start, end, path_to_fpost, inclusive=True) -> List[Path]:
        files : List[simulationfile.CheckpointFile] = self.getBetweenTimesteps(start, end, inclusive) # type: ignore
        sims, simfiles = self.split_sims(files)
        self.updateFortranPostProcessing(start, end, path_to_fpost, inclusive=True, iyderiv=1)
        result = []
        for sim, files in zip(sims, simfiles):
            for file in files:
                result += [sim.path.parent / "gradients" / f"d{coord}dy{file.fn[0].stem[-5:]}" for coord in "uvw"]
        return result
    def getTimeAveragedPseudoDissipation(self, start, end, *args, inclusive=True, **kvargs) -> np.ndarray:
        rootpath = self.sim.path
        if (isinstance(rootpath, list)):
            rootpath = Path(rootpath[0])
        filename = rootpath.parent / f"psdiss__snapshots_{start}_{end}.npz"
        if (os.path.exists(filename)):
            with np.load(filename) as data:
                return data['psdiss']
        files : List[simulationfile.SnapshotFile] = self.getBetweenTimesteps(start, end, inclusive) # type: ignore
        pdiss = None
        for file in files:
            file.loadFile()

            # construct a temporary snapshot file with the relevant quantities
            cp = simulationfile.CheckpointFile(None, self.sim, False)
            cp.timestep = file.timestep
            cp.contents = file.contents
            
            result = cp.computePseudoDissipation(*args, **kvargs).contents["psdiss"] / len(files)
            file.unloadContent()
            if pdiss is None:
                pdiss = result
            else:
                pdiss += result
        np.savez(filename, psdiss=pdiss)
        return pdiss
            

            
            
        
class Stats2DManager(FileManager, TimeDependentMixin):
    
    def __init__(self, sim, direction):
        self.files: List[simulationfile.StatisticsFile]
        if direction == "x" or direction == "z":
            self.direction = direction
        else:
            raise ValueError("Direction must be either x or z")
        super().__init__(sim)
    def GetExpectedFileNames(self, sim : Simulation)  -> List[Tuple[Simulation, List]]:
        filelist = []
        for sim, istat in sim.getInputParams(f"InOutParam/istat{self.direction}"):
            if istat == 0:
                log.warn(f"No statistics in {self.direction} requested")
                continue
            ifirst = int(sim.getInputParam("BasicParam/ifirst"))
            ilast = int(sim.getInputParam("BasicParam/ilast"))
            ofreq2d = int(sim.getInputParam("Statistics/ofreq2d"))
            icheckpoint = int(sim.getInputParam("InOutParam/icheckpoint"))
            if (ofreq2d > icheckpoint or icheckpoint % ofreq2d != 0):
                ofreq2d = icheckpoint
            statistics = [i / ofreq2d for i in range(ifirst, ilast+1) if i % ofreq2d == 0]
            statsfolder = Path("stats2d")
            for i in statistics:
                current_files = [statsfolder / f"{self.direction}stats{int(i):07d}"]
                numscalar = sim.getInputParam("BasicParam/numscalar")
                if (numscalar > 0):
                    for n in range(1, numscalar+1):
                        current_files += [statsfolder / f"{self.direction}stats_phi{n}{int(i):07d}"]
                current_files += [statsfolder / f"{self.direction}stats{int(i):07d}.ini"]
                filelist += [(sim, current_files)]
        return filelist
    def GetHandle(self, fn, sim):
        return simulationfile.StatisticsFile(fn, sim, self.direction)
    def GetHeading(self):
        return "Read shapshot files"
    def TimeAverageInterval(self, start, end, inclusive=True) -> simulationfile.StatisticsFile:
        import glob
        parent = self.sim.path
        if isinstance(parent, list):
            import os
            parent = Path(os.path.commonprefix(parent))
        search_path = (Path(parent.parent) / "time_averaged.npz").as_posix()
        npzfilenames =  glob.glob(search_path)
        for file in npzfilenames:
            with np.load(file) as statsfile:
                if (statsfile["t_start"] == start and statsfile["t_end"] == end):
                    file = simulationfile.StatisticsFile(None, self.sim, self.direction, start, end - start)
                    file.contents = {k: statsfile[k] for k in statsfile if k not in ["y", "y+", "z", "z+", "t_start", "t_end"]}
                    file.y = statsfile["y"]
                    file.otherdim = statsfile["z"]
                    file.direction = "x"
                    file.isLoaded = True
                    return file
        
        files : List[simulationfile.StatisticsFile] = self.getBetweenTimesteps(start, end, inclusive) # type: ignore
        sum_field = {}
        for file in files:
            file.loadFile()
            for key, field in file.contents.items():
                if key not in sum_field:
                    sum_field[key] = [np.zeros(field.shape), 0]
                sum_field[key][0] += field * file.deltat
                sum_field[key][1] += file.deltat
        for key in sum_field.keys():
            sum_field[key] = sum_field[key][0] / sum_field[key][1]
        file = simulationfile.StatisticsFile(None, self.sim, self.direction, start, end - start)
        file.info = files[0].info
        file.contents = sum_field
        file.isLoaded = True
        return file
    def getDimensionlessAxes(self, wall_units = False):
        n = {coord : int(self.sim.getInputParam(f"BasicParam/n{coord}")) for coord in ["x", "y", "z"]}
        self.sim.compute_coordinates()
        axes = [getattr(self.sim, j) * (int(self.sim.getInputParam(f"BasicParam/re")) if (j == "y" and wall_units) else 1) for j in n.keys() if j != self.direction]
        return axes
        
class BCManager(FileManager):
    def __init__(self, sim):
        super().__init__(sim)
    def GetExpectedFileNames(self, sim : Simulation)  -> List[Tuple[Simulation, List]]:
        filelist = []
        for (sim, read_x) in sim.getInputParams("BasicParam/ibc_shear_stress_read_dudy"):
            current_files = []
            if read_x == 1:
                filelist += [(sim, [Path("ustressbc.dat")])]
            if sim.getInputParam("BasicParam/ibc_shear_stress_read_dwdy") == 1:
                filelist += [(sim, [Path("wstressbc.dat")])]
            
        return filelist
    def GetHandle(self, fn, sim):
        return simulationfile.WallStressBCFile(fn, sim)
    def GetHeading(self):
        return "Read wall stress boundary files"


