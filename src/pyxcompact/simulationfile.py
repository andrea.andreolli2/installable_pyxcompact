from __future__ import annotations
import numpy as np
import os.path
import os
from .log import log, TerminalColors
from pyxcompact import simulation
from abc import ABCMeta, abstractmethod
from typing import Dict, List
from pyxcompact import filechecker
from pathlib import Path

class TimeDependentFileMixin:
    def __init__(self, timestep : int, *args, **kwargs):
        self.timestep = timestep
        super().__init__(*args, **kwargs)

class SimulationFile:
    __metaclass__ = ABCMeta
    isLoaded = False
    def __init__(self, fn : List, sim, load=False, check=True):
        self.contents: Dict[str, np.ndarray]
        self.fn = fn
        self.sim = sim
        if (load):
            self.loadFile()
        if check and not(self.CheckForPlausibility(sim)):
            log.error("File appears to be broken")
            #raise ValueError("File appears to be broken")
    def __getitem__(self, key):
        if (self.isLoaded):
            return self.contents.__getitem__(key)
        else:
            raise ValueError("Load file first")
    @abstractmethod
    def loadFile(self) -> SimulationFile:
        pass
    @abstractmethod
    def CheckForPlausibility(self, sim):
        pass

class MonitorFile(SimulationFile):
    row_headers = {3: ["UBulk", "Nusselt", "Tauw", "Tm"], 14: ["Nusselt"], 15: ["UBulk", "Nusselt"]} 
    def __init__(self, fn, sim, check=True):
        super().__init__(fn, sim, True, check)
    def loadFile(self, index=0) -> MonitorFile:
        self.contents = np.loadtxt(self.fn[0])
        self.isLoaded = True
        return self
    def CheckForPlausibility(self, sim):
        if len(self.contents.shape) == 1 and self.contents.shape[0] == 0:
            self.length = 0
            c = 0
        else:
            self.length, c = self.contents.shape
        ts = sim.getInputParam("BasicParam/ilast")
        mf = sim.getInputParam("InOutParam/fmonitor")
        self.aborted = int(ts / mf) > self.length
        if (self.aborted):
            log.warn(f"""Monitor file contains only {self.length} entries, expected {ts}/{mf}={ts/mf}. Simulation likely ended prematurely.""")
        else:
            log.info(f"""Monitor file with {self.length} entries read""")
        if self.length == 0:
            return True
        simtype = sim.getInputParam("BasicParam/itype")
        if simtype not in MonitorFile.row_headers:
            log.warn(f"Simulation type {simtype} usually doesn't produce a monitor file. We don't understand the contents.")
            return False
        logcontents = ['time'] + MonitorFile.row_headers[simtype]
        log.info(f"Expecting {len(logcontents)} columns, likely {logcontents}")
        if (len(logcontents) == c):
            log.info(f"Found: {c} columns.")
            self.headers =  logcontents
        else:
            log.warn(f"Found: {c} columns. We don't understand the contents.")
            return False
        return True
    def getAtTimestep(self, timestep: float, tol: float=0, simplify:bool=True):
        test = self.contents[:, 0] / self.sim.getInputParam("BasicParam/dt") - timestep
        return self.contents[np.abs(self.contents[:, 0] / self.sim.getInputParam("BasicParam/dt") - timestep) <= tol]

class NumpyArrayFile(SimulationFile):
    dtype='<f8'
    def __init__(self, fn, sim, check=True):
        self.contents = {}
        super().__init__(fn, sim, False, check)
    def loadFile(self, index=0, only=[], noconcat=False) -> NumpyArrayFile:
        if (self.isLoaded):
            return self
        cont = self.getContents(index)
        
        # fn is a list in this case
        with open(self.fn[index], "rb") as f:
            for i in cont.keys():
                base_dimensions = len(cont[i])
                
                splitted = [i] if noconcat else i.split("_")
                name = splitted[0]
                #print(name)
                if (len(only) > 0):
                    if name not in only:
                        # don't read this one
                        f.seek(np.prod(cont[i]) * np.dtype(self.dtype).itemsize, 1)
                        #print(f.tell())
                        continue
                # if the array doesn#t exist yet, create a new one
                if not (name in self.contents):
                    candidates = []
                    for key in cont:
                        if (key.startswith(name)):
                            candidates += [key]
                    # For more than 3-D arrays, the keys are name_i_j_k_... . 
                    sizes = cont[i] + [1] * (len(splitted) - 1)
                    # Loop over the number of dashes and find the maximum index for all variables with the same name.
                    for k in range(1, len(splitted)):
                        for j in candidates:
                            if cont[j] != cont[i]:
                                raise ValueError("Dimension mismatch")
                            sizes[k + base_dimensions - 1] = max(sizes[k +  base_dimensions - 1], int(j.split("_")[k])+1)
                    self.contents[name] = np.zeros(sizes)
                array = self.contents[name]
                read = np.fromfile(f, self.dtype, np.prod(cont[i])).reshape(cont[i], order="F")
                array[(slice(None),)*base_dimensions + tuple([int(i) for i in splitted[1:]])] = read
            rest = f.read() 
            if (len(rest) != 0):
                log.error(f"{len(rest)} remaining bytes in the file. Expected contents: {cont} with datatype {self.dtype}")
        if len(only) == 0 and not noconcat:
            # only set the flag if fully loaded
            self.isLoaded = True
        return self
    def unloadContent(self, to_unload=[]):
        for i in list(self.contents):
            if (len(to_unload) == 0 or i in to_unload):
                del self.contents[i]
        self.isLoaded = False
    def CheckForPlausibility(self, sim) -> bool:
        result = True
        for j in range(len(self.fn)):
            if self.fn[j].suffix == ".info" or self.fn[j].suffix == ".ini":
                continue
            size = os.path.getsize(self.fn[j])
            cont = self.getContents(j)
            expected_size = sum([np.prod(cont[i]) for i in cont.keys()]) * np.dtype(self.dtype).itemsize
            if (size != expected_size):
                result = False
        return result
    def writeFile(self, index = 0, overwrite=False, only=[], append=False, noconcat=False, filename="", contents=None):
        filename = filename if filename != "" else self.fn[index]
        if len(only) == 0 and not self.isLoaded:
            raise ValueError("File not loaded")
        cont = contents if contents is not None else self.getContents(index)
        filemode = "ab" if append else "wb" 
        if os.path.exists(filename):
            if not append:
                log.warn(f"File {filename} will be overwritten")
                if not overwrite:
                    log.error("No overwrite permission given")
                    return
            if append:
                log.info(f"Appending to existing file {filename}")
        elif append:
            # file doesn't exist, but should append -> create
            log.info(f"Creating file {filename}")
            filemode = "wb"
        with open(filename, filemode) as f:
            for i in cont.keys():
                base_dimensions = len(cont[i])
                splitted = [i] if noconcat else i.split("_")
                name = splitted[0]
                if (len(only) > 0):
                    if name not in only:
                        continue
                if not name in self.contents:
                    raise ValueError("value not found")
                slc = (slice(None),)*base_dimensions + tuple([int(i) for i in splitted[1:]])
                sliced = self.contents[name][slc]
                if not np.array_equal(sliced.shape, cont[i]):
                    raise ValueError("Dimension mismatch")
                fshape = np.prod(cont[i])
                sliced.reshape(fshape, order="F").astype(self.dtype).tofile(f)
                #print(f.tell())
    def getDimensionless(self, wall_units):
        dimensionless = {}
        for key, value in self.contents.items():
            dimensionless[key] = value / self.getDimensionFactor(key, wall_units)
        return dimensionless
    
    @abstractmethod
    def getDimensionFactor(self, key: str, wall_units: bool = False):
        pass

    @abstractmethod
    def getContents(self, index: int):
        pass
from scipy.interpolate import RegularGridInterpolator
class CheckpointFile(TimeDependentFileMixin, NumpyArrayFile):
    dtype='<f8'

    def __init__(self, fn, sim, check=True):
        self.info = None
        if (fn is None):
            self.generateInfoFile(sim, 0)
            super().__init__(self.info["Time/itime"], self.fn, sim, False)
        else:
            self.fn = fn
            self.readInfoFile()
            super().__init__(self.info["Time/itime"], fn, sim)
    def readInfoFile(self):
        self.info = simulation.Simulation.parseInputfile(self.fn[1])
    def getContents(self, index : int) -> Dict:
        n = {}
        n["x"] = self.info["NumParam/nx"]
        n["y"] = self.info["NumParam/ny"]
        n["z"] = self.info["NumParam/nz"]
        itype = self.info["NumParam/itimescheme"]
        iscalar = self.info["NumParam/iscalar"]
        standard_size = [n[j] for j in n.keys()]
        pressure_size = [n["x"], n["y"] - 1, n["z"]]
        contents = {f"u{i}":standard_size for i in n.keys()}
        deriv1 = itype == 2 or itype == 3 or itype == 7
        deriv2 = itype == 3 or itype == 7
        if (deriv1):
            contents.update({f"du{i}_0":standard_size for i in n.keys()})
        if (deriv2):
            contents.update({f"du{i}_1":standard_size for i in n.keys()})
        contents.update({"p": pressure_size})
        if (iscalar == 1):
            for i in range(self.info["NumParam/numscalar"]):
                contents.update({f"phi_{i}":standard_size})
                if (deriv1):
                    contents.update({f"dphi_{i}_0":standard_size})
                if (deriv2):
                    contents.update({f"dphi_{i}_1":standard_size})
        return contents
    def generateInfoFile(self, sim, timestep : int):
        self.info = simulation.SimulationParameters(False)
        self.info["Time/tfield"] = timestep * sim.getInputParam("BasicParam/dt")
        self.info["Time/itime"] = timestep
        self.info["NumParam/nx"] = sim.getInputParam("BasicParam/nx")
        self.info["NumParam/ny"] = sim.getInputParam("BasicParam/ny")
        self.info["NumParam/nz"] = sim.getInputParam("BasicParam/nz")
        self.info["NumParam/Lx"] = sim.getInputParam("BasicParam/xlx")
        self.info["NumParam/Ly"] = sim.getInputParam("BasicParam/yly")
        self.info["NumParam/Lz"] = sim.getInputParam("BasicParam/zlz")
        self.info["NumParam/istret"] = sim.getInputParam("BasicParam/istret")
        self.info["NumParam/beta"] = sim.getInputParam("BasicParam/beta")
        self.info["NumParam/iscalar"] = sim.getInputParam("BasicParam/numscalar") > 0
        self.info["NumParam/numscalar"] = sim.getInputParam("BasicParam/numscalar")
        self.info["NumParam/itimescheme"] = sim.getInputParam("NumOptions/itimescheme")
        rootpath = sim.path
        if (isinstance(rootpath, list)):
            rootpath = Path(rootpath[0])
        self.fn = [rootpath.parent / f"restart{timestep:07}", rootpath.parent / f"restart{timestep:07d}.info"]
    @staticmethod
    def interpolateArrays(orig_grid, values, new_grid, period):
        # this would be nice, but doesn't work 100% since RegularGridInterpolator has no idea of the 
        # periodicity. So let's write it ourselves
        """interpolator = RegularGridInterpolator(orig_grid, values)
        mx, my, mz = np.meshgrid(*new_grid)
        grid_size = np.prod(list(map(len, new_grid)))
        mx = mx.reshape(grid_size)
        print(np.max(mx))
        print(np.max(orig_grid[0]))
        my = my.reshape(grid_size)
        mz = mz.reshape(grid_size)
        return interpolator((mx, my, mz)).reshape(*map(len, new_grid))""" 
        # This method returns an iterator generating a Numpy indexing, 
        # with : on position all_index and traversing all other indices
        def iterate_array(values, all_index = 0, current_index = 0, max_index = 1, slicer=[]):
            if current_index == all_index:
                slicer[current_index] = slice(None)
                yield from iterate_array(values, all_index, current_index+1, max_index, slicer)
            elif current_index < max_index:
                for i in range(values.shape[current_index]):
                    slicer[current_index] = i
                    yield from iterate_array(values, all_index, current_index+1, max_index, slicer)
            else:
                yield slicer
            
        array = values
        # loop over all grid dimensions, and iterpolate it
        for index in range(len(orig_grid)):
            # the new array has dimension 
            # [new_x,     y,     z, ...] after interpolating x
            # [new_x, new_y,     z, ...] after interpolating y
            # [new_x, new_y, new_z, ...] after interpolating z
            log.info(f"  Interpolating dimension {index}")
            dbg = [len(new_grid[i]) if i <= index else len(orig_grid[i]) for i in range(len(orig_grid))]
            interp_array = np.zeros(
                [len(new_grid[i]) if i <= index else len(orig_grid[i]) for i in range(len(orig_grid))] + 
                [values.shape[i] for i in range(len(orig_grid), len(values.shape))])
            for slc in iterate_array(array, index, 0, len(array.shape), [0] * len(array.shape)):
                interp_array[tuple(slc)] = np.interp(new_grid[index], orig_grid[index], array[tuple(slc)], period=period[index])            
            array = interp_array
        return array
    def getDimensionFactor(self, key: str, wall_units: bool = False):
        return NotImplemented
                    
    
    def interpolate(self, new_sim, sequential : bool=True, index=0, shift_u = 0):
        """Interpolate a checkpoint to act as initial field for another simulation"""
        other = CheckpointFile(None, new_sim)
        self.sim.compute_coordinates(True)
        new_sim.compute_coordinates(True)
        #self.loadFile()
        other.isLoaded = True
        other.contents = {}
        other.sim = new_sim
        if sequential:
            self.unloadContent()
        else:
            self.loadFile()
        period = (self.sim.getInputParam("BasicParam/xlx"), None, self.sim.getInputParam("BasicParam/zlz"))
        
        def sequential_iterator():
            append = False
            for key in self.getContents(index).keys():
                #if "_" not in key: 
                #    continue
                self.loadFile(index, only=[key], noconcat=True)
                
                yield key, self.contents[key]
                other.writeFile(index, overwrite=True, only=[key], noconcat=True, append=append)
                self.unloadContent([key])
                other.unloadContent([key])
                append = True
        iterator = sequential_iterator() if sequential else self.contents.items()
        for key, value in iterator:
            log.info(f"Interpolating array {key}")
            if key == "p":
                other.contents[key] = CheckpointFile.interpolateArrays(
                    (self.sim.xp, self.sim.yp, self.sim.zp), value, 
                    (other.sim.xp, other.sim.yp, other.sim.zp), period) 
            else:
                other.contents[key] = CheckpointFile.interpolateArrays(
                    (self.sim.x, self.sim.y, self.sim.z), value, (other.sim.x, other.sim.y, other.sim.z), period) + (shift_u if key == "ux" else 0)
        return other
    def extractPlanes(self, direction, indices) -> PlaneFile:
        log.info(f"Extracting planes for t={self.timestep}")
        n = {}
        n["x"] = self.info["NumParam/nx"]
        n["y"] = self.info["NumParam/ny"]
        n["z"] = self.info["NumParam/nz"]

        pl = PlaneFile(None, self.sim, check=False, timestep = self.timestep)
        pl.axis = direction
        pl.index_pl = indices
        pl.isLoaded = True
        pl.contents = {}
        slicer = tuple([slice(None) if j != direction else indices for j in n.keys() ])
        keys = ["ux", "uy", "uz"] 
        if self.sim.getInputParam("InOutParam/isavephi2d") > 0:
            keys += [f"phi_{scalar}" for scalar in range(self.sim.getInputParam("BasicParam/numscalar"))]
        plane_ax = list(n.keys()).index(direction)
        for key in keys:
            self.loadFile(only=[key])
            pl.contents[key] = np.swapaxes(self.contents[key][slicer], plane_ax, 2)
            self.unloadContent()
        return pl
    def computePseudoDissipation(self, **kwargs) -> CheckpointFile:
        log.info(f"Computing pseudo dissipation for checkpoint t={self.timestep}")
        self.sim.compute_coordinates()
        # pseudo dissipation Ɛ̃ = <du_i'/dx_j * du_i'/dx_j>
        coord_list = "xyz"
        shape = tuple((self.info[f"NumParam/n{x}"] for x in "xyz"))
        pdiss = np.zeros(shape[1:])
        means = {"x": None, "y": None, "z" : None}
        for coord in means.keys():
            u_coord = chr(ord(coord) -3)
            if f"{u_coord}_mean" not in kwargs:
                # in this case we have to guess umean from the velocity profile in this snapshot
                means[coord] = np.mean(self.contents[f"u{coord}"], axis=(0, 2), keepdims=True) # keep dims to get shape (1, ny, 1) -> broadcastable with (nx, ny, nz)
            else:
                mn = kwargs[f"{u_coord}_mean"]
                if (mn.ndim == 1):
                    means[coord] = mn.reshape((1, mn.shape[0], 1))
                else:
                    means[coord] = mn.reshape((1, *mn.shape))
        def enumerator():
            for i in "xyz":
                for j in range(3):
                    yield (i, j)
        def process(i, j):
            return self.sim.get_nu() * np.mean(self.sim.derive(self.contents[f"u{i}"] - means[i], j, coord_list[j])**2, axis=0)
        #results = Parallel(n_jobs=1)(delayed(process)(i, j) for i,j in enumerator())
        for i, j in enumerator():
            pdiss += process(i,j)
        #psdiss = sum(results)
        self.contents["psdiss"] = pdiss
        return self
    
    def convertToSnapshot(self, sm) -> SnapshotFile:
        log.info(f"Converting Checkpoint at ts={self.timestep} to Snapshot")
        # create data folder.
        datafolder = self.sim.path.parent / "data"
        if not(os.path.exists(datafolder)):
            os.mkdir(datafolder)
        i = self.timestep / self.sim.getInputParam("InOutParam/icheckpoint")
        current_files = SnapshotFile.getExpectedFiles(self.sim, i, datafolder)
        if not os.path.exists(current_files[-1]):
            SnapshotFile.writeconfig(self.sim, current_files[-1], self.timestep)
        s = SnapshotFile(current_files, self.sim, check=False)
        for f in range(len(current_files) -1):
            if not os.path.exists(current_files[f]):
                skip = False
                if (current_files[f].stem[:-5] == "pp"):
                    continue# TODO interpolate p
                self.loadFile()
                contents = s.getContents(index=f)
                for key, value in contents.items():
                    if key[0:3] == "phi":
                        s.contents[key] = self.contents["phi"][:, :, :, int(key[3:]) - 1]
                    else:
                        s.contents[key] = self.contents[key]
                s.isLoaded = True
                s.writeFile(index=f)
            else:
                log.warn("Skipping file, already exists")
        self.unloadContent()
        s.unloadContent()
        inserted = False
        if (sm.getAtTimestep(self.timestep) == None):
            for f in range(len(sm.files) - 1):
                first = sm.files[f]
                second = sm.files[f+1]
                if (first.timestep < self.timestep and self.timestep < second.timestep):
                    sm.files.insert(f+1, s)
                    inserted = True
                    break
            if not inserted:
                sm.files.append(s)
        return s
    def getPhiTensor(self, keep_dims = ["y"]) -> PlaneFile:
        log.info(f"Computing velocity structure tensor for ts={self.timestep}")
        if not self.isLoaded:
            self.loadFile()
        nx, ny, nz = self.sim.getMeshSize()
        Rhat_ij = np.zeros((nx, ny, nz, 3, 3))
        iterate = {i : i in keep_dims for i in "xyz"}
        for x in range(nx if iterate["x"] else 1):
            for y in range(ny if iterate["y"] else 1):
                for z in range(nz if iterate["z"] else 1):
                    fftslicer = [index if iterate[ax] else slice(None) for ax, leng, index in zip("xyz",  [nx, ny, nz], [x, y, z])]
                    ulist = [np.fft.fftn(self.contents[var][tuple(fftslicer)]) for var in ["ux", "uy", "uz"]]
                    for u1 in range(3):
                        for u2 in range(3):
                            Rhat_ij[tuple(fftslicer + [u1, u2])] = np.real(np.conj(ulist[u1]) * ulist[u2]) / np.prod(ulist[u1].shape)
        return Rhat_ij


class StretchingFile(SimulationFile):
    def __init__(self, fn, sim):
        super().__init__(fn, sim, True)
    def loadFile(self, index=0) -> StretchingFile:
        self.contents = np.loadtxt(self.fn[0])
        self.isLoaded = True
        return self
    def CheckForPlausibility(self, sim : simulation.Simulation) -> bool:
        self.length = self.contents.shape[0]
        ylen = sim.getInputParam("BasicParam/ny")
        if (ylen != self.length):
            log.error(f"""Stretching file contains only {self.length} entries, expected ny={ylen}.""")
            return False
        return True

import configparser
class PlaneFile(TimeDependentFileMixin, NumpyArrayFile):
    dtype='<f8'
    def __init__(self, fn, sim, timefile=None, check=True, timestep=None):
        self.isavephi2d = sim.getInputParam("InOutParam/isavephi2d")
        self.numscalar = sim.getInputParam("BasicParam/numscalar")
        if fn is not None:
            self.fn = fn
            self.readInfoFile()
            self.idplane = int(fn[0].stem[3:])
            row = timefile[timefile[:, 0] == self.idplane]
            self.idtime = row[0][2]
            self.axis = fn[0].stem[0]
            super().__init__(self.idtime, fn, sim, check=check)
        else:
            super().__init__(timestep, fn, sim, check=check)
    def readInfoFile(self):
        
        self.info = configparser.ConfigParser()
        self.info.read(self.fn[1])
        
    def getContents(self, index: int):
        planes = self.info["planes"]
        self.planepos = np.array(planes[f"{self.axis}plpos"].split()).astype(np.float)
        self.index_pl = np.array(planes[f"id{self.axis}pl"].split()).astype(np.int)
        n = {}
        nump = self.info["domain"]
        n["x"] = int(nump["nx"])
        n["y"] = int(nump["ny"])
        n["z"] = int(nump["nz"])
        fields = 3 + (self.numscalar if self.isavephi2d else 0)
        planes = len(self.index_pl)
        standard_size = [n[j] if j != self.axis else fields * planes for j in n.keys() ]
        return {"data": standard_size}
    def loadFile(self) -> PlaneFile:
        super().loadFile()
        if "data" not in self.contents: # file was probably already loaded
            return
        data = self.contents["data"]
        idx = 0
        n = {}
        nump = self.info["domain"]
        n["x"] = int(nump["nx"])
        n["y"] = int(nump["ny"])
        n["z"] = int(nump["nz"])
        step = 3 + (self.numscalar if self.isavephi2d else 0)
        def indices(start):
            return [start + step * i for i in range(len(self.index_pl))]
        def getSlicer(start):
            return tuple([slice(None) if j != self.axis else indices(start) for j in n.keys() ])
        plane_ax = list(n.keys()).index(self.axis)
        idx = 0
        for direction in n.keys():
            slicer = getSlicer(idx)
            self.contents[f"u{direction}"] =  np.swapaxes(data[getSlicer(idx)], plane_ax, 2)
            idx += 1
        if (self.isavephi2d > 0):
            for scalar in range(self.numscalar):
                self.contents[f"phi_{scalar}"] = np.swapaxes(data[getSlicer(idx)], plane_ax, 2)
                idx += 1
        del self.contents["data"]
        return self

    def getDimensionFactor(self, key: str, wall_units: bool = False):
        return NotImplemented
    def computePhiTensor(self) -> PlaneFile:
        log.info(f"Computing velocity structure tensor for ts={self.timestep}")
        if not self.isLoaded:
            self.loadFile()
        Rhat_ij = np.zeros((self.contents["ux"].shape[0], self.contents["ux"].shape[1], 3, 3, len(self.index_pl)))
        for idx in range(len(self.index_pl)):
            ulist = [np.fft.fftn(self.contents[var][:, :, idx]) for var in ["ux", "uy", "uz"]]
            for u1 in range(3):
                for u2 in range(3):
                    Rhat_ij[:, :, u1, u2, idx] = np.conj(ulist[u1]) * ulist[u2] / ulist[0].shape[0] / ulist[0].shape[1]
        self.contents["Phi"] = Rhat_ij
        return self

class SnapshotFile(TimeDependentFileMixin, NumpyArrayFile):
    dtype='<f8'
    def __init__(self, fn, sim, check=True):
        self.fn = fn
        self.info = configparser.ConfigParser()
        self.info.read(self.fn[-1])
        super().__init__(float(self.info["time"]["itime"]), fn, sim, check=check)
    def getContents(self, index):
        nump = self.info["domain"]
        filename = self.fn[index]
        standard_size = [int(nump[f"n{coord}"]) for coord in ["x", "y", "z"]]
        return {filename.name[:-5] : standard_size}
    def loadFile(self, index=0, only=[]) -> SnapshotFile:
        for i in range(len(self.fn)-1):
            super().loadFile(i, only=only)
            self.isLoaded = False
        self.isLoaded = True
        return self
    def getDimensionFactor(self, key: str, wall_units: bool = False):
        return NotImplemented
    @staticmethod
    def getExpectedFiles(sim, index, datafolder):
        as_str = f"{int(index):05d}"
        current_files = [datafolder / f"u{coord}{as_str}" for coord in ["x", "y", "z"]]
        current_files += [datafolder / f"pp{as_str}"]
        if (sim.getInputParam("BasicParam/ilmn")):
            current_files += [datafolder / f"rho{as_str}"]
        numscalar = sim.getInputParam("BasicParam/numscalar")
        if (numscalar > 0):
            for n in range(1, numscalar+1):
                current_files += [datafolder / f"phi{n}{as_str}"]
        current_files += [datafolder / f"snap{as_str}.ini"]
        return current_files
    @staticmethod
    def writeconfig(sim, filename, timestep):
        info = configparser.ConfigParser()
        info["domain"] = {"nx" : sim.getInputParam("BasicParam/nx"), "ny" : sim.getInputParam("BasicParam/ny"), "nz" : sim.getInputParam("BasicParam/nz"), 
                          "istret" : sim.getInputParam("BasicParam/istret"), "beta" : sim.getInputParam("BasicParam/beta"), "Lx" : sim.getInputParam("BasicParam/xlx"),
                          "Ly" : sim.getInputParam("BasicParam/yly"), "lz" : sim.getInputParam("BasicParam/zlz")}
        info["time"] = {"itime" : timestep, "dt" : sim.getInputParam("BasicParam/dt"), "t" : sim.getInputParam("BasicParam/dt") * timestep}
        with open(filename, "w") as configfile:
            info.write(configfile)
class StatisticsFile(TimeDependentFileMixin, NumpyArrayFile):
    dtype='<f8'
    def __init__(self, fn, sim, direction, timestep=None, deltat=None):
        self.direction = direction
        self.fn = fn
        if (self.fn is not None):
            self.readInfoFile()
            self.deltat = int(self.info["statistics"]["ofreq2d"])
            super().__init__(int(self.info["time"]["itime"]), fn, sim)
        else:
            self.deltat = deltat
            super().__init__(int(timestep), [], sim)
            if not isinstance(sim.path, list):
                self.fn = [sim.path.parent / "stats2d" / "xstats_mean"]
        
        self.sim.compute_coordinates()
        self.y = self.sim.y
        self.otherdim = self.sim.x if direction == "z" else self.sim.z 
        
    def readInfoFile(self):
        self.info = configparser.ConfigParser()
        self.info.read(self.fn[-1])
    def getContents(self, index):
        stat = self.info["statistics"]
        istat = int(stat[f"istat{self.direction}"])
        n = {coord : self.sim.getInputParam(f"BasicParam/n{coord}") for coord in ["x", "y", "z"]}

        standard_size = [n[j] for j in n.keys() if j != self.direction]
        
        if index == 0:
            contents = ["u", "v", "w", "p"]
            if istat > 1:
                contents += ["uu", "uv", "uw", "vv", "vw", "ww", "pp", "up", "vp", "wp"]
            if istat > 2:
                contents += ["uuu", "uuv", "uuw", "uvv", "uvw", "uww", "vvv", "vvw", "vww", "www"]
            contents = ["*".join(i) for i in contents]
        else:
            contents = [f"phi{index}"]
            if istat > 1:
                contents += [f"{idx}*phi{index}" for idx in ["u", "v", "w", f"phi{index}"]]
            if istat > 2:
                contents += [f"{idx}*phi{index}*phi{index}" for idx in ["u", "v", "w"]]
        return {f"<{cont}>" : standard_size for cont in contents}
    def computeReynoldsStresses(self) -> StatisticsFile:
        if not self.isLoaded:
            self.loadFile()
        for key in list(self.contents): # copy the keys since we'll change the dict
            if not(key[0] == "<" and key[-1] == ">" and "'" not in key and "~" not in key):
                # only treat averaged quantities which are not averaged fluctuations
                continue
            quantities = key[1:-1].split("*")
            if len(quantities) == 2:
                q1 = quantities[0]
                q2 = quantities[1]
                self.contents[f"<{q1}'*{q2}'>"] = self.contents[f"<{q1}*{q2}>"] - \
                    self.contents[f"<{q1}>"] * self.contents[f"<{q2}>"]
            if len(quantities) == 3:
                q1 = quantities[0]
                q2 = quantities[1]
                q3 = quantities[2]
                self.contents[f"<{q1}'*{q2}'*{q3}'>"] = self.contents[f"<{q1}*{q2}*{q3}>"] \
                    - self.contents[f"<{q1}>"] * self.contents[f"<{q2}*{q3}>"] \
                    - self.contents[f"<{q2}>"] * self.contents[f"<{q1}*{q3}>"] \
                    - self.contents[f"<{q3}>"] * self.contents[f"<{q1}*{q2}>"] \
                    + 2 * self.contents[f"<{q1}>"] * self.contents[f"<{q2}>"] * self.contents[f"<{q3}>"]
        return self
    def computeLumleyInvariants(self) -> StatisticsFile:
        re_tensor = np.array([[self.contents["<u'*u'>"], self.contents["<u'*v'>"], self.contents["<u'*w'>"]], 
                              [self.contents["<u'*v'>"], self.contents["<v'*v'>"], self.contents["<v'*w'>"]], 
                              [self.contents["<u'*w'>"], self.contents["<v'*w'>"], self.contents["<w'*w'>"]]])
        if (re_tensor.ndim == 3):
            re_tensor = np.expand_dims(re_tensor, axis=3)
        trace = np.trace(re_tensor) 


        b_tensor = re_tensor / trace[None, None, :, :]
        for i in range(3):
            b_tensor[i, i, :, :] -= 1.0 / 3

        eta = np.sqrt(np.einsum('ij...,ji...->...', b_tensor, b_tensor) / 6)
        zeta = (np.einsum('ij...,jk...,ki...->...', b_tensor, b_tensor, b_tensor) / 6)
        zeta = zeta**(1.0/3)

        # check that excercise 11.4 from Pope is fulfilled

        eig = np.linalg.eig(np.swapaxes(np.swapaxes(b_tensor, 2, 0), 3, 1))[0]
        eta_2 = np.sqrt((eig[:, :, 0] **2 + eig[:, :, 0] * eig[:, :, 1] + eig[:, :, 1] **2)/3)

        zeta_2 = (-eig[:, :, 0]  * eig[:, :, 1] * (eig[:, :, 1] + eig[:, :, 0]) / 2 )**(1.0/3)

        assert (np.allclose(eta, eta_2))
        #assert (np.allclose(zeta, zeta_2))

        self.contents["zeta"] = zeta
        self.contents["eta"] = eta
        return self
    def computeSwirlingStrength(self, signed=True) -> StatisticsFile:
        """Compute the swirling strength as indicated in Anderson 2015. Assumes statistical homogeneity in x direction, therefore. Only available for x-statistics"""
        if self.direction == "z":
            raise ValueError("swirling strength only supported for x statistics")
        dudy, dvdy, dwdy = [np.gradient(self.contents[f"<{x}>"], self.y, axis=0, edge_order = 2) for x in "uvw"]
        dudz, dvdz, dwdz = [np.gradient(self.contents[f"<{x}>"], self.otherdim, axis=1, edge_order = 2) for x in "uvw"]
        mat = np.zeros((len(self.y), len(self.otherdim), 3, 3))
        mat[:, :, 1, 0] = 0 #dudy
        mat[:, :, 1, 1] = dvdy
        mat[:, :, 1, 2] = dwdy
        mat[:, :, 2, 0] = 0 #dudz
        mat[:, :, 2, 1] = dvdz
        mat[:, :, 2, 2] = dwdz
        eigval, eigvec = np.linalg.eig(mat)
        
        self.contents["swirl"] = np.max(np.abs(eigval.imag), axis=2)
        omega = dwdy - dvdz
        if signed:
            self.contents["swirl_signed"] = self.contents["swirl"] * np.sign(omega)
        return self

    def computeVelocityDeficit(self) -> StatisticsFile:
        self.sim.compute_coordinates()
        self.contents["uh-u"] = self.contents["<u>"][int(int(self.sim.getInputParam("BasicParam/ny")) / 2)] - self.contents["<u>"]
        return self
    def computeTKEChannelBudget(self) -> StatisticsFile:
        if len(self.contents["<v>"].shape) != 1:
            raise ValueError("Data hasn't been flattened")
                
        # production P = -<u_i'*u_j'>*(d<u_i>/dx_j) = -<u_i'*v'>*(d<u_i>/dy) 
        self.contents["P"] = - (self.sim.derive(self.contents["<u>"], 0, "y") * self.contents["<u'*v'>"]
                              + self.sim.derive(self.contents["<v>"], 0, "y") * self.contents["<v'*v'>"]
                              + self.sim.derive(self.contents["<w>"], 0, "y") * self.contents["<v'*w'>"])
        # viscous diffusion = nu*(d²k/dx_jdx_j) = nu*(d²k/d_y²)
        self.contents["k"] = 0.5 * sum([self.contents[f"<{coord}'*{coord}'>"] for coord in ["u", "v", "w"]])
        self.contents["viscdiff"] = self.sim.get_nu() * self.sim.derive(self.contents["k"], 0, "y", 2)

        # turbulent transport = -1/2 d(<u_i'*u_i'*u_j*>)/dx_j = -1/2 d(<vu_i'*u_i*>)/dy
        self.contents["ttrans"] = - 0.5 * (self.sim.derive(self.contents["<u'*u'*v'>"] + self.contents["<v'*v'*v'>"] + self.contents["<v'*w'*w'>"], 0, "y"))

        # pressure transport ptr = - d(<u_j'p'>/rho)/dx_j = - d(<v'p'>/rho)/dy
        self.contents["ptr"] = - self.sim.derive(self.contents["<v'*p'>"], 0, "y")
        return self
    def computeTKEBudget(self) -> StatisticsFile:
        # production P = -<u_i'*u_j'>*(d<u_i>/dx_j)
        self.contents["P"] = - (self.sim.derive(self.contents["<u>"], 0, "y") * self.contents["<u'*v'>"]
                              + self.sim.derive(self.contents["<v>"], 0, "y") * self.contents["<v'*v'>"]
                              + self.sim.derive(self.contents["<w>"], 0, "y") * self.contents["<v'*w'>"]
                              + self.sim.derive(self.contents["<u>"], 1, "z") * self.contents["<u'*w'>"]
                              + self.sim.derive(self.contents["<v>"], 1, "z") * self.contents["<v'*w'>"]
                              + self.sim.derive(self.contents["<w>"], 1, "z") * self.contents["<w'*w'>"])
        # viscous diffusion = nu*(d²k/dx_jdx_j) = nu*(d²k/d_y²)
        self.contents["k"] = 0.5 * sum([self.contents[f"<{coord}'*{coord}'>"] for coord in ["u", "v", "w"]])
        self.contents["viscdiff"] = self.sim.get_nu() * (self.sim.derive(self.contents["k"], 0, "y", 2) + self.sim.derive(self.contents["k"], 1, "z", 2)) 

        # turbulent transport = -1/2 d(<u_i'*u_i'*u_j*>)/dx_j = -1/2 d(<vu_i'*u_i*>)/dy
        self.contents["ttrans"] = - 0.5 * (self.sim.derive(self.contents["<u'*u'*v'>"] + self.contents["<v'*v'*v'>"] + self.contents["<v'*w'*w'>"], 0, "y")
                                         + self.sim.derive(self.contents["<u'*u'*w'>"] + self.contents["<v'*v'*w'>"] + self.contents["<w'*w'*w'>"], 1, "z"))

        # pressure transport ptr = - d(<u_j'p'>/rho)/dx_j = - d(<v'p'>/rho)/dy
        self.contents["ptr"] = - self.sim.derive(self.contents["<v'*p'>"], 0, "y") - self.sim.derive(self.contents["<w'*p'>"], 1, "z") 

        # mean flow convection mconv
        self.contents["mconv"] = -self.sim.derive(self.contents["<v>"] * self.contents["k"], 0, "y") - self.sim.derive(self.contents["<w>"] * self.contents["k"], 1, "z")
        return self
    def computeTripleDecomposition(self) -> StatisticsFile:
        if len(self.contents["<u>"].shape) == 1:
            raise ValueError("Data must not be flattened in advance")
        flattened = self.flatten("z").computeReynoldsStresses()
        contents =[list(self.getContents(i).keys()) for i in range(len(self.fn))]
        contents = [item for sublist in contents for item in sublist]

        for key in contents:
            quantities = key[1:-1].split("*")
            if len(quantities) > 2:
                continue
            elif len(quantities) == 1:
                name = quantities[0]
                self.contents[f"<{name}~>"] = self.contents[f"<{name}>"] - flattened.contents[f"<{name}>"][:, None]
                self.contents[f"<{name}''>"] = self.contents[f"<{name}~>"] - self.contents[f"<{name}~>"] 
            else:
                n1, n2 = quantities
                #old
                self.contents[f"<{n1}~*{n2}~>"] = self.contents[f"<{n1}~>"] * self.contents[f"<{n2}~>"]
                self.contents[f"<{n1}''*{n2}''>"] = self.contents[f"<{n1}'*{n2}'>"] - self.contents[f"<{n1}~*{n2}~>"]
        return self


    def computeLongitudinalVorticity(self) -> StatisticsFile:
        dwdy = self.sim.derive(self.contents["<w>"], 0, "y")
        dvdz = self.sim.derive(self.contents["<v>"], 1, "z")
        self.contents["<omega_x>"] = dwdy - dvdz
        return self
    def computeStreamwiseEnstrophy(self) -> StatisticsFile:
        self.computeLongitudinalVorticity()
        integrand = 0.5 * self.contents["<omega_x>"]**2
        # in order to fully cover the domain, we need to append along the periodic axes.
        otherdim = self.otherdim
        # only otherdim can be periodic, so check which one it is 
        otherax = [i for i in ["x", "z"] if i != self.direction][0]
        idx = self.get_dimension_index(otherax)
        if self.sim.getInputParam(f"BasicParam/ncl{otherax}n") == 0:
            slc = [slice(None), slice(None)]
            slc[idx] = None
            first_slc = [slice(None), slice(None)]
            first_slc[idx] = 0
            integrand = np.append(integrand, integrand[tuple(first_slc)][tuple(slc)], axis=idx)
            otherdim = np.append(self.otherdim, [self.sim.getInputParam(f"BasicParam/{otherax}l{otherax}")])
            # append to 
        result = np.trapz(np.trapz(integrand, otherdim, axis=idx), self.y, axis=self.get_dimension_index("y"))
        # now divide by area
        lengths = [self.sim.getInputParam(f"BasicParam/{i}l{i}") for i in ["x", "y", "z"] if i != self.direction]
        result = result / lengths[0] / lengths[1] 
        self.contents["Omega_x"] = result
        return self
    def computeSkinFriction(self) -> StatisticsFile:
        if not self.isLoaded:
            self.loadFile()
        # first average u field over y if not already done
        if (len(self.contents["<u>"].shape) > 1):
            u_avg = self.averageDimension(self.contents["<u>"], dim=1, mirror_symmetry=False)
        else:
            u_avg = self.contents["<u>"]
        u_cl = u_avg[int(len(u_avg)/2)]
        u_b = np.trapz(u_avg, self.y) / 2
        u_tau = self.getDimensionFactor("<u>", True)
        self.contents["cf"] = 2*(u_tau / u_cl)**2
        self.contents["Cf"] = 2*(u_tau / u_b)**2
        return self

    def getDimensionFactor(self, key: str, wall_units : bool = False):
        u_avg = self.contents["<u>"]
        if (len(self.contents["<u>"].shape) > 1):
            u_avg = self.averageDimension(self.contents["<u>"], dim=1, mirror_symmetry=False)
        ub_neu = np.trapz(u_avg, self.y) / self.y[-1]
        velocity_scaling = ub_neu # type: ignore
        if wall_units:
            #idx = cp.files[0].headers.index("Tauw")   
            #wall_stress = np.average(region[:, idx])
            wall_stress_new = self.getWallStress(3)
            velocity_scaling = np.sqrt(wall_stress_new)
        pressure_scaling = (velocity_scaling**2)
        temp_scaling = 1
        length_scaling = (self.sim.get_nu() / velocity_scaling) if wall_units else 1
        # first, get velocity scaling from monitor
        #cp = self.sim.monitor
        #region = cp.getBetweenTimesteps(self.timestep, self.timestep + self.deltat)
        #idx = cp.files[0].headers.index("UBulk")
        scalefactors = {"u": velocity_scaling, "v" : velocity_scaling, "w": velocity_scaling, "p": pressure_scaling, "phi1" : temp_scaling, 
                        "omega_x" : velocity_scaling / length_scaling}
        if not(key[0] == "<" and key[-1] == ">"):
            if key in ["k", "Omega_x"]:
                return velocity_scaling**2
            if key in ["ttrans", "ptr", "P", "mconv"]:
                return velocity_scaling**3  / length_scaling
            if key == "viscdiff":
                return velocity_scaling**2 * self.sim.get_nu() / length_scaling**2
            if key == "psdiss":
                return velocity_scaling**2 * self.sim.get_nu() / length_scaling**2
            if key == "uh-u":
                return velocity_scaling
            if key in ["swirl","swirl_signed", "eta", "zeta", "Cf", "cf"]:
                return 1

            raise ValueError("Dimension of key not understood")
        else:
            return np.prod([scalefactors[i.replace("'", "").replace("~", "")] for i in key[1:-1].split("*")]) 
    @classmethod
    def averageDimension(cls, array, dim, mirror_symmetry):
        s = np.sum(array, axis=dim) / array.shape[dim]
        if (mirror_symmetry):
            s = cls.flip_and_sum(array, 0)
        return s
    @classmethod
    def flip_and_sum(cls, array, dim, opposite_sign=False):
        if opposite_sign:
            return (array - np.flip(array, dim))/2
        else:
            return (np.flip(array, dim) + array)/2
    def getWallStress(self, degree=3, flatten=True, direction="u"):
        self.sim.compute_coordinates()
        if not self.isLoaded:
            self.loadFile()
        velocity = self.contents[f"<{direction}>"]

        flatten_idx = self.get_dimension_index("x" if self.direction == "z" else "z")
        if (len(velocity.shape) > 1 and flatten):
            # First, flatten the contents along the axis we found.
            # No reason to symmetrize here. If the user wanted that, foldChannel() would have been called.
            velocity = self.averageDimension(velocity, flatten_idx, False)
        if (len(velocity.shape) == 1):
            # Fit a polynomial to the first 0:degree values:
            poly = np.polyfit(self.y[0:degree+1], velocity[0:degree+1], degree)
            # Compute the first derivative of the polynomial.
            deriv = poly[-2]
            return deriv * self.sim.get_nu()
        else:
            result = np.zeros((self.contents["<u>"].shape[flatten_idx]))
            vel_swapped = np.swapaxes(velocity, flatten_idx, 0)
            for i in range(len(result)):
                # Fit a polynomial to the first 0:degree values:
                poly = np.polyfit(self.y[0:degree+1], vel_swapped[i, 0:degree+1], degree)
                # Compute the first derivative of the polynomial.
                deriv = poly[-2]
                result[i] = deriv * self.sim.get_nu()
            return result
    def flatten(self, dimension: str) -> StatisticsFile:
        contents_flattened = {}
        if (not self.isLoaded):
            raise ValueError("Load file first")
        idx = self.get_dimension_index(dimension)
        for key, value in self.contents.items():
            contents_flattened[key] = self.averageDimension(value, idx, False)
        file = StatisticsFile(None, self.sim, self.direction, self.timestep, self.deltat)
        file.contents = contents_flattened
        file.isLoaded = True
        # carry y over in case mirror symmetry was already applied
        file.y = self.y
        file.otherdim = self.otherdim
        return file
    def get_dimension_index(self, dimension):
        current_dims = [i for i in ["x", "y", "z"] if i != self.direction]
        return current_dims.index(dimension)
    def writeFile(self, as_mean=True, **kvargs):
        contents = None
        if as_mean:
            self.computeReynoldsStresses()
            orig_cont = self.getContents(0)
            # The fortran postprocessing expects fluctuations instead of the raw products, so let's create a new dict
            contents = {}
            for key,value in orig_cont.items():
                quantities = key[1:-1].split("*")
                if len(quantities) == 1:
                    contents[key] = value
                else:
                    contents["<" + "*".join([f"{q}'" for q in quantities]) + ">"] = value
        super().writeFile(contents=contents, **kvargs)
    def foldChannel(self) -> StatisticsFile:
        contents_flattened = {}
        if (not self.isLoaded):
            raise ValueError("Load file first")
        idx = 0 if self.direction == "x" else 1

        slicer = [slice(None), slice(None)]
        if len(self.contents["<u>"].shape) == 1:
            idx = 0
            slicer = [slice(None)]
        
        for key, value in self.contents.items():
            if value.ndims == 0:
                contents_flattened[key] = value # don't process integral quantities
                continue
            slicer[idx] = slice(0, int(value.shape[idx] / 2 + 1))
            opposite_sign = False
            quantities = key[1:-1].split("*")
            for q in quantities:
                if "v" in q:
                    opposite_sign = not opposite_sign
                # TODO: handle scalar
            contents_flattened[key] = self.flip_and_sum(value, idx, opposite_sign)[tuple(slicer)]
        
        file = StatisticsFile(None, self.sim, self.direction, self.timestep, self.deltat)
        file.contents = contents_flattened
        file.isLoaded = True
        file.y = self.y[0:int(value.shape[idx] / 2 + 1)]
        file.otherdim = self.otherdim
        return file
    @staticmethod
    def phaseaveragefield(field, period, axis):
        slicer = [slice(None), slice(None)]
        contents_averaged = 0
        divisor = field.shape[axis] // period
        for i in range(divisor):
            slicer[axis] = slice(i*period, i*period+period)
            contents_averaged += field[tuple(slicer)] / divisor
        return contents_averaged
    def phaseaverage(self) -> StatisticsFile:
        """Phase-averages a statistics file by using the periodicity of the simulation, as defined in sim.getPeriod.
        Prerequisite: the file has not been flattened, but it may have been folded or time-averaged.
        All quantities, including Reynolds stresses, are phase-averaged and contained in the returned new file."""
        if len(self.contents["<u>"].shape) == 1:
            raise ValueError("Can't phase average flattened file")
        period = self.sim.getPeriod("x" if self.direction == "z" else "z")
        contents_averaged = {}
        idx = 1 if self.direction == "x" else 0
        for key, value in self.contents.items():
            contents_averaged[key] = self.phaseaveragefield(value, period, idx)
        file = StatisticsFile(None, self.sim, self.direction, self.timestep, self.deltat)
        file.contents = contents_averaged
        file.isLoaded = True
        file.y = self.y
        file.otherdim = self.otherdim[0:period]
        return file
    def export_to_npz(self, filename, fields=None, wall_units = True, **other_fields):
        contents = self.contents # self.getDimensionless(wall_units)
        re = self.sim.getInputParam("BasicParam/Re")
        
        coord_dict = {"y": self.y, "y+" : self.y * re}
        if hasattr(self, "otherdim") and len(self.otherdim) != 1:
            coord_dict.update({"z" : self.otherdim, "z+" : self.otherdim * re})
        if (fields != None):
            contents = {k: v for k, v in contents.items() if k in fields}
        np.savez(filename, **coord_dict, **contents, **other_fields)
    def export_self(self, filename=None, **kwdargs):
        if filename is None:
            filename = self.sim.path
            if isinstance(parent, list):
                filename = Path(parent[0])
            filename = filename.parent / "time_averaged.npz"
        self.export_to_npz(filename, self.contents, t_start = self.timestep, t_end = self.deltat + self.timestep, **kwdargs)
        
            

class VelocityForcingFile(NumpyArrayFile):            
    def __init__(self, fn, sim, check=True, direction=None):
        if fn is None:
            self.direction = direction
            self.fn = [sim.path.parent / f"uvelforc.dat"]
            check = False
        else:
            self.fn = fn
            self.direction = fn[0].stem[0]
        super().__init__(self.fn, sim, check)
    def getContents(self, index) -> Dict[str, List]:
        n = {coord : self.sim.getInputParam(f"BasicParam/n{coord}") for coord in ["x", "y", "z"]}
        return {f"uforc": [n["y"], n["z"]] }
    def getDimensionFactor(self, key: str, wall_units: bool = False):
        return NotImplemented
class WallStressBCFile(NumpyArrayFile):
    def __init__(self, fn, sim, check=True, direction=None):
        if fn is None:
            self.direction = direction
            self.fn = [sim.path.parent / f"{self.direction}stressbc.dat"]
            check = False
        else:
            self.fn = fn
            self.direction = fn[0].stem[0]
        super().__init__(self.fn, sim, check)
    def getContents(self, index) -> Dict[str, List]:
        n = {coord : self.sim.getInputParam(f"BasicParam/n{coord}") for coord in ["x", "y", "z"]}
        return {f"{self.direction}bc": [n["x"], n["z"]] }
    def getDimensionFactor(self, key: str, wall_units: bool = False):
        return NotImplemented
    def getPeriod(self, direction):
        if direction == "y":
            return 1
        periods = []
        nx, _, nz = self.sim.getMeshSize()
        for _, field in self.contents.items():
            iterate_over = nx if direction == "z" else nz
            other = nz if direction == "z" else nx
            for x in range(0, iterate_over):
                for z in range(1, other + 1):
                    if (other % z != 0):
                        continue
                    periodic = np.reshape(field[x, :] if direction == "z" else field[:, x], (z, -1))
                    if (z == 1 and np.min(periodic) == np.max(periodic)):
                        # cant compute gradient in this case
                        periods += [1]
                    elif (z > 1):
                        grad = np.gradient(periodic, axis=0)
                        if np.allclose(grad, 0):
                            # period found, add to periods
                            periods += [z]
        return int(other / np.lcm.reduce(periods))

class Datatable1D:
    def __init__(self, sim=None):
        if sim is not None:
            self.sim = sim
            self.colnames = ["y", "y+"]
            self.fields = [sim.y, sim.getDimensionlessAxes(True)[1]]
        else:
            self.colnames = []
            self.fields = []
    def append(self, field, title):
        self.fields += [field]
        self.colnames += [title]
    def export(self, filename):
        field = np.zeros((len(self.fields[0]), len(self.fields)))
        for i in range(len(self.fields)):
            field[:, i] = np.reshape(self.fields[i], len(self.fields[0]))
        np.savetxt(filename, field, delimiter=";", header=";".join(self.colnames), comments='')