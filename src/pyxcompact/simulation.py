from __future__ import annotations
import types
import re
import os
import pathlib
from pyxcompact.log import log, TerminalColors
from pathlib import Path
import numpy as np
from pyxcompact import filechecker
from typing import List, Dict, Optional, Union, Tuple
from pyxcompact.derive import Derivator

def ystretching(yl, ny, beta, istret, derivativeMapping=False):

    yp = np.zeros(ny)
    ypi = np.zeros(ny)
    yeta = np.zeros(ny)
    yetai = np.zeros(ny)
    dyp = np.zeros(ny-1)
    nym = ny - 1

    # see incompact3d tools.f90 up from line 603
    yinf = -yl/2
    den = 2*beta*yinf

    xnum = -yinf - np.sqrt(np.pi * np.pi * beta * beta + yinf * yinf)
    alpha = np.abs(xnum / den)
    xcx = 1/beta/alpha

    if alpha != 0:
        if istret == 1:
            yp[0] = 0
            yeta[0] = 0
        if istret == 2:
            yp[0] = 0
            yeta[0] = -0.5
        if istret == 3:
            yp[0] = 0
            yeta[0] = -0.5

        for j in range(2, ny+1):
            if istret == 1:
                yeta[j-1] = (j-1)/nym
            if istret == 2:
                yeta[j-1] = (j-1)/nym - 0.5
            if istret == 3:
                yeta[j-1] = (j-1)/nym*0.5 - 0.5

            den1 = np.sqrt(alpha*beta+1)
            xnum = den1/np.sqrt(alpha/np.pi)/np.sqrt(beta)/np.sqrt(np.pi)
            den = 2*np.sqrt(alpha/np.pi)*np.sqrt(beta)*np.pi*np.sqrt(np.pi)
            den3 = ((np.sin(np.pi*yeta[j-1])) *
                    (np.sin(np.pi*yeta[j-1]))/beta/np.pi)+alpha/np.pi
            den4 = 2*alpha*beta-np.cos(2*np.pi*yeta[j-1])+1
            xnum1 = (np.arctan(xnum*np.tan(np.pi*yeta[j-1])))*den4/den1/den3/den
            cst = np.sqrt(beta)*np.pi/(2*np.sqrt(alpha)*np.sqrt(alpha*beta+1))

            if istret == 1:
                if yeta[j-1] < 0.5:
                    yp[j-1] = xnum1-cst-yinf
                if yeta[j-1] == 0.5:
                    yp[j-1] = -yinf
                if yeta[j-1] > 0.5:
                    yp[j-1] = xnum1+cst-yinf

            if istret == 2:
                if yeta[j-1] < 0.5:
                    yp[j-1] = xnum1-cst+yl
                if yeta[j-1] == 0.5:
                    yp[j-1] = yl
                if yeta[j-1] > 0.5:
                    yp[j-1] = xnum1+cst+yl

            if istret == 3:
                if yeta[j-1] < 0.5:
                    yp[j-1] = (xnum1-cst+yl)*2
                if yeta[j-1] == 0.5:
                    yp[j-1] = (yl)*2
                if yeta[j-1] > 0.5:
                    yp[j-1] = (xnum1+cst+yl)*2

    if (alpha == 0):
        yp[0] = -1.e10
        for j in range(2, ny+1):
            yeta[j-1] = (j-1) * (1/ny)
            yp[j-1] = -beta * np.cos(np.pi*yeta[j-1])/np.sin(yeta[j-1]*np.pi)

      # loop for ypi and yetai

    if alpha != 0:
        for j in range(0, ny):
            if istret == 1:
                yetai[j] = (j + 1 - 0.5) / nym
            if istret == 2:
                yetai[j] = (j + 1 - 0.5) / nym - 0.5
            if istret == 3:
                yetai[j] = (j + 1 - 0.5) * 0.5 / nym - 0.5
            den1 = np.sqrt(alpha * beta + 1)
            xnum = den1/np.sqrt(alpha/np.pi) / np.sqrt(beta)/np.sqrt(np.pi)
            den = 2 * np.sqrt(alpha/np.pi) * np.sqrt(beta) * np.pi*np.sqrt(np.pi)
            den3 = (
                (np.sin(np.pi*yetai[j])) * (np.sin(np.pi*yetai[j])) / beta/np.pi) + alpha/np.pi
            den4 = 2*alpha*beta - np.cos(2*np.pi*yetai[j]) + 1
            xnum1 = (np.arctan(xnum*np.tan(np.pi*yetai[j]))) * den4/den1/den3/den
            cst = np.sqrt(beta)*np.pi / (2*np.sqrt(alpha)
                                        * np.sqrt(alpha*beta+1))

        if istret == 1:
            if yetai[j] < 0.5:
                ypi[j] = xnum1-cst-yinf
            if yetai[j] == 0.5:
                ypi[j] = -yinf
            if yetai[j] > 0.5:
                ypi[j] = xnum1+cst-yinf

        if istret == 2:
            if yetai[j] < 0.5:
                ypi[j] = xnum1-cst+yl
            if yetai[j] == 0.5:
                ypi[j] = yl
            if yetai[j] > 0.5:
                ypi[j] = xnum1+cst+yl

        if istret == 3:
            if yetai[j] < 0.5:
                ypi[j] = (xnum1-cst+yl)*2
            if yetai[j] == 0.5:
                ypi[j] = (yl)*2
            if yetai[j] > 0.5:
                ypi[j] = (xnum1+cst+yl)*2

    if (alpha == 0):
        ypi[0] = -1.e10
        for j in range(1, ny):
          yetai[j] = j * (1/ny)
          ypi[j] = -beta * np.cos(np.pi*yetai[j])/np.sin(yetai[j]*np.pi)

    if not derivativeMapping:
        return yp
    else:
        ppy = np.zeros((ny))
        pp2y = np.zeros((ny))
        pp4y = np.zeros((ny))
        ppyi = np.zeros((ny))
        pp2yi = np.zeros((ny))
        pp4yi = np.zeros((ny))
        if (istret != 3):
            for j in range(0,ny):
                ppy[j]=yl*(alpha/np.pi+(1.0/np.pi/beta)*np.sin(np.pi*yeta[j])*np.sin(np.pi*yeta[j]))
                pp2y[j]=ppy[j]*ppy[j]
                pp4y[j]=(-2.0/beta*np.cos(np.pi*yeta[j])*np.sin(np.pi*yeta[j]))
     
            for j in range(0, ny):
                ppyi[j]=yl*(alpha/np.pi+(1.0/np.pi/beta)*np.sin(np.pi*yetai[j])*np.sin(np.pi*yetai[j]))
                pp2yi[j]=ppyi[j]*ppyi[j]
                pp4yi[j]=(-2.0/beta*np.cos(np.pi*yetai[j])*np.sin(np.pi*yetai[j]))
        if (istret == 3):
            pp4y /= 2
            pp4yi /= 2
        return ppy, pp2y, pp4y, ppyi, pp2yi, pp4yi


class Simulation:
    def __init__(self, path):
        self.path = Path(path)
        self.input = Simulation.parseInputfile(self.path, SimulationParameters(), False, False)
        self.monitor = filechecker.MonitorManager(self)
        self.derivators = {}
        self.compute_coordinates(True)
    @staticmethod
    def parseInputfile(path, input=None, createNewAttributes=True, createNewNamespaces=True) -> SimulationParameters:
        if (input is None):
            input = SimulationParameters(False)
        f=open(path, "r")
        current_ns : Optional[Dict] = None
        for line in f:
            line = line.split("!")[0].strip()  # remove comments first
            if len(line) == 0: 
                continue
            elif line[0] == "&" and current_ns == None:
                current_ns_str = re.findall(r"\&(\S*)", line)[0]
                if current_ns_str in input:
                    log.info_heading(f"Reading group '{current_ns_str}'")
                    current_ns = input[current_ns_str]
                elif (createNewNamespaces):
                    log.info_heading(f"Creating new group '{current_ns_str}'")
                    name = current_ns_str
                    current_ns = {}
                    input[name] = current_ns
                else:
                    log.warn(f"Unknown namelist encountered: '{current_ns}'")
                    current_ns = None
            elif line[0:4] == "/End":
                current_ns = None
            elif current_ns is not None:
                value = re.match(r"(\S*?)\s*=\s*(.*)\s*", line)
                if value is None:
                    continue
                elif value.group(1) not in current_ns: 
                    if createNewAttributes:
                        log.info(f"Creating new parameter: '{line}'")
                        current_ns[value.group(1)] = Parameter(value.group(1), 0)
                    else:
                        log.warn(f"Unknown parameter encountered: '{line}'")
                        continue
                result = SimulationParameters.parseParameter(value.group(2))
                if (result == None):
                    log.warn(f"Data type not understood: '{line}'")
                log.info(f"Assigning value {result} to {value.group(1)}")
                current_ns[value.group(1)].assign(result)
        return input
    def compute_coordinates(self, idpre=False):
        if (hasattr(self, "x")):
            return
        self.x = np.linspace(0, self.getInputParam("BasicParam/xlx"), self.getInputParam("BasicParam/nx"), endpoint=False)
        self.dx = np.diff(self.x)[0]

        self.z = np.linspace(0, self.getInputParam("BasicParam/zlz"), self.getInputParam("BasicParam/nz"), endpoint=False)
        self.dz = np.diff(self.z)[0]

        self.y = ystretching(self.getInputParam("BasicParam/yly"), self.getInputParam("BasicParam/ny"),
                             self.getInputParam("BasicParam/beta"), self.getInputParam("BasicParam/istret"))
        self.dy = np.diff(self.y)

        # Coordinates of pressure nodes
        if idpre:
            self.xp = np.arange(self.dx/2, self.getInputParam("BasicParam/xlx"), self.dx)
            self.zp = np.arange(self.dz/2, self.getInputParam("BasicParam/zlz"), self.dz)
            self.yp = (self.y[1:] + self.y[:-1]) / 2
        return
    def getDimensionlessAxes(self, wall_units = False):
        n = {coord : int(self.getInputParam(f"BasicParam/n{coord}")) for coord in ["x", "y", "z"]}
        self.compute_coordinates()
        axes = [getattr(self, j) * (self.getInputParam(f"BasicParam/re") if (j == "y" and wall_units) else 1) for j in n.keys()]
        return axes
    def getTimesteps(self, frequencyKey : str) -> List[Tuple[Simulation, int]]:
        start_time = self.input["BasicParam/ifirst"]
        freq = self.input[frequencyKey]
        end_time = self.input["BasicParam/ilast"]
        return [(self, j) for j in range(start_time, end_time+1) if j % freq == 0]
    def getInputParam(self, key : str):
        return self.input[key]
    def getInputParams(self, key : str) -> List[Tuple[Simulation, Union[int, float, str]]]:
        return [(self, self.getInputParam(key))]
    def setInputParam(self, key : str, value):
        self.input[key] = value
    def getMeshSize(self) -> Tuple[int]:
        nx = self.getInputParam("BasicParam/nx")
        ny = self.getInputParam("BasicParam/ny")
        nz = self.getInputParam("BasicParam/nz")
        return nx, ny, nz
    def getPhysicalSize(self) -> Tuple[float]:
        nx = self.getInputParam("BasicParam/xlx")
        ny = self.getInputParam("BasicParam/yly")
        nz = self.getInputParam("BasicParam/zlz")
        return nx, ny, nz
    def get_nu(self):
        re = self.getInputParam("BasicParam/re")
        if (self.getInputParam("BasicParam/icpg")):
            re = (re/0.116)**(1.0/0.88)
        return 1/re
    def getPeriod(self, axis):
        bc = filechecker.BCManager(self)
        if len(bc.files) == 0:
            return 1
        periods = [f.loadFile().getPeriod(axis) for f in bc.files]
        return np.lcm.reduce(periods)
    def init_derivators(self):
        if len(self.derivators) > 0:
            return
        for c in "xyz":
            self.derivators[f"d1{c}"] = Derivator.create(self, c, 1)
            self.derivators[f"d2{c}"] = Derivator.create(self, c, 2, self.derivators[f"d1{c}"])
    def derive(self, array, axis, direction: str=None, order=1):
        self.init_derivators()
        if direction is None:
            ms = self.getMeshSize()
            occ = ms.count(array.shape[axis])
            if (occ == 1):
                direction = "xyz"[ms.index(array.shape[axis])]
            elif (occ == 0):
                raise ValueError("The size of the array along the specified axis does not match a coordinate axis")
            else:
                raise ValueError("There is more than one coordinate axis with the same length as the array size among the given axis")
        return self.derivators[f"d{order}{direction}"].derive(array, axis)
            





class MultiSimulation(Simulation):
    def __init__(self, paths):
        self.sims = [Simulation(p) for p in paths]
        self.path = paths
        self.derivators = {}
        end = 0
        for i in self.sims:
            first = i.getInputParam("BasicParam/ifirst")
            last = i.getInputParam("BasicParam/ilast")
            if (first < end):
                raise ValueError("First < last of previous simulation. This is not supported")
            end = last
    def getTimesteps(self, frequencyKey : str) -> List[Tuple[Simulation, int]]:
        result = []
        for i in self.sims:
            start_time = i.getInputParam("BasicParam/ifirst")
            freq = i.getInputParam(frequencyKey)
            end_time = i.getInputParam("BasicParam/ilast")
            result += [(i, j) for j in range(start_time, end_time+1) if j % freq == 0]
        return result
    def getInputParams(self, key) -> List[Tuple[Simulation, Union[int, float, str]]]:
        return [(i, i.getInputParam(key)) for i in self.sims]
    def getInputParam(self, key):
        path = key.lower().split("/")
        if (len(path) != 2): # namespace requested
            raise ValueError("Can't return namespaces for MultiParameters")
        last = None
        for i in self.sims:
            if (last is not None and last != i.getInputParam(key)):
                log.error(f"Simulations have different values set for {key}")
            last = i.getInputParam(key)
        return last
    def setInputParam(self, key : str, value):
        for i in self.sims:
            i.input[key] = value

class Parameter:
    def __init__(self, name, default, value=None):
        self.name = name
        self.default = default
        if value is not None:
            self.assign(value)
        else:
            self.assign(default)
    def assign(self, value):
        self.value = value
    def isDefault(self):
        if hasattr(self, "value"):
            if self.value != self.default:
                return False
        return True
    def __repr__(self):
        return f"{self.name}: {self.value} {'(default)' if self.isDefault() else ''}"

class SimulationParameters:
    
    def __init__(self, InitializeDefaults = True):
        self.namespaces = {}
        if (InitializeDefaults):
            self.readDefaultParameters()
    def readDefaultParameters(self):
        namelists=open(Path(os.path.dirname(__file__)) / "namelists.f90", "r")
        params = open(Path(os.path.dirname(__file__)) / "default_parameters.f90", "r")
        defaults = "\n".join(params.readlines())
        nl = "".join(namelists.readlines()).replace("\r","").replace("\n","").replace("&","")
        nl = nl.split("NAMELIST")
        for i in nl:
            ns = re.findall(r"/(.*)/", i)
            if len(ns) == 0:
                continue
            ns = ns[0].strip()
            self[ns] = {}
            names = re.findall(r"([\w_\d]+?)[,|\s]", i)
            for n in names:
                self[ns][n.strip()] = Parameter(n, SimulationParameters.getFromFile(n, defaults))
        namelists.close()
        params.close()
    def __getitem__(self, key):
        path = key.lower().split("/")
        if (len(path) == 1): # namespace requested
            return self.namespaces[path[0]]
        return self.namespaces[path[0]][path[1]].value

    def __setitem__(self, key, value):
        path = key.lower().split("/")
        if (len(path) == 1): # set namespace
            self.namespaces[path[0]] = value
            return
        if path[0] not in self.namespaces:
            self.namespaces[path[0]] = {}
        ns = self.namespaces[path[0]]
        if isinstance(value, Parameter):
            ns[path[1]] = value
        elif path[1] in ns and isinstance(ns[path[1]], Parameter):
            ns[path[1]].value = value
        else:
            ns[path[1]] = Parameter(key[1], 0, value)
    def __contains__(self, key):
        path = key.lower().split("/")
        ns_exists = path[0] in self.namespaces
        if len(path) == 1:
            return ns_exists
        else:
            return ns_exists and path[1] in self.namespaces[path[0]]

    @staticmethod
    def getFromFile(name, params):
        value = re.findall(name + r"\s*=\s*(.*)\s*\n", params)
        if len(value) == 0:
            return None
        result = SimulationParameters.parseParameter(value[0])
        return result
    @staticmethod
    def parseParameter(input):
        value = input.split("!")[0].strip() # remove trailing comment
        simple_values = {"one": 1.0, "zero": 2.0, ".TRUE.": True, ".FALSE.": False, "huge(i)": 2147483647}
        if value in simple_values:
            return simple_values[value]
        if "'" in value:
            return value.replace("'", '')
        try:
            return int(value)
        except ValueError:
            try:
                return float(value)
            except ValueError:
                return None
    def __repr__(self):
        result = f"{TerminalColors.HEADER}Parameter summary:\n{TerminalColors.ENDC}"
        for x in vars(self):
            result += f"{TerminalColors.BOLD}{x}{TerminalColors.ENDC}\n"
            for y in sorted(vars(self)[x]):
                result += f"   {vars(self)[x][y]}\n"
        return result
    """def add_ns(self, namespace):
        setattr(self, namespace, types.SimpleNamespace())
    def add_attr(self, path, value):
        ns, attribute = path.split("/")
        if not(hasattr(self, ns)):
            self.add_ns(ns)
        if not(isinstance(value, Parameter)):
            value = Parameter(attribute, 0, value)
        setattr(getattr(self, ns), attribute, value)"""

                
def write_submitfile(path, partition, nodes, time, name, write_develop = True):

    if partition[-2:] == "_e":
        tpn = 28
    else:
        tpn = 40
    output = f"""#!/bin/bash
#SBATCH -p {partition}
#SBATCH --nodes={nodes}
#SBATCH --ntasks-per-node={tpn}
#SBATCH --time={time}:00
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jonathan.neuhauser@kit.edu
#SBATCH --job-name={name}
#SBATCH --export=ALL

    module purge
    module load compiler/intel/19.1
    module load mpi/openmpi/4.0
    module load numlib/mkl/2019

    mpirun --bind-to core --map-by core -report-bindings ~/source/xcompact3d-jh/xcompact3d"""

    if Path(path).is_dir():
        if write_develop: 
            write_submit_develop(path, partition, name)
        path = Path(path) / "submit_job.sh"
    elif write_develop:
        raise ValueError("Provide directory to write develop file")
    
    with open(path, "w") as f:
        f.write(output)

def write_submit_develop(path, partition, name):
    if Path(path).is_dir():
        path = Path(path) / "submit_develop.sh"
    if not partition.startswith("dev"):
        partition = "dev_" + partition
    name += "_dev"
    write_submitfile(path, partition, 2, "00:30", name, write_develop=False)


