from pyxcompact.simulation import Simulation, MultiSimulation
from pyxcompact.filechecker import Stats2DManager, CheckpointManager, SnapshotManager, BCManager
from pyxcompact.simulationfile import StatisticsFile
import matplotlib.pyplot as plt
import numpy as np
from scipy import interpolate

def channel_plot_phase_averaged_secondary_flow_internal(datadict, quiver: bool = False, streamplot: bool = False, 
    u_contour: bool = False, u_contourf: bool = False, magnitude: bool = False, swirling_strength : bool = False, y_pos = None, z_pos = None, frameless=False, 
    swirl_max=1.5, u_levels=None, size_x=10, size_y=2, initialize=True, speedfactor=100, density=1, speedmax=None, shift=1/4, plot_boundary=None, normalize_u=True, treat_as_u="<u>", **kwargs):
    if (initialize):
        if (frameless):
            plt.figure(figsize=(size_x, size_y), frameon=False)
            plt.gcf().set_size_inches(size_x, size_y, forward=False)
            plt.gcf().add_axes([0, 0, 1, 1])
            plt.axis("off")
        else:
            plt.figure(figsize=(size_x, size_y))
    u_tau = datadict["u_tau"]
    period = datadict["period"]
    shift = int(shift * period)
    # scale velocities like Chung 2015
    u_normalized = (datadict[treat_as_u] - (np.mean(datadict[treat_as_u][0, :]) if normalize_u else 0)) / datadict["u_normalizer"]
    u_normalized = np.roll(u_normalized, shift, 1)
    u_normalized = np.append(u_normalized, u_normalized[:, 0][:, None], axis=1)
    w_normalized = (datadict["<w>"])

    newx = np.append(datadict["z"] , np.array(datadict["z"][-1]+(datadict["z"][1] - datadict["z"][0])))
    mg = np.meshgrid(newx, datadict["y"])
    positions = np.vstack(map(np.ravel, mg))
    exported = np.zeros((positions.shape[1], 3))
    exported[:, :2] = positions.T
    exported[:, 2] = np.reshape(u_normalized, exported.shape[0])
    def export_u_contour(simname):
        np.savetxt(f"u_contour_{simname}.csv", exported, delimiter=";", header="z; y; <u>", comments='')
    if magnitude:
        speed = np.roll(np.sqrt(datadict["<v>"]**2 + w_normalized**2), shift, axis=1) / u_tau
        print(f"Max speed: {np.max(speed)}")
        speed = np.append(speed, speed[:, 0][:, None], axis=1)
        a = plt.pcolormesh(newx, datadict["y"] , speed, cmap="OrRd", shading="auto", vmax=speedmax)
        if not frameless:
            plt.colorbar(a)
    if u_contourf:
        b = plt.contourf(newx, datadict["y"] , u_normalized, levels=u_levels, **kwargs)
    if u_contour:
        if (u_levels is None):
            b = plt.contour(newx, datadict["y"] , u_normalized, **kwargs)
        else:
            b = plt.contour(newx, datadict["y"] , u_normalized, levels=u_levels, **kwargs)
        
        if not frameless:
            plt.colorbar(b)
    

    if swirling_strength:
        swirl = datadict["swirl_signed"] / u_tau
        limit = swirl_max
        rolled = np.roll(swirl, shift, 1)
        rolled = np.c_[rolled, rolled[:, 0]]
        c = plt.pcolormesh(newx, datadict["y"] , rolled, cmap="seismic", shading='gouraud', vmin=-limit, vmax=limit)
        if not frameless:
            plt.colorbar(c)
    xnew = np.linspace(0, newx[-1], 128) if z_pos is None else z_pos
    ynew = np.linspace(0, datadict["y"] [-1], 128) if y_pos is None else y_pos
    f_v =interpolate.interp2d(datadict["z"] , datadict["y"] , np.roll(datadict["<v>"], shift, 1))
    v_new = f_v(xnew, ynew)
    f_w =interpolate.interp2d(datadict["z"] , datadict["y"] , np.roll(w_normalized, shift, 1))
    w_new = f_w(xnew, ynew)

    if (streamplot):
        speed = np.sqrt(w_new**2 + v_new**2)
        plt.streamplot(xnew, ynew, w_new, v_new, density=density, linewidth=speed*speedfactor)
    if (quiver):
        plt.quiver(xnew, ynew, w_new, v_new, **kwargs)
    if (plot_boundary is not None):
        for i in plot_boundary:
            key = i + "bc"
            if (len(datadict["z"]) < len(datadict[key])):
                datadict[key] = datadict[key][0:len(datadict["z"])]
            plt.plot(datadict["z"], np.roll(datadict[key], shift, 0))
        
    mg = np.meshgrid(xnew, ynew)
    positions = np.vstack(map(np.ravel, mg))
    exported = np.zeros((positions.shape[1], 4))
    exported[:, :2] = positions.T
    exported[:, 2] = np.reshape(w_new, exported.shape[0]) / u_tau
    exported[:, 3] = np.reshape(v_new, exported.shape[0]) / u_tau
    def export_secondary_flow(simname):
        np.savetxt(f"secondary_flow_{simname}.csv", exported, delimiter=";", header="z; y; <w>; <v>", comments='')
    def savefig(simname, suffix = "_uv_frameless"):
        if (frameless):
            plt.savefig(f"{simname}{suffix}.pdf", bbox_inches="tight", pad_inches=0)
        else:
            plt.savefig(f"{simname}{suffix}.png")
    return export_u_contour, export_secondary_flow, savefig
    

def channel_plot_phase_averaged_secondary_flow(sim: Simulation,  start_time: int, end_time: int, fold = True, phaseaverage = True, treat_as_u="<u>", **kwargs):
    datadict = {}
    s = Stats2DManager(sim, "x")

    datadict["period"] = sim.getPeriod("z")
    
    
    file = s.TimeAverageInterval(start_time, end_time)
    if fold:
        file = file.foldChannel()
    if phaseaverage:
        file = file.phaseaverage()
    file.computeVelocityDeficit().computeSwirlingStrength()
    if ("plot_boundary" in kwargs and kwargs["plot_boundary"] is not None):
        bc = BCManager(sim)
        for i in kwargs["plot_boundary"]:
            for f in bc.files:
                if i + "bc" in f.loadFile().contents:
                    datadict[i + "bc"] = f.contents[i+"bc"][0, :]

    datadict.update(file.contents)
    datadict["u_normalizer"] = file.getDimensionFactor(treat_as_u, True)
    datadict["u_tau"] = np.sqrt(file.getWallStress())
    datadict.update({"y" : file.y, "z": file.otherdim})
    return channel_plot_phase_averaged_secondary_flow_internal(datadict, treat_as_u=treat_as_u, **kwargs)
    

def add_streamplot_to_plot(file, field_z, field_y, z_pos = None, y_pos=None, quiver=False, factor=100, start_points=None, density=None, only_random=False, shift=1/4, **kwargs):
   
    if (only_random):
        field_z = field_z - np.average(field_z, axis=1)[:, None]
        field_y = field_y - np.average(field_y, axis=1)[:, None]
    newx = np.append(file.otherdim, np.array(file.otherdim[-1]+(file.otherdim[1] - file.otherdim[0])))
    period = file.sim.getPeriod("z")
    shift = int(shift * period)
    n = 128 if not quiver else 30
    xnew = np.linspace(0, newx[-1], n) if z_pos is None else z_pos
    ynew = np.linspace(0, 1, n) if y_pos is None else y_pos
    f_v =interpolate.interp2d(file.otherdim, file.y, np.roll(field_y, shift, 1))
    v_new = f_v(xnew, ynew)
    f_w =interpolate.interp2d(file.otherdim, file.y, np.roll(field_z, shift, 1))
    w_new = f_w(xnew, ynew)
    X, Y = np.meshgrid(xnew, ynew)

    if density is None:
        if start_points is not None:
            density=20
        else:
            density =2 
    if not quiver:
        speed = np.sqrt(w_new**2 + v_new**2)
        plt.streamplot(xnew, ynew, w_new, v_new, density=density, linewidth=speed*factor, start_points=start_points, arrowsize=0.5)
    else:
        plt.quiver(xnew, ynew, w_new, v_new)

def plot_tke(sim, local_u_tau=True, individual_term = None, at_y=None, frameless=True, initialize=True, shift=1/4, start=50000, end=200000, **kwdargs):#
    if (initialize):
        if (frameless):
            plt.figure(figsize=(4, 2), frameon=False)
            plt.gcf().set_size_inches(4, 2, forward=False)
            plt.gcf().add_axes([0, 0, 1, 1])
            plt.axis("off")
        else:
            plt.figure(figsize=(10, 2))
    simname = sim.path.parent.name
    g = Stats2DManager(sim, "x")
    ax = sim.getDimensionlessAxes(True)
    
    averaged = g.TimeAverageInterval(start, end).computeReynoldsStresses().foldChannel().phaseaverage().computeReynoldsStresses()
    if local_u_tau:
        u_tau = np.sqrt(averaged.getWallStress(flatten=False))
    else:
        u_tau = np.sqrt(averaged.getWallStress(flatten=True))[None]
    tke = 0.5 * (averaged.contents["<u'*u'>"] + averaged.contents["<v'*v'>"] + averaged.contents["<w'*w'>"]) / u_tau[None, :]**2
    if (individual_term is not None):
         tke = averaged.contents[individual_term]/ u_tau[None, :]**2
    period = sim.getPeriod("z") 
    shift = int(shift * period)
    if (at_y == None):
        normalized = np.roll(tke, shift, 1)
        normalized = np.append(normalized, normalized[:, 0][:, None], axis=1)
        newx = np.append(averaged.otherdim, np.array(averaged.otherdim[-1]+(averaged.otherdim[1] - averaged.otherdim[0])))
        plt.pcolormesh(newx, averaged.y, normalized, cmap="viridis", shading="gouraud")
        if not frameless:
            plt.colorbar()
        
    else:
        plt.plot(averaged.otherdim / sim.getPeriod("z"), tke[at_y, :], label=f"tke_{simname}")
        plt.title(f"TKE at y={averaged.y[at_y]}, y+={ax[1][at_y]}")
        plt.legend()
    def savefig(simname=simname):
        if (frameless):
            plt.savefig(f"{simname}_tke_frameless_{np.max(tke)}.pdf", bbox_inches="tight", pad_inches=0)
        else:
            plt.savefig(f"{simname}_{tke}.png")
    return savefig

def plot_tke_term_internal(datadict, term, termminus=None, termminussign=1, initialize=True, frameless=True, rng=None, only_random=False, shift = 1/4):
    if (initialize):
        if (frameless):
            plt.figure(figsize=(4, 2), frameon=False)
            plt.gcf().set_size_inches(4, 2, forward=False)
            plt.gcf().add_axes([0, 0, 1, 1])
            plt.axis("off")
        else:
            plt.figure(figsize=(10, 2))
    utau4nu = datadict["u_tau"]**4 / datadict["nu"] 
    shift = int(shift * datadict["period"])
    field = datadict[term] / utau4nu[None, :]
    if (termminus is not None):
        field += termminussign * datadict[termminus] / utau4nu[None, :]
    if (only_random):
        field = field - np.average(field, axis=1)[:, None]
    normalized = np.roll(field, shift, 1)
    normalized = np.append(normalized, normalized[:, 0][:, None], axis=1)
    if rng is None:
        rng = max(np.max(normalized), -np.min(normalized))
    newx = np.append(datadict["z"], np.array(datadict["z"][-1]+(datadict["z"][1] - datadict["z"][0])))
    plt.pcolormesh(newx, datadict["y"], normalized[0:len(datadict["y"]) // 2+1, :], cmap="seismic", vmin=-rng, vmax=rng, shading="gouraud") # 
    if not frameless:
        plt.colorbar()
        plt.ylabel("y coordinate")
        plt.title(term)
    def savefig(simname, appendix=""):
        if (frameless):
            #extent = plt.gca().get_window_extent().transformed(plt..dpi_scale_trans.inverted())
            #plt.savefig('/tmp/test.png', bbox_inches=extent)
            plt.savefig(f"{simname}_{term}_frameless_{rng}{appendix}.pdf", bbox_inches="tight", pad_inches=0)
        else:
            plt.savefig(f"{simname}_{term}{appendix}.png")
    return savefig
    
def plot_tke_term_test(sim, term, local_u_tau=False, start=100000, end=200000, initialize=True, frameless=True, **kwargs):
    datadict = {}
    start_time = 100000

    nx, ny, nz = sim.getMeshSize()
    simname = str(int(sim.getInputParam("BasicParam/re"))) + f"_{sim.path.parent.name}"
    sim.compute_coordinates()

    g = Stats2DManager(sim, "x")
    
    c = CheckpointManager(sim)
    
    # get local dimension factor of u_tau
    if (initialize):
        if (frameless):
            plt.figure(figsize=(4, 2), frameon=False)
            plt.gcf().set_size_inches(4, 2, forward=False)
            plt.gcf().add_axes([0, 0, 1, 1])
            plt.axis("off")
        else:
            plt.figure(figsize=(10, 2))
    averaged = g.TimeAverageInterval(start, end)

    if local_u_tau:
        u_tau = np.sqrt(averaged.getWallStress(flatten=False))
    else:
        u_tau = np.sqrt(averaged.getWallStress(flatten=True))[None]
    period = sim.getPeriod("z") 
    datadict.update({"u_tau": u_tau, "nu": sim.get_nu(), "period" :  period if period > 1 else nz, "y": averaged.y, "z": averaged.otherdim})

    averaged = averaged.computeReynoldsStresses().computeTKEBudget().phaseaverage().foldChannel()
    if "psdiss" not in averaged.contents and (term == "psdiss" or ("termminus" in kwargs and kwargs["termminus"] == "psdiss")):
        c = CheckpointManager(sim)
        diss = c.getTimeAveragedPseudoDissipation(100000, 200000, u_mean=averaged.contents["<u>"], v_mean=averaged.contents["<v>"], w_mean=averaged.contents["<w>"])
        averaged.contents["psdiss"] = - StatisticsFile.flip_and_sum(StatisticsFile.phaseaveragefield(diss, period, 1), 0)[0:len(sim.y) // 2+1, :]
    
    datadict.update(averaged.contents)
    

    return lambda appendix: (plot_tke_term_internal(datadict, term, **kwargs))(simname, appendix)

def plot_tke_term(sim, term, termminus=None, termminussign=1, initialize=True, frameless=True, local_u_tau=False, rng=None, only_random=False, shift = 1/4, start=50000, end=200000, **kwargs):


    nx, ny, nz = sim.getMeshSize()
    simname = str(int(sim.getInputParam("BasicParam/re"))) + f"_{sim.path.parent.name}"
    sim.compute_coordinates()

    g = Stats2DManager(sim, "x")
    
    c = CheckpointManager(sim)
    
    # get local dimension factor of u_tau
    if (initialize):
        if (frameless):
            plt.figure(figsize=(4, 2), frameon=False)
            plt.gcf().set_size_inches(4, 2, forward=False)
            plt.gcf().add_axes([0, 0, 1, 1])
            plt.axis("off")
        else:
            plt.figure(figsize=(10, 2))
    averaged = g.TimeAverageInterval(start, end)

    if local_u_tau:
        u_tau = np.sqrt(averaged.getWallStress(flatten=False))
    else:
        u_tau = np.sqrt(averaged.getWallStress(flatten=True))[None]
    utau4nu = u_tau **4 / sim.get_nu()
    c = CheckpointManager(sim)
    period = sim.getPeriod("z") 
    period = period if period > 1 else nz
    shift = int(shift * period)
    if "psdiss" not in averaged.contents and (term == "psdiss" or termminus == "psdiss"):
        c = CheckpointManager(sim)
        diss = c.getTimeAveragedPseudoDissipation(start, end, u_mean=averaged.contents["<u>"], v_mean=averaged.contents["<v>"], w_mean=averaged.contents["<w>"])
        averaged.contents["psdiss"] = - diss
    
    averaged = averaged.computeReynoldsStresses().computeTKEBudget().phaseaverage().foldChannel()
    newx = np.append(averaged.otherdim, np.array(averaged.otherdim[-1]+(averaged.otherdim[1] - averaged.otherdim[0])))
    
    # get production and dissipation
    field = averaged.contents[term] / utau4nu[None, :]
    if (termminus is not None):
        field += termminussign * averaged.contents[termminus] / utau4nu[None, :]
    if (only_random):
        field = field - np.average(field, axis=1)[:, None]
    normalized = np.roll(field, shift, 1)
    normalized = np.append(normalized, normalized[:, 0][:, None], axis=1)
    if rng is None:
        rng = max(np.max(normalized), -np.min(normalized))
    plt.pcolormesh(newx, averaged.y, normalized[0:len(sim.y) // 2+1, :], cmap="seismic", vmin=-rng, vmax=rng, shading="gouraud") # 
    if not frameless:
        plt.colorbar()
        plt.ylabel("y coordinate")
        #plt.yscale("log")
        plt.title(term)
    #else:
    #    plt.colorbar()
    def savefig(simname, appendix=""):
        if (frameless):
            #extent = plt.gca().get_window_extent().transformed(plt..dpi_scale_trans.inverted())
            #plt.savefig('/tmp/test.png', bbox_inches=extent)
            plt.savefig(f"{simname}_{term}_frameless_{rng}{appendix}.pdf", bbox_inches="tight", pad_inches=0)
        else:
            plt.savefig(f"{simname}_{term}{appendix}.png")
    return savefig

def derive_phase_averaged(sim, field, axis, direction):
    # unfold channel first
    periods = sim.getMeshSize()[2] // field.shape[1]
    field_tiled = np.tile(field, (1, periods))
    field_flipped = np.flip(field_tiled, 0)
    field_unfolded = np.concatenate((field_tiled, field_flipped[1:,:]), axis=0)
    derived = sim.derive(field_unfolded, axis, direction)
    return derived[0:field.shape[0], 0:field.shape[1]]

def tke_with_streamplot(sim, statsfile_or_dict, term, minusterm=None, appendix="", **kwargs):
    if isinstance(statsfile_or_dict, Simulation):
        statsfile = statsfile_or_dict
        export = plot_tke_term(statsfile, term, minusterm, **kwargs)
        datatable = statsfile.contents
    else:
        export = plot_tke_term(sim, term, **kwargs)
        datatable = statsfile_or_dict
    
    tke = datatable["k"]
    hor = None
    if (term == "mconv"):
        hor = datatable["<w>"]*tke
        vert = datatable["<v>"]*tke
    elif (term == "viscdiff"):
        vert = -derive_phase_averaged(sim, tke, 0, "y")
        hor = -derive_phase_averaged(sim, tke, 1, "z")
    elif (term == "ptr"):
        vert = - derive_phase_averaged(sim, datatable["<v'*p'>"], 0, "y")
        hor = - derive_phase_averaged(sim, datatable["<w'*p'>"], 1, "z")
    elif (term == "ttrans"):
        vert = (sum([datatable[f"<{a}'*{a}'*v'>"] for a in "uv"]) + datatable[f"<v'*w'*w'>"])*0.5
        hor = sum([datatable[f"<{a}'*{a}'*w'>"] for a in "uvw"]) * 0.5 
    if (hor is not None):
        add_streamplot_to_plot(datatable, hor, vert, **kwargs)
    plt.xlim(0, 2)
    plt.ylim(0, 1)
    return export
    
# def plot_term_contour(sim, term, shift = 1/4, start=50000, end=200000, **kwargs)
#     if (initialize):
#         if (frameless):
#             plt.figure(figsize=(size_x, size_y), frameon=False)
#             plt.gcf().set_size_inches(size_x, size_y, forward=False)
#             plt.gcf().add_axes([0, 0, 1, 1])
#             plt.axis("off")
#         else:
#             plt.figure(figsize=(size_x, size_y))
#     sim.compute_coordinates()

#     g = Stats2DManager(sim, "x")
#     newx = np.append(averaged.otherdim, np.array(averaged.otherdim[-1]+(averaged.otherdim[1] - averaged.otherdim[0])))
#     shift = shift.period
#     field = np.roll(g.contents[term] / g.getDimensionFactor(term), sim.getPeriod("z") * shift)
#     normalized = np.roll(field, shift, 1)
#     normalized = np.append(normalized, normalized[:, 0][:, None], axis=1)
#     plt.pcolormesh(sim.x)