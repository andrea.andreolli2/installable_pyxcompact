from pyxcompact.simulation import Simulation, MultiSimulation
from pyxcompact.filechecker import Stats2DManager, MonitorManager, CheckpointManager, ypManager
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path

sim = Simulation("/media/data/ISTM/wall_friction/variable/checkerboard/input.i3d")
sim.compute_coordinates()
a = Stats2DManager(sim, "x")
b = CheckpointManager(sim)
#c = ypManager(sim)



res = a.TimeAverageInterval(500, 6000).flatten("z").computeReynoldsStresses()


print(res.getWallStress())

plt.figure()
#plt.plot(sim.y, res.contents["<u>"])
a.files[0].loadFile()
#plt.imshow(b.files[0].contents["ux"][1])
plt.plot(sim.y, res.contents["<u>"])
plt.show()

