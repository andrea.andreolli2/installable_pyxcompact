from argparse import ArgumentParser
import toml
from glob import glob
import os

parser = ArgumentParser(
    prog = 'inventory',
    description = 'Returns a list of restart files that are yet to be used for this set of transient simulations.',
)
parser.add_argument('target_dir', metavar='sim_folder/', type=str, nargs=1,
                    help='the directory containing the set of transient simulations')

from argparse import ArgumentParser
import toml
from glob import glob
import os

parser = ArgumentParser(
    prog = 'inventory',
    description = 'Returns a list of restart files that are yet to be used for this set of transient simulations.',
)
parser.add_argument('target_dir', metavar='sim_folder/', type=str, nargs=1,
                    help='the directory containing the set of transient simulations')

# unpack input
inarg = parser.parse_args()
target_dir = inarg.target_dir[0]

#######################################

# first off: get list of used files
list_used = []
list_fold = [sthing for sthing in os.listdir(target_dir) if sthing.isnumeric()]
for fold in list_fold:
    xs = toml.load(target_dir + f'/{fold}/extra_settings.toml')
    list_used.append(xs['spinoff']['restart_fid'])

# get restart folder
if 'deca' in target_dir:
    src_dir = target_dir.replace('deca_','')
elif 'form' in target_dir:
    src_dir = target_dir.split('form_')[0] + 'r180_ref/'
else:
    raise Error('this is not a transient simulation folder!')
list_available = [afile.replace(src_dir,'').replace('/','').replace('restart','').replace('.info','') for afile in glob(src_dir + '/restart*.info')]

# remove unwanted elements
for used_file, da_fold in zip(list_used,list_fold):
    if list_used.count(used_file) > 1:
        print(f'Warning: file {used_file} in folder {da_fold} used multiple times!')
    if used_file in list_available:
        list_available.remove(used_file)

# sort the lists
list_available.sort(key=int)
list_used.sort(key=int)

print()
print('Available:', *list_available, sep=' ')
print()
print('Used:', *list_used, sep=' ')
print()