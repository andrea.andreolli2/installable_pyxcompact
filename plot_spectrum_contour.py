from pyxcompact.simulation import Simulation, MultiSimulation
from pyxcompact.filechecker import Stats2DManager, MonitorManager, CheckpointManager, PlanesManager
import matplotlib.pyplot as plt
import numpy as np
from numpy.fft import fftn
from pathlib import Path
from numba import jit
import os

sim  = Simulation("/media/data/ISTM/re150/re150/input.i3d")
sim.compute_coordinates()

c = CheckpointManager(sim)



lx = sim.getInputParam("BasicParam/xlx")
ly = sim.getInputParam("BasicParam/yly")
lz = sim.getInputParam("BasicParam/zlz")


g = Stats2DManager(sim, "x")
averaged = g.TimeAverageInterval(95000, 100000).flatten("z").computeReynoldsStresses()
utau =averaged.getDimensionFactor("<u>")**2
yplus = [15, 100]
axes = sim.getDimensionlessAxes(True)
indices = list(range(int(len(axes[1]) / 2)))
half = axes[1][0:len(indices)]

Rhat_ij = c.extractVirtualPlanes("y", indices).GetTimeAveragedPhiTensor(20000, 100000, "y")[1]

axes = [1, 0]
lengths = [lx, lz]
sizes = [len(sim.x), len(sim.z)]
names = ["x", "z"]
plots = [[0, 0], [2, 2], [1, 1], [0, 1]]
plnames = ["uu", "ww", "vv", "uv"]
counter = 0
plt.figure(figsize=(10, 14))
for ax, l, s, n in zip(axes, lengths, sizes, names):
    avg = np.average(Rhat_ij[:, :, :, :, :], ax)
    lmbda = np.zeros(s)
    for x in range(s):
        kx = x * 2 * np.pi / l
        avg[x, :, :, :] *= kx / utau
        if kx > 0:
            lmbda[x] = 2 * np.pi / kx * sim.getInputParam(f"BasicParam/re")
        else:
            lmbda[x] = 100000
    avg[0:1, :, :, :] = np.NaN

    lmbda_max = (np.abs(lmbda - 8)).argmin() 
    for p, pname in zip(plots, plnames):
        plt.subplot(4, 2, counter+1)
        cs = plt.contour(half, lmbda[0:lmbda_max], avg[0:lmbda_max, p[0], p[1], :] * (-1 if pname == "uv" else 1), vmin=0)
        plt.clabel(cs, inline_spacing=0, inline=False)
        plt.ylim(bottom=20, top=3000)
        plt.xlim(left=1, right=1000)
        plt.xscale("log")
        plt.yscale("log")
        plt.ylabel(f"$\\lambda_{n}^+$")
        plt.xlabel(r"$y^+$")
        sign = "-" if pname == "uv" else ""
        plt.title(f"${sign}k_{n}\\cdot\\Phi_{{{pname}}}/u_\\tau^2$")
        #plt.colorbar()
        counter += 2
    counter = 1
plt.subplots_adjust(wspace=0.3, hspace=0.4)
plt.savefig("output/spectrum_components.png", dpi=150)
plt.show()
