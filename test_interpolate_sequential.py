from pyxcompact.simulation import *
from pyxcompact.filechecker import *
from lib.Xcompact3d import Restart
from pathlib import Path
import copy
import numpy as np

a = Simulation("/media/data/ISTM/re150/re360/input.i3d")
b = MonitorManager(a)
c = CheckpointManager(a)


other = copy.deepcopy(a)
other.path = Path("/media/data/ISTM/re150/re540/input.i3d")
other.input["BasicParam/nx"] = 100 #1024
other.input["BasicParam/ny"] = 51 #521
other.input["BasicParam/nz"] = 50 #512
other.input["BasicParam/beta"] = 0.4064694669

restartfile = c.files[-1].interpolate(other, sequential=True)

print(restartfile.CheckForPlausibility(other))

