from pyxcompact.simulation import Simulation, MultiSimulation
from pyxcompact.filechecker import Stats2DManager, MonitorManager, CheckpointManager, SnapshotManager
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path

#sim = Simulation("/media/data/ISTM/re150/re540/run01/input.i3d")
sim = Simulation("/media/data/ISTM/re150/re150/input.i3d")


g = Stats2DManager(sim, "x")
averaged = g.TimeAverageInterval(50000, 100000).flatten("z").computeReynoldsStresses().computeTKEChannelBudget()
g.TimeAverageInterval(50000, 100000).writeFile()

axes = g.getDimensionlessAxes(True)
re = sim.getInputParam("BasicParam/re")

cp = CheckpointManager(sim)
plt.figure(figsize=(12, 8))

def read_headers(root, headers):
    result = {}
    for file, skip, header in headers:
        tx = np.loadtxt(root / file, skiprows=skip)
        for i in range(len(header)):
            result[header[i]] = [tx[:, 1], tx[:, i]]
    return result

def read_hj(re : int):
    root = Path("/media/data/ISTM/re150/hj/")
    headers = [[f"Re{re}.bal.kbal", 33, ["y/delta", "y+", "dissip", "produc", "pstrain", "pdiff", "tdiff", "vdiff"]]]
    return (f"Hoyas/Jimenez 2008, Re={re}", read_headers(root, headers))

hj_label, hj = read_hj(180)

for key in ["dissip", "produc", "pdiff", "tdiff", "vdiff"]:
    #pass
    plt.plot(hj[key][0], hj[key][1], "--", label=f"{hj_label}_{key}") 

sm = SnapshotManager(sim)
summ = 0
result = sm.getTimeAveragedTKEBudget(50000, 100000, "/media/data/storage/Studium/7. Semester/Masterarbeit/code kostelecky/post_field")

for key in result.keys():
    current = np.average(result[key], axis=1) / averaged.getDimensionFactor(key, True)
    summ += current
    plt.plot(axes[0],current, label=f"fortran_{key}")

print("test")

def read_mkm(re : int):
    root = Path(f"/media/data/ISTM/re150/Texas2/chan{re}/profiles/")
    headers = [[f"chan{re}.means", 25, ["y/delta", "y+", "<u>", "d<u>/dy", "<w>", "d<w>/dy" "P"]],
               [f"chan{re}.reystress", 25, ["y/delta", "y+", "<u'*u'>", "<v'*v'>", "<w'*w'>", "<u'*v'>", "<u'*w'>", "<v'*w'>"]],
               [f"chan{re}.velp", 25, ["y/delta", "y+", "<u'*p'>", "<v'*p'>", "<w'*p'>", "<p'*p'>"]],
               [f"chan{re}.vortvar", 25, ["y/delta", "y+", "<omega_x'*omega_x'>", "<omega_y'*omega_y'>", "<omega_z'*omega_z'>", 
                                                      ]]]
    return (f"MKM 1999, Re={re}", read_headers(root, headers))

# using python postprocessing
for key in ["viscdiff", "ttrans", "ptr", "P"]: #"viscdiff", "psdiss"  "ttrans", "ptr", "P", "viscdiff", , 
    dimensionless_data = averaged.getDimensionless(True)[key] 
    #summ += dimensionless_data
    #plt.plot(axes[0], dimensionless_data, label=key)
plt.plot(axes[0], summ, "k-", label="sum")
plt.legend()
plt.xlim(0, 150)

plt.savefig("output/tke_budget.png", dpi=150)
plt.show()