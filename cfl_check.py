from argparse import ArgumentParser
import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt

####################
# HARD CODED PARAM #
####################
stab_threshold = 0.871 # see TLAB doc by Juan Pedro Mellado
###########################################################

parser = ArgumentParser(
    prog = 'cfl_check',
    description='Read CFL number(s) from xcompact log files and performs checks. Recursively checks for files in the current working directory (CWD).'
    )
parser.add_argument('input_dir', metavar='sim_dir', type=str, nargs=1,
                    help='this folder is recursively scanned for log files to analyse')
parser.add_argument('keyword', metavar='log_file_keyword', type=str, nargs=1,
                    help='files whose names contain this keyword are identified as log files')
settings = parser.parse_args()
input_dir = settings.input_dir[0]
lkey = settings.keyword[0]

pattern = f'{lkey}'
filelist_gen = Path(input_dir).rglob(pattern)
file_list = [tf for tf in filelist_gen]

print('\nThe following files have been found:')
print(file_list)
print()

cfl_x = []
cfl_y = []
cfl_z = []

count_files = 0
for filename in file_list:

    count_files += 1

    with open(filename,'r') as f:
        all_lines = f.readlines()

    ii = 0
    while ii < len(all_lines):
        if 'CFL_x' in all_lines[ii]:
            cfl_x.append(float(all_lines[ii].split(':')[-1]))
            cfl_y.append(float(all_lines[ii+1].split(':')[-1]))
            cfl_z.append(float(all_lines[ii+2].split(':')[-1]))
            ii += 3
        else:
            ii += 1

if count_files == 0:
    raise Exception('no files matching the provided keyword were found.')

cfl_x = np.array(cfl_x); cfl_y = np.array(cfl_y); cfl_z = np.array(cfl_z)

array_of_maxima = np.array([np.amax(cfl_x), np.amax(cfl_y), np.amax(cfl_z)])
globmax = np.amax(array_of_maxima)

if globmax > stab_threshold:
    print()
    print('################# WARNING #################')
    print(' Max CFL greater than stability threshold ')
    print('###########################################')
print()
print(f'Global CFL maximum: {globmax}')
print()
print(f'CFL_x   -   initial: {cfl_x[0]}   -   mean: {cfl_x.mean()}   -   max: {array_of_maxima[0]}')
print(f'CFL_y   -   initial: {cfl_y[0]}   -   mean: {cfl_y.mean()}   -   max: {array_of_maxima[1]}')
print(f'CFL_z   -   initial: {cfl_z[0]}   -   mean: {cfl_z.mean()}   -   max: {array_of_maxima[2]}')
print()

fig, ax = plt.subplots()
ax.axhline(y=stab_threshold, color='r')
ax.plot(cfl_x)
ax.plot(cfl_y)
ax.plot(cfl_z)
ax.legend(['stab. threshold','CFLx', 'CFLy', 'CFLz'])
plt.show()