from argparse import ArgumentParser
import numpy as np
import matplotlib.pyplot as plt

parser = ArgumentParser(
    prog = 'cfl_check',
    description='Analyse xcompact log files to make estimates about performance.'
    )
parser.add_argument('input_file', metavar='xcompact_log.out', type=str, nargs=1,
                    help='xcompact log file.')
settings = parser.parse_args()
input_file = settings.input_file[0]

with open(input_file,'r') as f:
    all_lines = f.readlines()

# get dt in simulation time
for line in all_lines:
    if 'Time step dt' in line:
        dt = float(line.split(':')[-1])
        break

wallt_for_step = []
wallt_for_write = []

next_is_io = False
for line in all_lines:
    if 'Writing restart point' in line:
        next_is_io = True
    elif ' Time for this time step (s)' in line:
        if next_is_io:
            wallt_for_write.append(float(line.split(':')[-1]))
            next_is_io = False
        else:
            wallt_for_step.append(float(line.split(':')[-1]))

wallt_for_step = np.array(wallt_for_step)
wallt_for_write = np.array(wallt_for_write)

print()
print(f'Average wall time for a single time step: {wallt_for_step.mean()}')
print(f'Average wall time for io: {wallt_for_write.mean()}')
print()
print('Assuming value of dt is in mixed units:')
wallt_for_mu = np.ceil(1/dt)*wallt_for_step.mean() + wallt_for_write.mean()
print(f'Estimated wall time to produce 1 h/utau: {wallt_for_mu}s = {wallt_for_mu/3600}h')
print()

fig, ax = plt.subplots()
ax.plot(wallt_for_step)
plt.show()

