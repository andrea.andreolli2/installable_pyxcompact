from argparse import ArgumentParser
from pyxcompact.simulation import Simulation
import numpy as np

parser = ArgumentParser(
    prog = 'get_fourier_number',
    description='Calculates the Fourier number(s) for a given simulation.'
    )
parser.add_argument('input_file', metavar='input.i3d', type=str, nargs=1,
                    help='input file with simulation details.')

# unpack input
settings = parser.parse_args()
input_file = settings.input_file[0]
sim = Simulation(input_file)
re = sim.getInputParam('BasicParam/re'); xnu = 1/re
dt = sim.getInputParam('BasicParam/dt')
dx = sim.x[1] - sim.x[0]
dz = sim.z[1] - sim.y[0]
dy = sim.y[1] - sim.y[0]

# get numbers
fx = xnu * dt / (dx**2); fy = xnu * dt / (dy**2); fz = xnu * dt / (dz**2)
ftot = fx + fy + fz

# print out stuff
print()
print(f'Fourier number in x: {fx}')
print(f'Fourier number in y: {fy}')
print(f'Fourier number in z: {fz}')
print(f'Total Fourier number: {ftot}')
print()

#######################
# enter fomax or quit #
#######################
fomax = input('Enter target value of the Fourier number or press enter to quit: ')
if not fomax:
    exit()
fomax = float(fomax)
# get recommended dt
dt_recomm = fomax/np.amax(np.array([fx,fy,fz]))*dt
print(f'Suggested dt: {dt_recomm}')
print()

#######################
# enter retau or quit #
#######################

ret = input('Enter friction Reynolds number for more stats or press enter to quit: ')
if not ret:
    exit()

# unpack
ret = float(ret)
utau = ret/re
# assume h=1 and that you always have non-dimensionalisation with h in simulation
print() # one line spacing

# get dt in mixed units
mix_t_unit = 1/utau # h / utau
dt_recomm_mix = dt_recomm / mix_t_unit
print(f'Suggested dt in mixed units (h/utau): {dt_recomm_mix}')

# get dt in viscous units
visc_t_unit = mix_t_unit/ret
dt_recomm_visc = dt_recomm / visc_t_unit
print(f'Suggested dt in viscous units (h/utau): {dt_recomm_visc}')

# get number of iterations for 1 mixed unit
niter_onemix = int(np.ceil(mix_t_unit/dt_recomm))
print()
print(f'Number of iterations needed to produce 1 mixed unit: {niter_onemix}')
print()